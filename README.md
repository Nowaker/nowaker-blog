# #!/bin/nwkr devblog - Nowaker's blog 

This is my blog at [nowaker.net](https://www.nowaker.net/).
It's built with [Middleman](http://middlemanapp.com/).

## Requirements

- [RVM](https://rvm.io/)
- Ruby 2.3.3 from RVM
- Linux or Mac

### One time setup

```
rvm install 2.3.3
rvm use 2.3.3 --default
gem install bundler
yaourt -S advancecomp jpegoptim libjpeg-turbo optipng pngcrush
```

### Development

```
bundle install
bundle exec middleman server
xdg-open http://0.0.0.0:4567/
```

### Building

```
bundle exec middleman build
```

### Deployment

Website is built by [GitLab CI](https://about.gitlab.com/gitlab-ci/), whereas
the website is hosted at [Firebase](https://firebase.google.com/). `master`
branch is deployed to [nowaker.net](https://www.nowaker.net/) when build passes.

## License

See [LICENSE.md](/Nowaker/nowaker-blog/blob/master/LICENSE.md).
