require 'slim/smart'

Time.zone = 'UTC'

configure :development do
  activate :livereload
end

set :css_dir, 'css'
set :js_dir, 'js'
set :images_dir, 'img'
set :fonts_dir,  'font'

activate :syntax
set :markdown_engine, :redcarpet
set :markdown, fenced_code_blocks: true, smartypants: true

activate :bootstrap_navbar

configure :build do
  unless ENV['SKIP_IMAGEOPTIM']
    activate :imageoptim do |opts|
      opts.pngout = false # not available in Ubuntu
    end
  end
  activate :minify_css
  activate :minify_javascript
  activate :asset_hash
end

activate :blog do |blog|
  blog.permalink = 'post/{title}.html'
  blog.sources = 'posts/{year}/{year}-{month}-{day}-{title}.html'

  # blog.taglink = "tags/{tag}.html"
  blog.layout = 'post'
  blog.summary_separator = /<!--more-->/
  blog.summary_length = 250
  blog.default_extension = '.md'

  blog.paginate = false
end

page '/feed.xml', layout: false
