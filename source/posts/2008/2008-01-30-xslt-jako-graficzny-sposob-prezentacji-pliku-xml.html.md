---
title: XSLT jako sposób prezentacji danych XML
---

XSLT, czyli Extensible Stylesheet Language Transformations, służy w skrócie do prezentacji danych zawartych w pliku XML w graficzny sposób. Idea szeroko pojętego rozdzielania treści od formy jest tutaj zachowana. XML przechowuje tylko surowe dane, podczas gdy XSLT pobiera stosowne informacje z XML'a i prezentuje je w sposób przystępny dla czytającego. XSLT stanowi alternatywę dla generowania stron po stronie serwera (np. za pomocą PHP), jako że całej transformacji dokonuje silnik przeglądarki.

I tutaj zaczynają się schody... Czy postawić na nowe technologie XML'owe i generowanie widoku przez przeglądarkę, czy lepiej jednak zajmować się generowaniem widoku po stronie serwera? A może połączyć obie technologie i generować widok z szablonów XSLT za pomocą PHP? Pytanie pozostawiam otwarte.
<ul>
	<li>Projekt I:  <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker.xml" title="XML">plik xml</a>, <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker.xsl" title="XSL">plik xsl</a>, <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker.css">plik css</a></li>
	<li>Projekt II: <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker2.xml" title="XML">plik xml</a>, <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker2.xsl" title="XSL">plik xsl</a>, <a href="http://www.nowaker.net/wp-content/download/xslt/nowaker2.css" title="CSS">plik css</a></li>
</ul>