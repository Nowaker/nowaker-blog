---
title: Bezpieczeństwo SSH to walka, więc ulepsz sshd_config dla szatana!
---

Bardzo wiele serwerów korzysta z protokołu SSH do udostępniania powłoki. Wszyscy wiedzą, że jest to protokół bardzo bezpieczny. Mniej osób zdaje sobie sprawę z tego, że najsłabszym ogniwem w systemie zabezpieczeń systemów jest użytkownik i jego hasło. Postaram się przedstawić kilka prostych sztuczek jak uczynić SSH chociaż trochę bezpieczniejszym.

<!--more-->

<strong>1.</strong> Domyślny port, na którym działa SSH to 22. Dobrym pomysłem jest przenieść SSH na port powyżej 1024. Unikniemy w ten sposób znakomitą większość ataków, jaką są ataki słownikowe. W większości są to niegroźne próby zalogowania się przy użyciu haseł pobranych ze słownika. Jednak gdyby jakimś cudem hasło, któregoś z użytkowników znalazło się w takim słowniku sytuacja przestałaby być mało groźna. Alternatywnym rozwiązaniem jest stosowanie skryptów, które przeglądają logi i po kilku nieudanych próbach zalogowania dodają do firewalla regułkę blokująca adres IP atakującego. Osobiście korzystałem jedynie z Bruteblocka, który działa na FreeBSD i można go sobie zainstalować z portów.

<strong>2.</strong> Kolejną ważną rzeczą jest niepozwolenie na logowanie się przez SSH superużytkownikowi (root). Musimy zatem dopilnować, aby opcja PermitRootLogin była ustawiona na no. Logujemy się wówczas na zwykłego użytkownika, a prawa roota uzyskujemy poprzez użycie komendy su lub sudo. Należy dopilnować, aby użytkownik, z którego chcemy mieć możliwość użycia komendy su znalazł się w odpowiedniej grupie - najczęściej jest to grupa wheel.

Można również w konfigu demona SSH wyszczególnić, jacy użytkownicy mają prawo do logowania się przez SSH:

<code>AllowUsers nowaker kwiat admin</code>

<strong>3.</strong> Domyślnie demon SSH pozwala na logowanie się przy użyciu protokołów w wersji 1 oraz 2. Wersja 1 jest mniej bezpieczna dlatego powinniśmy zezwalać na używanie tylko wersji 2:

<code>Protocol 2</code>

<strong>4.</strong> Dobrym pomysłem jest również zezwalanie na łączenie się jedynie z określonych adresów IP. Można tego dokonać poprzez odpowiednie regułki do firewalli lub poprzez użycie tzw. wrapperów TCP. W takim przypadku korzystamy z pliku /etc/hosts.deny i uzupełniamy go wg uznania.Warto również zastanowić się nad limitowaniem ilości połączeń SSH w jednostce czasu za pomocą firewalla.

W ten oto sposób przedstawiłem kilka bardzo prostych sposobów na minimalne zwiększenie bezpieczeństwa naszego demona SSH. Nie jest to może instrukcja typu kopiuj-wklej, aczkolwiek przy odrobinie chęci każdy początkujący administrator powinien sobie z tym poradzić.

Dzięki dla kwiata, administratora FreeBSD na serwerze <a href="http://www.panic.pl">Panic.PL</a> za kilka poprawek merytorycznych :)

P.S. Tytuł wpisu stanowi nawiązanie do wpisu na <a href="http://zdzichubg.jogger.pl/2008/05/16/security-jest-walka-wiec-zmieniaj-klucze-dla-szatana/">zdzichuBG'u</a> ;)