---
title: Sprzedaż linków tekstowych przez LinkLift
---

Niektórzy pewnie zauważyli, iż wraz ze zmianą wyglądu bloga pojawił się z prawym menu dodatkowy dział pt. "Rekomendacje". Owy link to mój pierwszy, jaki udało mi się sprzedać tutaj na moim blogu za pośrednictwem firmy <a href="http://tnij.org/nwkr_ll" title="LinkLift - sprzedaż linków">LinkLift</a>.

<!--more-->

Jeśli chodzi o zarobki, to są całkiem niezłe. <a href="http://tnij.org/nwkr_ll" title="LinkLift - sprzedaż linków">LinkLift</a> wycenia naszego linka i pośredniczy w transakcjach, biorąc dla siebie 30% z ceny linku. Wg mnie to bardzo dobre rozwiązanie, ponieważ nie musimy się zbytnio wysilać wystawianiem linków na Allegro, obsługą nowych klientów, pobieraniem płatności - w przypadku <a href="http://tnij.org/nwkr_ll" title="LinkLift - sprzedaż linków">LinkLift</a> instalujemy tylko odpowiedni skrypt PHP i dalej już nas nic nie interesuje.

Odnośnie wyceny, jaką przeprowadza <a href="http://tnij.org/nwkr_ll" title="LinkLift - sprzedaż linków">LinkLift</a>, to jest ona uzależniona od wielu różnych czynników. Głównym czynnikiem jest PageRank, a drugim ważnym czynnikiem jest położenie linka na stronie. Wartość własnej strony można oszacować sobie <a href="http://tnij.org/nwkr_ll2" title="LinkLift - sprzedaż linków">tutaj</a>.

Aby nie być gołosłownym, mówiąc "niezłe zarobki" przedstawiam kwoty, na jakie wyceniono linki na moich stronach. Podana kwota to cena za jeden link na okres jednego miesiąca.
<ul>
	<li><a href="http://www.geozone.kero.pl" title="portal geograficzny - geografia">Portal geograficzny GeoZone</a> - 45 zł / miesiąc</li>
	<li><a href="http://www.forum.geozone.pl" title="Forum geograficzne - geografia">Forum geograficzne GeoForum</a> - 17 zł / miesiąc</li>
	<li><a href="http://www.nowaker.net" title="Nowakerowy webdeveloperski blog">#!/bin/nwkr devblog</a> - 7 zł / miesiąc</li>
</ul>
<a href="http://tnij.org/nwkr_ll">LinkLift </a>oferuje również <a href="http://tnij.org/nwkr_ll3" title="LinkLift - sprzedaż linków">program partnerski</a> - oferują wynagrodzenie w wysokości 25 zł za klienta, który po rejestracji sprzeda swoje linki, bądź zakupi jakieś za co najmniej 120 zł. Ja również uczestniczę w tym programie partnerskim :)