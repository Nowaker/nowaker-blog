---
title: Integracja Open Power Template z KohanaPHP framework
---

Są dwa sposoby, aby zintegrować system szablonów OPT z frameworkiem Kohana.
Pierwszy, łatwiejszy i zarazem bardziej elegancki, nie wymaga żadnych modyfikacji w OPT.

<!--more-->

Należy ściągnąć OPT i umieścić go w katalogu `system/vendor/opt`.
Tworzymy również katalog `application/templates`, a w nim katalogi `_compile` oraz `_cache`.

Poniżej prezentuję zaś kod, który zmusi nasz OPT do działania:

<code>define ('OPT_DIR', SYSPATH .'vendor/opt/');
require (SYSPATH .'vendor/opt/opt.class.php');
$this-&gt;opt = new OptClass;
$this-&gt;opt-&gt;root = APPPATH .'templates/';
$this-&gt;opt-&gt;compile = APPPATH .'templates/_compile/';
$this-&gt;opt-&gt;cache = APPPATH .'templates/_cache/';
$this-&gt;opt-&gt;plugins = OPT_DIR .'plugins/';</code>

I od tegoż momentu możemy już korzystać z naszego <strong>$this-&gt;opt</strong>.

Drugi sposób na uruchomienie Open Power Template z KohanaPHP to taka <strong>modyfikacja OPT</strong>, aby był częścią frameworka (dokładniej: biblioteką). Zaletą takiego rozwiązania jest to, że nie trzeba niczego "require'ować" (wystarczy tylko $this-&gt;opt = new Opt;). Utrudnieniem są jednak aktualizacje OPT - trzeba na nowo modyfikować pliki. Poniżej przedstawiam rozwiązanie krok po kroku:
<ol>
	<li>Rozpakować wszystkie pliki <strong>oprócz </strong>opt.class.php do <strong>system/libraries/opt</strong></li>
	<li>Rozpakować plik <strong>opt.class.php</strong> w <strong>system/libraries</strong> i nadać mu nazwę <strong>Opt.php</strong> (z wielkiej litery)</li>
	<li>Linie 69-72:
<code>{
define('OPT_DIR', './');
}</code>
zamienić na:
<code>if(!defined('OPT_DIR'))
{
$docroot = pathinfo(str_replace('\\', '/', realpath(__FILE__)));
define('OPT_DIR', $docroot['dirname']. '/opt/');
}</code></li>
	<li>Linia 144: <strong>class optClass</strong> zamienić na <strong>class Opt_Core</strong></li>
</ol>
Tak, jak w pierwszym rozwiązaniu, muszą oczywiście istnieć katalogi <strong>application/templates/_compile</strong> oraz <strong>_cache</strong>.

Takim sposobem posiadamy rozbudowany system szablonów OPT w frameworku Kohana.
<ol>
	<li><a href="http://libs.invenzzia.org/pl/biblioteki/open-power-template" title="Open Power Template (OPT)">Open Power Template - oficjalna strona</a></li>
	<li><a href="http://pl.wikipedia.org/wiki/Open_Power_Template" title="Open Power Template">Open Power Template - Wikipedia</a></li>
	<li><a href="http://kohanaphp.com" title="Kohana PHP framework">KohanaPHP framework</a></li>
</ol>