---
title: OPTv2 tak samo wydajny, jak OPTv1
---

Kilka dni temu <a href="http://www.zyxist.com">Zyx</a> zakomunikował światu, iż Open Power Template został prawie ukończony. Wydana została ostatnia wersja developerska (2.0.0-dev8). Jeśli wierzyć zapewnieniom Zyxa, zostało mu już tylko napisać kilkaset testów PHPUnit. Wtedy będzie wiadomo, czy 2.0.0-dev8 stanie się finalną wersją, czy wyszły na jaw jakieś błędy ;)

<!--more-->

Gdy Zyx pracuje, Nowaker zastanawia się nad wydajnością nowego OPT. Jak wiadomo, OPTv2 jest zgodny ze składnią XML. Chcąc nie chcąc, przetwarzanie XML-a jest zawsze wolniejsze... Ale nie w OPTv2 :) W tym przypadku Zyx dość solidnie zoptymalizował algorytmy przetwarzania szablonów XML-owych.

Wykonałem dwa krótkie testy wydajności na Apache Benchmark (ab -n 1000 -c 10). Pierwszy zawierał tylko jedną zmienną, drugi zawierał dziesięć prostych sekcji. Na jedno wywołanie skryptu PHP generowanych było 30 szablonów - w moich aplikacjach zdarza mi się parsować taką ilość szablonów w trakcie jednego wywołania skryptu. Wyniki:
<table width="226" height="85">
<tr>
<th></th>
<th>OPTv1</th>
<th>OPTv2</th>
</tr>
<tr>
<th>Test 1</th>
<td>65.27</td>
<td>68.79</td>
</tr>
<tr>
<th>Test 2</th>
<td>31.36</td>
<td>30.32</td>
</tr>
</table>
Jak widać, OPTv2 jest prawie tak samo wydajny, jak OPTv1. Różnica między nimi wynosi maksymalnie 5%. Co ciekawe, szablon z sekcjami wykonał się nieco szybciej na OPTv2. Gratuluję :)

Ten mini-benchmark wykonałem z czystej ciekawości. Nawet 3 rzędy gorszy wynik OPTv2 nie zniechęciłby mnie z korzystania z tej biblioteki. Jestem zdecydowanym zwolennikiem pisania kodu na wyższym poziomie abstrakcji, co pozwala skupić się na wydajnym pisaniu kodu, bez zastanawiania się nad sprawami drugorzędnymi.

Odwiedź również:
<ul>
	<li><a href="http://blog.invenzzia.org/en/post/One-year-of-hard-work">wpis nt. wydania ostatniej wersji developerskiej OPTv2</a> (ang.)</li>
	<li><a href="http://www.invenzzia.org/docs/OPT2-EN/">dokumentacja OPTv2</a> (ang.)</li>
	<li><a href="http://www.zyxist.com/pokaz.php/pozbadz_sie_rekurencji">o tym, jak Zyx pozbywa się rekursji, aby zagwarantować szybkie działanie OPTv2</a> ;)</li>
</ul>
P.S. Proszę nie sugerować się taką małą ilością żądań na sekundę. Domowy serwer to wiekowy blaszak.