---
title: DOS Prompt Here, czyli ułatwienie życia Windowsiarza
---

Miałem dzisiaj coś do porobienia z poziomu konsoli DOS-owej. Aby wejść do katalogu
`E:\Programowanie\Java\Książki\Materiały\Java Core 1\v1ch9` musiałem się nieźle ponaciskać tych Tabów,
albo naklikać (bo "mądra" konsola DOS'owa nie wie, co to CTRL+V).
Wtem przypomniało mi się, że miałem kiedyś w menu kontekstowym pozycję "DOS Prompt Here",
która otwierała mi konsolę na wskazanym katalogu...

<!--more-->

![DOS Prompt Here](/img/posts/dos_prompt_here.png)

... więc i znów ją mam. ;)

Dla zainteresowanych:

- [DOS Prompt Here](/files/dos_prompt_here.reg) - plik REG</li>
