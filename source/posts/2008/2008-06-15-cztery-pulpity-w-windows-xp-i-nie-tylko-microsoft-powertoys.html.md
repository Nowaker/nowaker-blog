---
title: Cztery pulpity w Windows XP i nie tylko – Microsoft PowerToys
---

KDE i Gnome mają obsługę wirtualnych pulpitów już od dawna, podczas gdy najnowszy Windows jeszcze tego nie ma.
Podczas przełączania za pomocą ALT+TAB widzimy tylko ikony poszczególnych programów, zamiast podglądu okna programu.
I wiele innych... Okazuje się jednak, iż na stronie Microsoftu udostępniona jest seria tzw. PowerToys dla Windows XP,
które usprawniają codzienną pracę z systemem.

<!--more-->

Poniżej podaję listę "mocnych zabawek", z których skorzystałem wraz z linkiem do ściągnięcia.
Resztę dodatków (14) znajdziecie [pod tym adresem](http://www.microsoft.com/windowsxp/downloads/powertoys/xppowertoys.mspx).
Ściągnięcie nie wymaga żadnych potwierdzeń legalności systemu.

* [Virtual Desktop Manager](http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/DeskmanPowertoySetup.exe) - cztery wirtualne pulpity
* [Alt-Tab Replacement](http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/TaskswitchPowertoySetup.exe) - podgląd każdego okna
* [Power Calculator](http://download.microsoft.com/download/whistler/Install/2/WXP/EN-US/PowerCalcPowertoySetup.exe) - prosty kalkulator z rysowaniem wykresów

Virtual Desktop Manager w akcji:

[![Cztery pulpity](/img/posts/pulpity.png)](/img/posts/pulpity.png "Cztery pulpity")
