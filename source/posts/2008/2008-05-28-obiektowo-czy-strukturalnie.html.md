---
title: Obiektowo czy strukturalnie?
---

<a href="http://blog.malcom.pl/2008/05/18/o-frameworkach/">Malcom na swoim blogu</a> podzielił się swoimi przemyśleniami na temat obiektowości w webdevelopingu, choć nie tylko. Jako że nie zgadzam się za bardzo, z tym, co napisał - odpisałem mu w komentarzu. Tak się tam rozpisałem, że zyskałem i materiał na wpis na moim blogu :)

<!--more-->

<h2>Strukturalnie czy obiektowo?</h2>
Jakie podejście jest lepsze - strukturalne czy obiektowe? <strong>Tym pytaniem chciałbym wywołać dyskusję</strong>, w jakich przypadkach lepiej jest stosować jedno, bądź drugie rozwiązanie. Nie można bowiem powiedzieć, że tylko jedno jedyne podejście jest słuszne, a inne całkowicie odpada.

Moim zdaniem, w webdevelopingu rządzi obiektowość. Wymagania co do moich projektów poszerzają się bardzo szybko. Bez obiektowości konserwacja aplikacji jest bardzo ciężka. Przykładem jest tutaj <a href="http://www.geozone.pl">portal geograficzny GeoZone.pl</a> - 3 lata dopisywania kolejnego strukturalnego kodu doprowadziło do kresu możliwości dalszego rozwoju. By dokonać jedną zmianę trzeba odwiedzić kilkanaście różnych miejsc w kodzie. A potem i tak nie działa to do końca tak, jak chcę.

Do realizacji algorytmów oczywiście obiektowość i wzorce projektowe nie bardzo się nam przydadzą. Tutaj rządzi C/C++ ze wględów wydajnościowych. (Oczywiście do organizacji struktur danych wykorzystujemy struktury, czy klasy - jak zwał, tak zwał - jednak nie stosujemy tu dziedziczenia, polimorfizmu, wzorców, itd.) Kto rozwiązywał zadania na <a href="http://pl.spoj.pl">SPOJ-u</a>, ten wie o co chodzi ;) A kto nie, temu tłumaczę, iż liczy się każda sekunda wykonywania programu.
<h2>Cytaty z MalDevBlog, komentarz Nowakerowy</h2>
<code>Oczywiście przewagą ActiveRecorda jest miedzy innymi to, że wystarczy zmienić implementację sterownika bazy i można generować zapytania do dowolnej bazy, bez poprawiania kodu. Ale czy znów tak często zmieniamy bazę danych, szczególnie w nieco mniejszych aplikacjach?
</code>

Często może i nie, co nie oznacza, że nie warto stosować warstwy abstrakcji na bazę danych. Za pół roku możesz np. zdecydować, że przenosisz swoje wszystkie projekty ze względów wydajnościowych z MySQL na PostgreSQL - i co wtedy zrobisz? Albo mozolnie zmienisz wszędzie mysql_ na odpowiedniki pg_ albo... olejesz sprawę, bo będzie z tym zbyt dużo roboty.

<code>Inna sprawa, to porozrzucanie po 40 klasach 4 funkcji, i tworzenie 10 obiektów tylko po to, aby zmienić jakąś regule w routingu czy funkcjonalność w innym module. I później przedzierać się przez tony dokumentacji i kodu, aby w efekcie otrzymać zamierzony cel działania ;)
</code>

Wg mnie nie masz tutaj racji, ponieważ dokumentację bedziesz musiał tylko raz przejrzeć, aby zrozumieć, o co chodzi. Później już nie będziesz tego odczuwał. Gdy zaś przyjdzie czas na jakąś przeróbkę w routingu, pójdzie ona gładko, podczas gdy w strukturalnym projekcie nie obeszło by się bez skakania po plikach i zmieniania wszystkiego po kolei w wielu miejscach.

Jednorazowe napisanie projektu obiektowo wymaga więcej pracy, aniżeli strukturalnie. Gdy zaś przychodzi czas na przeróbki, zyskujemy dużo czasu w stosunku do podejścia strukturalnego.

<code>Tak myślę, że chyba jednak dokończę tego własnego frameworka. Ileż to różnych projektów wstrzymałem, zawiesiłem, czy porzuciłem na różnych stopniach zaawansowania prac. Głównie z braku czasu i zmian w zapotrzebowaniu i użytkowaniu danego produktu (projektu).
</code>

Według mnie to upadło w wyniku strukturalnego projektowania. Zmieniło się zapotrzebowanie i okazało się, że napisany strukturalnie system nie jest już w ogóle przydatny. Przemyślany obiektowy system daje się łatwo wykorzystywać dalej oraz modyfikować, a więc i szansa powodzenia projektu jest większa.

Na koniec zapraszam na <a href="http://blog.malcom.pl">blog Malcoma</a>.