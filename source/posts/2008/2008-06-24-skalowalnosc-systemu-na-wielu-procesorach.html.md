---
title: Skalowalność systemu na wielu procesorach
---

Na rynku procesorów dla komputerów klasy PC swoje miejsce już dawno znalazły procesory tzw. dwurdzeniowe,
a procesory czterordzeniowe stają się coraz tańsze.
Procesor wielordzeniowy to de facto układ kilku oddzielnych procesorów o współdzielonej magistrali.
Rodzi się pytanie, czy procesor n-rdzeniowy wykona szereg operacji n razy szybciej niż jeden procesor.
Jak łatwo się domyśleć, odpowiedź brzmi "nie, jest wykona".

<!--more-->

Oprogramowanie nie jest idealne i nie jest przystosowane do obsługi wielu procesorów. Większość wykorzystywanych przez
nas algorytmów ma charakter sekwencyjny, tzn. aby wykonać następny krok, trzeba zakończyć poprzedni. Nawet gdy
programista poprawi algorytm tak, aby korzystał z wszystkich procesorów, nigdy nie osiągnie stuprocentowej skuteczności.
W praktyce, ta część programu, której nie daje się zrównoleglić wynosi co najmniej 10%.
Czy więc procesor 100-rdzeniowy jest tylko 10% wolniejszy od 100 osobnych procesorów? Kolejny raz odpowiadam "nie".

Gene Amdahl sformułował prawo dotyczące wyznaczania maksymalnego przyśpieszenia programu równoległego w środowisku wieloprocesorowym.

![Wzór na przyśpieszenie](/img/posts/wzor-na-przyspieszenie.png)

n to liczba procesorów, a s to procent części sekwencyjnej programu.

Wzór interpretujemy w taki sposób, że zawsze istnieje nierównoległa część programu, w której trzeba oczekiwać na wynik
poprzedniej operacji. Niemożliwe staje się więc wtedy przekazanie kolejnego rozkazu innemu procesorowi.

Poniżej przedstawiam teoretyczne przyśpieszenie programu w stosunku do systemu jednoprocesorowego na systemach o różnej
liczbie procesorów. Wartość dla nieskończenie wielu procesorów została obliczona z granicy funkcji.
Testuję kilka programów o różnym procencie zrównoleglenia. Wartość liczbowa wyraża przyśpieszenie w stosunku do systemu
1-procesorowego.

<table class="margin-bottom: 10px">
<tr>
<td>**% sekwencyjności**</td>
<td>15%</td>
<td>10%</td>
<td>5%</td>
<td>0,1%</td>
</tr>
<tr>
<td>**1 procesor**</td>
<td>1</td>
<td>1</td>
<td>1</td>
<td>1</td>
</tr>
<tr>
<td>**2 procesory**</td>
<td>1,7</td>
<td>1,8</td>
<td>1,9</td>
<td>2</td>
</tr>
<tr>
<td>**4 procesory**</td>
<td>2,8</td>
<td>3,1</td>
<td>3,5</td>
<td>4</td>
</tr>
<tr>
<td>**8 procesorów**</td>
<td>3,9</td>
<td>4,7</td>
<td>5,9</td>
<td>7,9</td>
</tr>
<tr>
<td>**20 procesorów**</td>
<td>5,2</td>
<td>6,9</td>
<td>10,3</td>
<td>19,6</td>
</tr>
<tr>
<td>**100 procesorów**</td>
<td>6,3</td>
<td>9,2</td>
<td>16,8</td>
<td>91</td>
</tr>
<tr>
<td>**nieskończenie wiele**</td>
<td>6,7</td>
<td>10</td>
<td>20</td>
<td>1000</td>
</tr>
</table>

Jak widać, potencjalnie niewielki procent kodu, który nie daje się wykonać równolegle ma bardzo poważne skutki w
wydajności programu na większej liczbie procesorów.
Nawet 99,9% równoległości programu spowoduje teoretycznie wzrost szybkości maksymalnie tysiąckrotnie.

Popatrzmy teraz na benchmark, aby odnieść te teoretyczne obliczenia do rzeczywistości.
Bierzemy pod uwagę procesory Phenom X3 8750 oraz Phenom X4 9750. Są one idealne do tego porównania,
ponieważ różnią się tylko i wyłącznie liczbą rdzeni, podczas gdy częstotliwość taktowania,
czy ilość pamięci podręcznej jest taka sama.

![Benchmark WinRAR](/img/posts/winrar-benchmark.thumbnail.png)

Jak widać, w żadnym z testów procesor trzyrdzeniowy nie jest półtora razy szybszy od dwurdzeniowego, choć potencjalnie
tego byśmy oczekiwali. Jest to potwierdzenie, że rzeczywiście programy testujące nie są do końca równoległe.
Przy okazji oceniłem program WinRAR, aby określić, w jakim stopniu algorytm kompresujący jest napisany równolegle.
Wynik to ok. 90%.

W teście nie brałem pod uwagę strat wydajności wynikających z konstrukcji sprzętu, np. zapychania się magistrali między
procesorami, problemów z zachowaniem spójności danych w poszczególnych L1 cache z pamięcią RAM, czy podziałem wątków na
procesory (system operacyjny).

Powiązane:

* Benchmarki pochodzą z [PCLab](http://pclab.pl/art32655-4.html).
* [Arkusz kalkulacyjny z obliczeniami przyśpieszenia](/files/przyspieszenie-programu.ods "Przyśpieszenie programu")
