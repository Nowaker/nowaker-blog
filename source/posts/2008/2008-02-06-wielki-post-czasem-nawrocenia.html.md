---
title: Wielki Post czasem nawrócenia
---

Dzisiaj, w Środę Popielcową, weszliśmy - my katolicy - w pierwszy dzień Wielkiego Postu. Wielki Post to okres szczególny. W czasie tych 40 dni mamy czas, aby skierować się czynami w stronę Boga. Czy jednak wykorzystamy ten czas tak, jak się należy?

<h2>Po co nam Wielki Post?</h2>
Wielki Post ma nas nastawić duchowo do godnego przeżycia najważniejszego z katolickich świąt - święta Wielkanocy. Jest to czas, gdy wspominamy zmartwychwstanie Jezusa Chrystusa. Jego zmartwychwstanie daje nam gwarancję, że również i my powstaniemy z martwych i będziemy się mogli cieszyć życiem wiecznym w idealnym świecie.