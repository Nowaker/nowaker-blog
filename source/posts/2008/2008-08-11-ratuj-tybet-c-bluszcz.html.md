---
title: Ratuj Tybet! (c) Bluszcz
---

W związku z trwającą właśnie Olimpiadą w Pekinie postanowiłem jeść mniej cukierków. Wierzę, że dzięki tej manifestacji obrony życia, Chiny zaczną respektować prawa człowieka.

<!--more-->

Ten wpis jest nawiązaniem do wpisu Bluszcza, który postanowił wyłączyć jeden z większych serwerów Jabbera (jabberpl.org) w Polsce  na czas Igrzysk. Gratuluję rozwagi!