---
title: Opera 9.50? Nie, dziękuję.
---

Na wielu subskrybowanych przeze mnie blogach pojawiło się wiele wpisów na temat nowej Opery 9.50. 
Zachwyt, jaki spowodowała nowa Opera jest ogromny, tymczasem ja jestem nią bardzo zawiedziony. 
Jeden dzień jej użytkowania i już mam jej dosyć.

<!--more-->

Opery używam począwszy od którejś "piątki" bądź "szóstki" (nie pamiętam dokładnie).
Swego czasu Opera była produktem adware - jeśli nie zapłaciłeś(aś),
to w prawym górnym rogu ekranu wyświetlał się banner wielkości 486x60.
Czasy te już dawno minęły i w chwili obecnej mamy już wersję 9.50.
Owa wersja jest pierwszą, którą uważam za gorszą w stosunku do poprzedniej.

Jako że już na pewno wiecie, co takiego ma nowa Opera, nie będę opisywał tych zmian.
Wymienię zaś to, co nową Operę czyni w moim mniemaniu gorszą.

* W pewnych nieznanych mi sytuacjach klient IRC działa w taki sposób,
  że co równe 5 minut wychodzi kanału i od razu na niego wraca.
  Niezły spam na kanale się robi z powodu takiego Nowakera.
* Przeciągnięcie okna na pasek zadań otwiera drugą Operę, a nie tylko to jedno konkretne okno.
  Przez to na słowo "Nowaker" w IRCu okno na pasku zadań nie ma highlighta.
  Nie widzę więc, gdy ktoś coś do mnie mówi.
* Skórka "Naturalna Windows" posiada czarno-białe ikony, co utrudnia szybką identyfikację funkcji.
  Nie lubię wodotrysków, dla mnie ewolucja wyglądu GUI zakończyła się na Windows 95.
* Okno pobierania zostało uszczuplone, poprzednio posiadało więcej informacji.
* "Wklej i przejdź", miało skrót CTRL+B, a teraz ma CTRL+SHIFT+V. Czy ten nowy jest szybszy?
* Czasami po wpisaniu adresu Opera nie odpowiada przez 3-6 sekund. Quad Core zbyt wolny dla niej?

Co jeszcze znajdę w ciągu następnej doby? Chyba już nic, jeśli zrobię downgrade.

Nowa Opera w akcji:

[![Urok nowej Opery](/img/posts/nowa-opera.thumbnail.png)](/img/posts/nowa-opera.png "Urok nowej Opery")