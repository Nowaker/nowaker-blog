---
title: O wzorcu projektowym Singletonu słów kilka
---


Na polskiej Wikipedii przeczytałem, iż singleton jest uważany za antywzorzec. To zdanie wzburzyło mnie dość mocno, ale gdy emocje tego strasznego przeżycia opadły zastanowiłem się, dlaczego autor tak napisał.

<!--more-->

Faktem jest, iż singleton jest często <strong>nadużywany </strong>przez programistów, którzy nie potrafią zaprojektować aplikacji w prawidłowy sposób. Uciekają się więc wtedy do singletonów i w każdym miejscu kodu wywołują Sth.instance().doSth(). Twierdzą przy tym, że ich aplikacja jest zaprojektowana w pełni obiektowo i, co więcej, korzystają z wzorca projektowego Singletonu!

Patrząc na to realnie, stworzyli sobie zmienną globalną - a to nie ma nic wspólnego z projektowaniem zorientowanym obiektowo. W rzeczywistości powstała tylko karykatura wzorca projektowego - co wcale nie oznacza, iż prawidłowy Singleton jest antywzorcem. Temu stwierdzeniu mówię stanowcze "nie" :)

Kiedy więc zastosowanie wzorca Singletonu ma sens? Jest kilka przypadków, w których singleton jest idealnym rozwiązaniem:

<ul>
	<li>gdy musi istnieć tylko jedna instancja danej klasy</li>
	<li>gdy z przyczyn wydajnościowych nie warto tworzyć i niszczyć w wielu miejscach instancji danej klasy</li>
</ul>

Przykładem z życia wziętym jest np. biblioteka Session z frameworka KohanaPHP. Biblioteka Session ma sterownik native, database oraz cookie i w zależności od wybranej operacji zachowuje sesję którymś ze sposobów. Jako że sesja istnieje tylko jedna (logicznie patrząc - użytkownik jest jeden na każde uruchomienie skryptu), stworzenie drugiej instancji sesji było całkowicie bez sensu; co więcej, mogłoby zniszczyć dane wykonane przez pierwszą sesję.

Drugim przykładem jest baza danych. Nawiązywanie połączenia z serwerem bazy danych zawsze zabiera trochę czasu. Gdyby dziesięć różnych modeli z mojej aplikacji miało tworzyć, po czym już za chwilę zamykać połączenie - straciłbym cenne milisekundy na każdym żądaniu.

Podsumowując, używajmy Singletonu z głową i zgodnie z założeniami. W innym wypadku będzie to tylko czysty singletonitis ;)


P.S. Kilka dni temu założyłem sobie <a href="http://nowaker.pinger.pl">mikro-devbloga</a> na Pingerze. Wszystkich zainteresowanych krótkimi wpisami developerskimi serdecznie zapraszam.