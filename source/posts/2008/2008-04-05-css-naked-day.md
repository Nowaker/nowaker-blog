---
title: CSS Naked Day
---

Dzisiaj, 5 kwietnia obchodzimy CSS Goły Dzień w Polsce (na świecie obchodzimy go 9 kwietnia).
Pomyślałem, że warto się przyłączyć i zobaczyć, jak będzie wyglądała strona bez dołączania arkuszy CSS.
Jak widać, szablon Wordpressowy jest dobrze zrobiony, strona wygląda OK.
Natomiast [Portal geograficzny GeoZone.pl](http://www.geozone.pl) wręcz przeciwnie, mimo odłączenia pliku CSS,
style i tak zostały częściowo zachowanie (dużo atrybutów "style").
Nie świadczy to dobrze o webmasterze (czyt. mnie), jednak na swoją obronę mogę powiedzieć, że projekt
[portalu geograficznego](http://www.geozone.pl/) powstał trzy lata temu oraz, że na dysku powstaje powolutku
nowe GeoZone zgodne z [wzorcem projektowym MVC](http://pl.wikipedia.org/wiki/MVC).
