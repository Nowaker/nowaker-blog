---
title: Integracja Open Power Template 1.x z KohanaPHP 2.2
---

Pół roku temu <a href="http://www.nowaker.net/devblog/framework-php/integracja-open-power-template-z-kohanaphp-framework">zamieściłem</a> sposób na integrację systemu szablonów Open Power Template z frameworkiem KohanaPHP. Po pewnym czasie doszedłem jednak do wniosku, iż moje rozwiązanie jest <strong>lamerskie</strong> ;) Oto zamieszczam moduł integrujący Kohanę z OPT w taki sposób, że z szablonów korzystamy zupełnie tak samo, jak z View.

<!--more-->

<code>$view = new View('test.tpl');
$view-&gt;dynamic = 'And this is a dynamic.';
$view-&gt;render(TRUE);</code>

Wybór silnika przetwarzania szablonów (natywny KohanaPHP lub OPT) dokonywany jest poprzez rozszerzenie szablonu. Wystarczy do nazwy szablonu dodać rozszerzenie <strong>.tpl</strong>, aby to OPT został użyty do wygenerowania widoku.

Moduł OPT v.1 dla Kohany w skrócie:
<ul>
	<li>Takie samo API, jak w View.</li>
	<li>Dobór silnika przetwarzania szablonów po rozszerzeniu.</li>
	<li>Kaskadowość systemu plików - szablony mogą się znajdować w różnych katalogach.</li>
</ul>
Zapraszam do pobierania i korzystania. Moduł został udostępniony na <a href="http://www.nowaker.net/wp-content/uploads/2008/11/opt-v1-kohana-module-license.html">licencji BSD</a>.
<ul>
	<li><a href="http://www.nowaker.net/wp-content/uploads/2008/11/opt-v1-kohana-module.7z" title="opt-v1-kohana-module.7z">opt-v1-kohana-module.7z</a></li>
	<li><a href="http://www.nowaker.net/wp-content/uploads/2008/11/opt-v1-kohana-module.tar.bz2" title="opt-v1-kohana-moduletar.bz2">opt-v1-kohana-module.tar.bz2</a></li>
	<li><a href="http://www.nowaker.net/wp-content/uploads/2008/11/opt-v1-kohana-module.tar.gz" title="opt-v1-kohana-moduletar.gz">opt-v1-kohana-module.tar.gz</a></li>
	<li><a href="http://www.nowaker.net/wp-content/uploads/2008/11/opt-v1-kohana-module.zip" title="opt-v1-kohana-module.zip">opt-v1-kohana-module.zip</a></li>
</ul>