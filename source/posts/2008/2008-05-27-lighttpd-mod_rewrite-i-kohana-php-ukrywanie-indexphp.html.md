---
title: LightTPD, mod_rewrite i Kohana PHP – ukrywanie index.php/
---

Dzisiejszy wpis poświęcony jest integracji frameworka KohanaPHP z serwerem <a href="http://www.lighttpd.net" title="LightTPD - light httpd server">Lighttpd</a> w taki sposób, aby ukryć adres index.php. Tzw. ładne linki, czy nice urls są bardzo ważne pod względem optymalizacji strony internetowej pod wyszukiwarki.

<!--more-->

<strong>Plik lighttpd.conf</strong>:
<code>
$HTTP["host"] =~ "^(www\.|)nowaker\.net$" {
simple-vhost.server-root = "/home/www/nowaker.net/html"
accesslog.filename = "/home/www/nowaker.net/logs/access.log"
url.rewrite-once = ("^/(.*)$"  => "index.php/$1")
}
</code>
<strong>Plik application/config/config.php</strong>:
<code>
[...]
$config['index_page'] = '';
[...]
</code>
Prawda, że proste? ;)