---
title: 33degree – wiedza wyniesiona z konferencji
---

Świat Javy zawsze budzi się na wiosnę.
Konferencje 33degree, DevCrowd oraz 4developers już za nami,
a wkrótce czekają na nas Geecon i Confitura.

Dzisiaj relacja z 33degree - prezentuję wiedzę prosto z moich notatek.
Wpis trochę spóźniony, ale wcześniej nie miałem czasu - a działo się sporo.
Zmieniłem stan cywilny, zmieniłem pracę i założyłem firmę. ;-)


<!--more-->

## Luke Daley, Smarter Testing with Spock

Kawałek kodu źrodłowego powie więcej niż tysiąc słów.

```groovy
import spock.lang.*

class HelloSpock extends Specification {

    @Unroll
    def 'string length of #name should be #length'() {
        expect:
        name.size() == length

        where:
        name      | length
        "Spock"   | 5
        "Kirk"    | 4
        "Scotty"  | 6
        "Nowaker" | 666
        "null"    | 0
    }
}
```

![Testy Spock w IntelliJ IDEA](/img/posts/spock-unroll.png)

A czerwone asercje wyglądają tak:
    
    name.size() == length
    |    |      |  |
    |    7      |  666
    Nowaker     false
     <Click to see difference>
    
    	at playground.HelloSpock.string length of #name should be #length(HelloSpock.groovy:10)


**Isn't it awesome?** Do widzenia TestNG - już do niczego nie jesteś potrzebny.
JUnitowym testom też już dziękuję, ale sam JUnit zostaje. Dzięki temu, że Spock
korzysta z runnera JUnit, nie trzeba się w ogóle męczyć z konfiguracją
maven-surefire-plugin. `mvn test` działa od razu. Do testów tylko Groovy. :-)

## Luke Daley, GEB - Very Groovy browser automation

Ciąg dalszy testowania w Groovym - tym razem w odstawkę idzie Selenium.

- *$('a #jquery > selectors[syntax]')*, *$('div', attr: value)*, *$('div', contains('GEB'))*
- *Spec* - user story rozbita na osobne metody, które odpalają się po kolei. Wyjątek podczas wykonania przerywa dalsze wykonywanie reszty metod testowych.
- testowanie w IE - Selenium Server Standalone na maszynie wirtualnej z Windowsem
- zapisuje zrzut ekranu, gdy test jednostkowy nie przejdzie


## Venkat Subramaniam, Pointy haired bosses and pragmatic programmers: Facts and Fallacies of Software Development

- to ludzie są krytyczną częścią developmentu - projekt upada przez ludzi, nie proces
- pytaj *dlaczego?* kilka razy - dopiero wtedy poznasz prawdziwą odpowiedź
- używanie oprogramowania tylko dlatego, że jest darmowe, to jak małżeństwo dla pieniędzy (nt. Eclipse VS IntelliJ IDEA)
- najpierw bądź innowacyjny, dopiero potem standaryzuj (nt. EJB 1.0 VS Ruby on Rails, Spring Framework)


## Robert C. Martin, The Three Laws of Test Driven Development

Think of yourself as a professional. Doctor tells you what to do. Doctor is your employee, he is professional, he knows. We developers are employes as professionals. We have to tell others what to do.

Any idiot can make it work. You are paid to continue it working long term at any time.

- We need to redesign. After that we are able to code fast.
- Why I would believe that? You made a mess once.

"We can't test this part because it's coupled with other parts of the system."<br/>
It never happens with test first approach.

You exactly know the line of code you want to write. But you are not allowed! Type the test that forces you to do this.

You don't want to be good at debugging. You should not bug. Time spent on debugging is much smaller with TDD.

Unit test is a small example of how system or library works. It's a document. A perfect document.

- Ready to ship? You sure?
- Yes, I trust my tests.

And I may change anything. Tests will tell if I fail. Test is my parachute.

## Robert C. Martin, Demanding Professionalism

Im większy jest koszt budowania, tym więcej potrzebnych jest planów up-front.
Np. budownictwo jest bardzo drogie. Budowanie software'u może być bardzo tanie,
jeśli tylko będziesz miał continuous delivery. Hardware poszedł 22 rzędy
wielkości do przodu. Nie masz prawa narzekać, że coś się długo buduje.
Napraw to!
