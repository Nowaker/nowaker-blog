---
title: I'm building VirtKick
---

Long time no write. It's been quite a long. Much happened in the meantime.
Long story short, today I've got two kiddos, and awaiting a third one.
And in the tech field, I completely abandoned Java for Ruby.
I was mainly building apps that manage servers somehow - from HTTP, through DNS to virtualization.
This all leads to where I'm today.

<!--more-->

<div class="text-center" style="margin-top: 30px; margin-bottom: 30px">
  <a href="/img/posts/virtkick-dashboard.png">
    <img src="/img/posts/virtkick-dashboard.png" alt="20,000$ collected for VirtKick" style="width: 100%; max-width: 600px;" />
  </a>
</div>

[VirtKick](https://www.virtkick.io/) simplifies creating, managing, hosting and providing virtual servers.
It's a self-hosted cloud panel, similar to DigitalOcean.
It's free to use by anyone, and code is on [GitHub](https://github.com/virtkick/virtkick).
I'm building it together with [the team](https://www.virtkick.io/community.html).

Curious to see VirtKick in action? Check out [hosted alpha](https://alpha.virtkick.io/) or
[give it a whirl](https://github.com/virtkick/virtkick-starter#virtkick-take-cloud-back) on your Linux desktop.
And if you like what we're doing, spread the word and support our
[crowdfunding campaign](https://www.indiegogo.com/projects/virtkick-take-cloud-back/x/8368808).




