---
title: Integracja Open Power Template 2.0 z KohanaPHP 2.2 / 2.3
---

Open Power Template w wersji 2.0 ma się coraz lepiej. Niedawno wyszła jego pierwsza wersja beta. Wraz z wersją dev8 zdecydowałem się wykorzystać OPTv2 w produkcji, ponieważ składnia została już zamrożona - jeśli wierzyć zapewnienion Zyx'a ;-).

<!--more-->

Odpowiedni moduł do integracji napisałem już wcześniej, jednak dopiero dzisiaj go publikuję. Tak samo, jak w przypadku modułu dla OPTv1, tak i dla OPTv2 składnia pozostaje zgodna z interfejsem oryginalnego View:

~~~~
$view = new View('szablon.tpl');
$view->dynamic = 'And this is a dynamic.';
$view->render(TRUE);
~~~~

Wybór silnika przetwarzania szablonów (natywny KohanaPHP lub OPT) dokonywany jest poprzez rozszerzenie szablonu. Wystarczy do nazwy szablonu dodać rozszerzenie OPT, aby właśnie OPT został użyty do wygenerowania widoku. Rozszerzenie można oczywiście określić w pliku konfiguracyjnym.

Moduł OPTv2:

+ [opt-v2-kohana-module.7z][7z]
+ [opt-v2-kohana-module.tar.bz2][bz]
+ [opt-v2-kohana-module.tar.gz][gz]
+ [opt-v2-kohana-module.zip][zip]
+ [przykładowa aplikacja][ex]
+ [licencja użytkowania][lic]

Uwaga. W wersji OPT2-beta1 jest mały błąd - OPT wyrzuca błąd typu notice podczas przetwarzania szablonu. Drobiazg, ale KohanaPHP ma domyślnie takie ustawienie, że nawet notice uznawany jest jako błąd (zresztą słusznie). W związku z tym mój moduł używa domyślnie OPT2-dev8. Dla tych, którzy chcą używać OPT2-beta1:

- usunąć katalog vendor/opt2
- zmienić nazwę katalogu vendor/opt2-beta1 na vendor/opt2
- zmienić error_reporting w pliku index.php na **E_ALL ^ E_NOTICE**

Stworzyłem też moduł obsługujący jednocześnie OPTv1 oraz OPTv2. Dla nowych użytkowników OPT nie jest on zalecany. Ja z niego korzystam, ponieważ szablony dla starszych modułów są napisane w składni OPTv1, zaś nowsze w OPTv2. Przepisać te dwieście plików do OPTv2? Nie, dziękuję. ;-)

Moduł OPTv1/v2:

+ [opt-v1-v2-kohana-module.7z][7zx]
+ [opt-v1-v2-kohana-module.tar.bz2][bzx]
+ [opt-v1-v2-kohana-module.tar.gz][gzx]
+ [opt-v1-v2-kohana-module.zip][zipx]
+ [przykładowa aplikacja][exx]
+ [licencja użytkowania][licx]



[7z]: http://www.kohanaphp.com.pl/modules/opt-v2/opt-v2-kohana-module.7z
[bz]: http://www.kohanaphp.com.pl/modules/opt-v2/opt-v2-kohana-module.tar.bz2
[gz]: http://www.kohanaphp.com.pl/modules/opt-v2/opt-v2-kohana-module.tar.gz
[zip]: http://www.kohanaphp.com.pl/modules/opt-v2/opt-v2-kohana-module.zip
[ex]: http://www.kohanaphp.com.pl/modules/opt-v2/opt-v2-kohana-module-example-application.7z
[lic]: http://www.kohanaphp.com.pl/modules/opt-v2/license.html

[7zx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/opt-v1-v2-kohana-module.7z
[bzx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/opt-v1-v2-kohana-module.tar.bz2
[gzx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/opt-v1-v2-kohana-module.tar.gz
[zipx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/opt-v1-v2-kohana-module.zip
[exx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/opt-v1-v2-kohana-module-example-application.7z
[licx]: http://www.kohanaphp.com.pl/modules/opt-v1-v2/license.html