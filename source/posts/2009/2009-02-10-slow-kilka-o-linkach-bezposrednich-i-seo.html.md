---
title: Słów kilka o linkach bezpośrednich i SEO
---

Bycie developerem to nie tylko tworzenie stron, ale także promocja. Przez promocję rozumiem pozycjonowanie serwisów tak, żeby osoby szukające informacji przez wyszukiwarkę trafiały do mnie. Niestety, w sieci jest na tyle dużo spamu, że samo budowanie dobrych, merytorycznych i przydatnych serwisów to połowa sukcesu.
 
<!--more-->

Pozycjonowanie stron jest koniecznością. Próbowałem różnych technik. Umieszczałem linki w katalogach, zamieszczałem słowa kluczowe w artykułach, dodawałem komentarze na forum i blogi z linkiem do siebie, optymalizowałem META. **Najlepsze rezultaty dawały linki bezpośrednie**.

Stąd też wpadłem na pomysł stworzenia [LinkBazar][lb]. Jest to platforma, na której zachęcam do handlu linkami bezpośrednimi. Współpracujemy w ramach programu partnerskiego z serwisem LinkLift. Oczywiście, złośliwi mogą powiedzieć, że nasza strona to fake - przykrywka programu partnerskiego. Nic bardziej mylnego. A oto moje argumenty.

1. Zarejestrowałem wszystkie swoje serwisy w LinkLift i występuję zarówno jako reklamodawca, jak i reklamobiorca. **I stąd są niezłe pieniądze**. 
2. Na LinkBazar wprawdzie zamieściłem linki referencyjne i za poleconych użytkowników dostaję pieniądze, ale ważniejsze są dla mnie **efekty pośrednie**.
3. **Efekt pośredni**  jest taki, że przybywa nowych serwisów zarejestrowanych na platformie. Dzięki temu powiększa się baza oferentów miejsca reklamowego i potencjalnych klientów. Serwisy te są zazwyczaj pokrewne tematyczne (wszak osoby rejestrowały się z mojego polecenia i były na mojej stronie o takiej, a nie innej tematyce). **Dzięki temu mam gdzie się reklamować i mam komu sprzedawać linki**. 
4. Największym działem w serwisie jest **baza artykułów SEO**. Przez 5 lat nabrałem doświadczenia w pozycjonowaniu stron i dzielę się nim z innymi.

**Czwarty argument jest dla mnie bardzo istotny**. Być może wiedza, którą przekazuję w artykułach, nie jest rewolucyjna dla profesjonalistów. Ale początkujący z pewnością zyskają. A nawet profesjonaliści czasem znajdą ciekawe rzeczy, które wynikły w trakcie mojej pracy przy pozycjonowaniu stron w postaci **case studies**. 

Skoro już jestem przy artykułach SEO, to ostatnio pojawiły się dwa nowe. W artykule [Linki tekstowe – jak stworzyć dobry i skuteczny link][link] piszę o linkach od podstaw – jak wygląda link od strony technicznej i jakie funkcje pełni na stronie. Najważniejszy jest oczywiście marketing oraz pozycjonowanie. Zastosowanie się do wskazówek zawartych w tekście może pomóc osiągnąć wysokie wskaźniki CTR i poprawić pozycję strony w wyszukiwarce. W drugim artykule [PageRank][pr], algorytm opracowany przez twórców Google jest podstawą wywodów na temat zasadności stosowania algorytmu wartościujących strony. Przedstawiam alternatywne algorytmy – Trust Rank, Alexa Rank, HITS. Piszę o wadach i zaletach. 

Kończąc ten wpis, zachęcam do przeczytania moich [artykułów na ten pozycjonowania][arty].

[lb]: http://www.linkbazar.pl/
[link]: http://www.linkbazar.pl/seo-pozycjonowanie/artykuly-seo/linki-tekstowe-jak-stworzyc-dobry-i-skuteczny-link.html
[pr]: http://www.linkbazar.pl/seo-pozycjonowanie/artykuly-seo/pagerank-co-to-jest.html
[arty]: http://www.linkbazar.pl/seo-pozycjonowanie/artykuly-seo.html