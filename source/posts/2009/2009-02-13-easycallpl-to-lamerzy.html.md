---
title: EasyCall.pl to lamerzy
---

Kilka dni temu musiałem wykonać dość długi telefon do Niemczech. Był to więc dobry moment na założenie sobie VoIP-a. Znajomy polecił mi EasyCall.pl, gdyż jest ich klientem od roku i nie narzeka.

<!--more-->

Jako, że jestem posiadaczem telefonu Nokia E51 z obsługą telefonii internetowej SIP, chciałem z tego zrobić użytek. Nie chcąc kupować usługi w ciemno, najpierw przeszukałem Internet w poszukiwaniu słów kluczowych EasyCall oraz Nokia E51. Znalazłem kilka instrukcji, jak skonfigurować telefon i podziękowania innych użytkowników, że konfiguracja działa. Uznałem więc, że mogę kupić usługę od EasyCall.

Niestety, okazało się, że żadne z ustawień dostępnych w Internecie nie działało. Na stronie internetowej EasyCall zaś nic nie ma na temat konfiguracji telefonów z obsługą SIP. Skontaktowałem się więc telefonicznie z Biurem Obsługi Klienta. Pan zasugerował, abym wysłał SMS-a Premium (2,44 zł), a otrzymam dane do konfiguracji. Jak to, za dane do konfiguracji muszę jeszcze **dodatkowo płacić**? Odpowiedziałem więc, że nie mam zamiaru wysyłać płatnych SMS-ów. Pan odesłał mnie do formularza kontaktowego na stronie mówiąc, że w odpowiedzi otrzymam dane do konfiguracji.

Zgodnie z obietnicą otrzymałem dane do konfiguracji, jednak kilka pól było dla mnie niejasne:

~~~~
Publiczna nazwa użytkownika: sip:nazwa@sip.easycall.pl
Nazwa użytkownika: nazwa sześciocyfrowa
Hasło: hasło szesciocyfrowe
~~~~

Co to jest *nazwa*? I czym są *nazwa sześciocyfrowa* i *hasło sześciocyfrowe*? W moim profilu na stronie nie ma takich informacji. Wysłałem więc kolejne zapytanie, czym są te dane, a Pan udzielił mi informacji, że poznam te dane, dopiero gdy doładuję swoje konto.

Konto doładowałem kwotą 15 zł. W zakładce "Moje konto" znalazłem nazwę użytkownika oraz hasło - obydwa sześciocyfrowe. Uzupełniłem konfigurację SIP, za *nazwę* wpisując swój login (nowaker) zaś za nazwę sześciocyfrową i hasło sześciocyfrowe nowo poznane dane.

Próba połączenia się z testowym numerem 904 wypadła pozytywnie. Dzwonię więc na telefon domowy rodziców, ale wita mnie błąd o treści **Adres nie w użyciu**. Idzie więc kolejny do BOK-u z pytaniem, aby upewnić się, że *nazwa* oznacza nazwę użytkownika (nowaker). Odpowiedź brzmi, że *nazwa* to *nazwa sześciocyfrowa*. No ludzie! Dlaczego jedna rzecz nazywana jest różnie? Poprawiłem...

Nadal nie działa. Tym razem wita mnie komunikat **Błąd połączenia**. Zgodnie z tradycją, wysyłam kolejne zgłoszenie do BOK-u. Otrzymuję odpowiedź, iż wysyłanie ID musi być włączone. Niestety, cały czas było. No to kolejny e-mail. Odpowiedź brzmi, iż mam wysłać SMS Premium.


Konkluzja całej sprawy brzmi **EasyCall to lamerzy**. 

- brak jakichkolwiek danych nt. konfiguracji telefonów na ich stronie internetowej
- wymuszanie ponoszenia dodatkowych kosztów
- niejednoznaczne instrukcje przesyłane e-mailem
- poroniony system ticketów - za każdym razem trzeba tworzyć nowy
- strona internetowa, po której nawigacja to koszmar

Nie polecam.