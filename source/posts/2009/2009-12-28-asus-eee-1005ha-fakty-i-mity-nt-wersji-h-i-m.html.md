---
title: Asus Eee 1005HA – fakty i mity nt. wersji H i M
---

Jakiś czas temu zastanawiałem się nad kupnem netbooka. Najważniejszym kryterium był dla mnie czas pracy na baterii, dlatego wybór padł na Asus Eee 1005HA. Problemem jednak okazały się całkowicie niespójne oznaczenia - 1005HA(M) i 1005HA(H). Ten wpis ma na celu podanie najważniejszych różnic pomiędzy wersjami 1005HA, aby potencjalni kupujący nie dali wprowadzić się w błąd. 

<!--more-->

W Internecie panuje zamieszanie związane z oznaczeniami. Objawia się tym, że każdy twierdzi coś innego nt. modeli H oraz M. Najpopularniejsze są dwa mity:

- **Mit 1**: wersja H posiada procesor Intel Atom N280, zaś M - N270.
- **Mit 2**: wersja H posiada pojemniejszą baterię od M<!--more-->

Generalnie, "lepszą" wersją jest H. Ale wcale nie oznacza to, że każda wersja H na pewno będzie "lepsza" od M, w kilku przypadkach może być mniej więcej taka sama.

Oznaczenie M/H wprowadza tylko mętlik, bo nie określa dokładnie wszystkich parametrów. Dokładnie określa wyłącznie kod producenta w postaci:

**1005HA-[BLK/WHI/...][ID], np. 1005HA-BLK059S**

To pierwsze to nic innego, jak kolor. ID określa dokładne, niezmienne parametry tej konkretnej wersji. To ID jest najważniejsze, a nie literka M/H, która w kodzie producenta nawet nie występuje (kto ją wymyślił?).

Poniżej przedstawiam listę kilku wersji wraz różnicami, które zauważyłem.

- ID = **059S** ma Atom N270, "największą" baterię, matrycę matową, dysk 250GB, ma BlueTooth. Oznaczona jako 1005HA(H).
- ID = **060S** ma Atom N270, "średnią" baterię, matrycę matową, dysk 160GB, nie ma BlueTooth. Oznaczona jako 1005HA(M).
- ID = **031S** ma Atom N270, "średnią" baterię, matrycę matową, dysk 160GB, nie ma BlueTooth. Oznaczona jako 1005HA(M).
- ID = **045S** ma Atom N270, "średnią" baterię, matrycę matową, dysk 250 GB, ma BlueTooth. Oznaczona jako 1005HA(H).
- ID = **135X** ma Atom N270, "średnią" baterię, matrycę matową, dysk 160 GB, ma BlueTooth. Oznaczona jako 1005HA(M). 
- ID = **126X** ma Atom N280, "największą" baterię, matrycę glare, dysk 160 GB, ma BlueTooth. Oznaczona jako 1005HA(H).
- ID = ... i wiele innych

Jak więc widać, mówiąc sobie H bądź M wcale nie przekazujemy informacji o procesorze, pojemności baterii, pojemności dysku, rodzaju matrycy oraz czy posiada BlueTooth. Jaki jest więc sens tego oznaczenia? Kto je w ogóle stworzył, skoro na [stronie producenta][oficjalna] nie znajdziemy takiego oznaczenia?

Starałem się, aby porównanie było jak najbardziej rzetelne. Specyfikacje dot. konkretnej wersji porównywałem w co najmniej dwóch różnych sklepach. Sam zaś gwarantuję poprawność specyfikacji 1005HA-059S, ponieważ taką właśnie posiadam.

Mity związane z wersjami H i M uznaję za obalone. Przy okazji postaram się jednak poruszyć kilka innych kwestii związanych z tym netbookiem.

## Atom N270 czy N280?

W ogólności (dotyczy to wszystkich netbooków), jeśli tylko jest to możliwe, warto wybrać procesor Intel Atom N280. Chociaż, że jest on o [3% wydajniejszy od N270][benchmark], jest to praktycznie nieistotne. Atom N280 zjada mniej prądu, co na "największej" baterii z 1005HA daje mu dodatkowe półtorej godziny pracy wg producenta.

Problem jednak pojawia się taki, że w modelu Asus Eee 1005HA ten procesor występuje **tylko i wyłącznie** z matrycą błyszczącą (glare). Różni ludzie lubią różne typy matryc. Pozostaję neutralny i tylko ostrzegam, że konsekwencją wyboru N280 jest wybór matrycy glare.

## Czas pracy na baterii wg producenta - prawda czy fałsz?

Mogę powiedzieć, jak to wygląda w przypadku modeli 1005HA-059S w systemie Ubuntu Linux (*ps aux | wc -l* = 150 po zalogowaniu się).

Czas pracy na baterii wg producenta to 9 godzin (bez WiFi). W rzeczywistości mogę wykonywać "lekką" pracę (przeglądanie stron internetowych, jakiś Notatnik) ok. 7,5 godziny. Ciężkie rzeczy, jak programowanie w NetBeansie to ok. 6,5 godziny. Wartości nie są z palca, lecz wzięte z *last | grep reboot*. Bez WiFi nigdy nie pracowałem, jednak nie zdziwiłbym się, gdyby w takim przypadku czas pracy zbliżył się do gwarantowanego przez producenta czasu.

## Jak mieć pewność, że otrzymam dokładnie taką wersję, jaką chcę?

Jeśli sklep nie podaje kodu producenta, tylko H i M, nie można mieć żadnej pewności. Pod tymi oznaczeniami kryje się tyle różnych konfiguracji, że nie wiadomo, co się dostanie.

Ja u sprzedawcy zamówiłem konkretny model wg kodu producenta (1005HA-059S, jeden z wariantów H) i poinformowałem sprzedawcę, że na fakturze ten kod musi się znaleźć. Jeśli otrzymam nieodpowiedni model, mogę go zwrócić i żądać wydania sprzętu zgodnego z dowodem sprzedaży.

## Nie chcę Windowsa

Firma Asus nie robi w kwestii zwrotu systemu Windows żadnych problemów. Jedyny warunek to zgłoszenie rezygnacji z systemu Windows w ciągu siedmiu dni od daty zakupu. Liczy się data wysłania pierwszego e-maila.

W moim przypadku dokumenty do wypełnienia dostałem jakoś po tygodniu od zgłoszenia rezygnacji. Poinformowali mnie najpierw, że muszą ustalić wartość Windowsa 7 Starter. Czyżbym był pierwszą osobą, która zgłosiła rezygnację z tego super systemu?

Chociaż, że zwracana kwota jest śmiesznie niska, doceniam sam fakt możliwości zwrotu. Ja otrzymałem 6,75$ zwrotu za Windows 7 Starter. [Na innym blogu][wariat] przeczytałem, że za Windows XP otrzymuje się 7€. Warto podjąć działanie choćby po to, aby w przyszłości móc kupić komputer bez narzuconego systemu operacyjnego. Wystarczy, że co drugi kupujący odda Windowsa - Asusowi przestanie się to opłacać. ;-)

## Na Linuksie wszystko działa?

Używam Ubuntu 9.10. Na tym systemie miałem tylko jeden problem. Gdy przez WiFi szły dane z szybkością 2 MiB/s, sieć przerywała transfery co ok. 10 sekund. Problemem okazał się sterownik, wystarczy zainstalować nowsze sterowniki z serii "backports" - *sudo apt-get install linux-backports-modules-wireless-karmic-generic*. Problem nie występował przy mniejszych transferach rzędu kilkuset KiB/s. Błąd występował również w Mandrivie i Fedorze (oba LiveCD, czy raczej LivePenDrive). Nie odnotowałem zaś tego błędu na OpenSuse - system został wypuszczony później, niż Ubuntu 9.10 i zawiera już w sobie nowy sterownik.

Wszystkie inne peryferia, jak czytnik kart pamięci, kamera internetowa i BlueTooth działają OOTB. Co do BlueTootha, to czasami psuje się gnomowy aplet. Nie psuje się za to program z pakietu *blueman*, oferując przy okazji lepszą funkcjonalność. Przy jego pomocy udało mi się nawet użyć telefonu Nokia E51 do połączenia z Internetem. Domyślny aplet nie ma możliwości użycia modemu z telefonu komórkowego i odpowiedniego skonfigurowania pppd. [Tutaj przeczytasz więcej o łączeniu się z Internetem za pośrednictwem komórki][restless].

Działa większość z przycisków funkcyjnych - te najważniejsze. Włączenie/wyłączenie WiFi, przyciemnienie/rozjaśnienie podświetlenia wyświetlacza, kontrola głośności (o ile się nie wyrzuci znienawidzonego przeze mnie PulseAudio). Nie działają takie, których znaczenia nie jestem w stanie się domyślić, patrząc na ikonkę. 

Netbook posiada kartę graficzną Intela. Na niektórych kartach były [problemy z DRI i niedziałającym xrandr][intel] (więc i gnome-display-settings). Tutaj nie występuje ten problem - można podłączyć drugi monitor 24'' i cieszyć się wirtualnym obszarem roboczym o szerokości powyżej 2048px.

Podstawowe efekty graficzne w Gnome, które wykorzystują Compiza działają płynnie. Efekty określone mianem "dodatkowe" również, jednak nie podoba mi się falujące okno podczas przenoszenia go w inne miejsce ekranu.

Uwaga, nie instalować programu eee-control i jego pochodnych (nie ma go w repo, jest dostępny gdzieś w sieci). Jego zainstalowanie dodaje bardzo niewiele ficzerów, psuje z kolei klawisz włączający WiFi. :-)

[oficjalna]: http://asus.com.pl/product.aspx?P_ID=BtksJTDENqfsnuyf
[wariat]: http://wariat.jogger.pl/2009/10/01/ile-kosztuja-windowsy/
[benchmark]: http://www.tomshardware.com/forum/261356-28-speed-difference-atom-n270-n280-other-questions
[intel]: https://bugs.launchpad.net/ubuntu/+source/mesa/+bug/146859
[restless]: http://restlessbeing.pl/blog/2009/08/21/xubuntu-904-i-mobilny-internet/
