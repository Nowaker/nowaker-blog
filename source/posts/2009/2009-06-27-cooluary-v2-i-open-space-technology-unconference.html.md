---
title: COOLuary v.2 i Open Space Technology (Unconference)
---

Dzisiaj uczestniczyłem w pierwszej części konferencji [COOLuary v.2][cool]. Nim jednak podzielę się wrażeniami, opiszę samą ideę konferencji tego typu.

<!--more-->

Najczęściej konferencje są zwykłymi wykładami. Prelegent, nim przejdzie do meritum sprawy, mówi dużo o sobie. Potem zaczyna mówić o swojej firmie. Dopiero po wielu pustych słowach zaczyna się coś rzeczowego. Niewielu też się odważy zadać jakieś pytanie w trakcie trwania wykładu. Nie mówiąc już o wyrażeniu swojej przeciwnej opinii w stosunku do tego, o czym mówi prelegent. W efekcie, wykład jest najmniej interesującym punktem programu. Najważniejsze okazują się przerwy między wykładami - tylko rozmowami prowadzonymi w kuluarach face to face można dowiedzieć się czegoś ciekawego, poznać ciekawych ludzi, czy wymienić poglądy.

Cel, jaki przyświeca „antykonferencjom”, nazywanymi przez Bruce'a Eckela jako Open Space, to prowadzenie rozmów znanych z kuluarów zwykłych konferencji. Uczestnicy sami decydują, na jaki temat chcieliby porozmawiać. Na dzisiejszych COOLuarach były cztery sale i pięć sesji. Na samym początku spotkania proponowane były tematy rozmów, spisywane na żółte karteczki post-it i przyczepiane do którejś sali i sesji. Po ustaleniu agendy rozpoczęła się pierwsza konferencja.

Na konferencji każdy jest równy - nie ma podziału na prelegenta i słuchaczy. Każdy może zadać pytanie, czy też wyrazić swoją opinię. Mówi każdy, kto ma coś do powiedzenia. Nie ma miejsca na opowiadanie o sobie więcej niż potrzeba, jak to w zwykłej rozmowie. Jeśli temat Cię nie interesuje, możesz (a nawet musisz) w każdej chwili opuścić salę i przyłączyć się do innej rozmowy.

Idea jest świetna, a co z wykonaniem? Otóż, również było świetne. ;-) Naprawdę nie żałuję wydanych pieniędzy. Dowiedziałem się naprawdę ciekawych rzeczy, których nie znalazłbym w żadnej książce i na żadnym blogu. Poznałem też ludzi, którzy bardzo chętnie dzielili się ze mną swoją wiedzą.

W sumie była to moja pierwsza konferencja dla programistów Javy. Większość osób uczestniczących w spotkaniu to fachowcy z branży. Część problemów, o jakich rozmawiali nigdy nie dotknąłem. Pomimo, że dzieliła nas ogromna przepaść w posiadanej wiedzy, czułem się swobodnie za sprawą miłej atmosfery i bycia ze wszystkimi na równi. A w temacie analizy potrzeb klienta powiedziałem nawet trochę od siebie.

Na zakończenie dziesiejszej części COOLuarów odbyło się losowanie. Pierwszy raz w życiu coś wygrałem - wejściówkę na tegoroczny Java Developers' Day. Dobrze, że nie było tam [Zala][zal], bo wtedy nie miałbym szans na wygraną ;-) Pięć osób wyszło z książkami Helionu, m.in. kolega z mojej przyszłej pracy [Wojtek Seliga][spartez]. Otrzymaliśmy też kupony rabatowe 20% na książki Helionu oraz koszulki. Jutro kolejna część COOLuarów, tym razem warsztatowa.

P.S. Pozdrowienia dla wszystkich uczestników.


[cool]: http://dworld.pl/COOLuaryV2/
[zal]: http://blog.4zal.net/
[spartez]: http://www.spartez.com/pl/ludzie.html
