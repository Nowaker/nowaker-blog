---
title: Cała prawda o pseudointeligentnym budynku wydziału PG ETI
---

Dzisiaj przypadkowo trafiłem na stronę, [na której wychwalano budynek mojego wydziału][mur]. Osoba, która nie studiuje na ETI mogłaby naprawdę uwierzyć, że ten budynek jest wręcz perfekcyjny. Niestety, tak nie jest. Ten wpis będzie stanowił uzupełnienie [artykułu na MuratorPlus.pl][mur] o negatywne strony budynku nowego ETI.

<!--more-->

Zacznijmy od audytoriów. Fajnie by było, gdyby na audytoriach siedziało się wygodnie. Z zewnątrz audytorium wygląda bardzo ładnie, jednak wystarczy posiedzieć 20 minut, by przekonać się, że na tych siedzeniach nie idzie po prostu usiedzieć. Stare, komunistyczne audytorium w gmachu wydziału Oceanotechniki, choć ciasne i obskurne, jest wygodniejsze niż to nowe. Bardzo dużo osób zgadza się ze mną, że na audytoriach nie idzie usiedzieć. W dodatku odległość siedzenia od blatu jest zbyt duża - nie idzie pisać w zeszycie, będąc opartym. Można pisać na kolanie, ale chyba nie o to chodzi.

Warto by też zadbać o mazaki. Mazaki na audytoriach i w salach lekcyjnych są prawie zawsze zużyte i tylko pierwsze rzędy mogą cokolwiek przeczytać. Jednak mazaki to drobnostka w porównaniu z nagłośnieniem. Nagłośnienie audytoriów jest po prostu słabe. Jeden wielki głośnik z przodu nie nagłośni wystarczająco całego audytorium. Cicho mówiących wykładowców praktycznie nie słychać w tylnych częściach audytorium. Porównując nagłośnienie do Uniwersytetu Gdańskiego, gdzie miałem przyjemność pisać egzamin FCE, Politechnika wypada bardzo mizernie. Na Uniwerku głośniki znajdowały się w suficie (mnóstwo ich) i w każdym miejscu audytorium głos był jednakowo słyszalny. Pisanie egzaminu w części Listening było przyjemnością. Na ETI nie byłoby to niczym przyjemnym.

Ostatecznie, gdy rozładują się baterie w mikrofonie, w biurku wykładowcy na audytorium nie znajdują się zapasowe baterie. Wykładowca musi iść na dół na portiernię i poprosić o nowe baterie... oczywiście tego nie zrobi, bo zabierze to 5 cennych minut. Dalsza część wykładu odbywa się więc bez mikrofonu. A wysztarczyło umieścić w biurku zapasowe baterie.

Nie tylko audytoria są niewygodne. Krzesła w salach lekcyjnych są również niewygodne - mają tak małe oparcia, że tylko dzieci przedszkolne siedziałyby na nich wygodnie. W dodatku większe osoby nie mieszczą się w ławkach - krzesła są zbyt wysokie, a ławki mają zbyt nisko blat. Efektem jest zgnieciona górna część nóg. I znowu stare, 20-letnie krzesła i ławki okazują się wygodniejsze, niż te nowe.

W budynku działa sieć bezprzewodowa WETI_PG. Szkoda tylko, że moc sygnału jest tak słaba, że korzystanie z Internetu w audytoriach jest utrudnione, a czasem niemożliwe. W tak nowoczesnym budynku zapomniano też o tym, by laptopy wykładowców miały dostęp do Internetu (te laptopy, które stoją na stałe w audytoriach). W bibliotece znajduje się ok. 30 stanowisk komputerowych. Dobrze by było, ażeby był na niej dostęp do Internetu. W ostatnim tygodniu na moje trzy wizyty, dwa razy sieć nie działała.

Gdyby budynek nosił miano inteligentnego, musi w nim panować normalna temperatura. W laboratoriach komputerowych często jest albo zbyt gorąco albo zbyt zimno. Sporadycznie zdarza się wąchać smród z klimatyzacji. Ewenementem jest biblioteka, w której zawsze jest tak gorąco, że chciałoby się po prostu siedzieć na gołej klacie.

I jeszcze mały szczegół, w sumie mało ważny, ale zawsze - drzwi frontowe, jak i boczne otwierają się bardzo wolno.

Myślę, że projektując ten superinteligentny budynek zapomniano o rzeczach podstawowych - budynek ETI powinien być przede wszystkim **wygodny**, a dopiero potem funkcjonalny i *zbajerowany*.


[mur]: http://www.muratorplus.pl/technika/inteligentny-budynek/inteligentny-wymiar-architektury_63336.html
