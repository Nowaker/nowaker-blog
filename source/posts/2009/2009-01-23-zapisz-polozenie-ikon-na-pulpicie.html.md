---
title: Zapisz położenie ikon na pulpicie
---

Denerwuje Cię, że Windows czasami resetuje Ci położenie wszystkich ikon pulpitu?

<!--more-->

- pobierz [ten plik][plik]
- skopiuj layout.dll do katalogu windows/system32
- uruchom layout.reg, który doda kilka wpisów do rejestru

Teraz klikając PPM na ikonie systemowej (np. Mój komputer) dostępne będą dwie opcje, które niejednokrotnie uratują nam tyłek :)

![Zapisanie położenia ikon na pulpicie](/img/posts/zapisany-wyglad-pulpitu.png)

Źródło:

+ http://www.msni.it/faqreg/saveicon.html

[plik]: http://www.wintricks.it/faqreg/regconf/saveicon.zip
