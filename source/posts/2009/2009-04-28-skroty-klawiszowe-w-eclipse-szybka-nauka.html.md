---
title: Skróty klawiszowe w Eclipse – szybka nauka
---

Zawsze miałem problem z nauką skrótów klawiszowych w dużych środowiskach programistycznych typu Eclipse, czy NetBeans. Gdy skrótów jest zbyt wiele, ciężko jest zapamiętać więcej niż kilka z listy. Z pomocą przychodzi jednak plugin MouseFeed - jeśli użyjesz skorzystasz z jakiejś funkcji myszką, program podpowie Ci, w jaki sposób można wykonać dokładnie to samo z klawiatury. Podpowiedzi udzieli w sposób dość dyskretny - nie przeszkadza, jeśli wykonanie czegoś myszką było rzeczywiście zamierzone. Największym plusem takiego rozwiązania jest to, że nie uczysz się skrótów, których nigdy nie użyjesz, lecz tych, których często używasz. Szkoda tylko, że plugin nie wszystko wyłapuje (warto czekać na aktualizacje), jednak i tak ogromny plus dla autora za pomysł i jego realizację.

- Link do pobrania: [http://update.mousefeed.com/](http://update.mousefeed.com/)
- Strona projektu: [www.mousefeed.com/](http://www.mousefeed.com/)

![](/img/posts/mousefeed.png)