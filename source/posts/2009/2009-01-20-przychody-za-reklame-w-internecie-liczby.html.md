---
title: Przychody za reklamę w Internecie – liczby
---

Niedawno Tomek [zamieścił na blogu Polish Words][pw] krótkie podsumowanie zysków z reklam na jego blogu. 
Trochę się zdziwiłem relatywnie wysokimi wynikami z reklam kontekstowych, a niskimi z programów partnerskich.

<!--more-->

Oto krótkie podsumowanie jego wyników, przeliczone:

- zagraniczna reklama kontekstowa - 125 zł / 100 000 odsłon
- polska reklama kontekstowa - 33,30 zł / 100 000 odsłon
- reklamy partnerskie (bannery?) - 4,54 zł / 100 000 odsłon
- programy partnerskie - 100 zł / 100 000 odsłon

U mnie zagraniczna reklama kontekstowa przynosiła zyski na poziomie 63 zł / 100 000 odsłon, zaś polska - 13,40 zł. Chciałbym tylko dodać, iż moje zyski nie biorą się z blogów, tylko z „normalnych” stron internetowych.

Przypatrzmy się programom partnerskim, które najbardziej cenię. Tutaj niestety nie potrafię określić zysku za każde 100 000 wyświetleń. Według mnie zysk wcale nie zależy tutaj od odwiedzalności.

## Linki tekstowe

W programie [sprzedaży płatnych linków tekstowych][linklift] uczestniczy dziewięć moich różnych stron. Uczestniczę również w programie partnerskim, pozyskując nowych uczestników do tego programu poprzez [LinkBazar.pl][linkbazar]. Działalność ta przynosi dość spore zyski. Listopad zakończył się wynikiem 174,20 zł, zaś grudzień 204,20 zł. W aktualnościach LinkBazar [na bieżąco][news1] [informuję][news2] o osiąganych zyskach, aby zachęcić innych do uczestnictwa.

Otrzymywane wynagrodzenie nie zależy tutaj od ilości odsłon danej strony, lecz (w ogólności) od jego PageRanku. Wbrew pozorom PageRank strony nie jest zależny liniowo od ilości odsłon danej strony. W swoim zapleczu posiadam strony o relatywnie wysokich PR (3/10) i bardzo małej odwiedzalności (np. 20 wizyt dziennie), jak i całkowicie odwrotnie.

## Program partnerski mBank

Konta bankowe w mBanku reklamuję raz na kilka miesięcy w malingach do użytkowników [portalu geograficznego GeoZone][geozone]. Przyniosło to średni zysk 400 złotych na około 100 000 wysłanych reklam.

Reklamy wysyłałem różne - zarówno konto bankowych, jak i sieci komórkowej mBanku. Zdecydowanie najlepsze wyniki przynoszą konta bankowe; telefonią zaś nikt się nie interesuje.

## Podsumowanie

Widać jak na dłoni, że najważniejsze przychody generują mądre programy partnerskie. Wiadomo, iż nie każdy program partnerski sprawdzi się na konkretnej stronie. Cytując Tomka, aby zarabiać, najpierw trzeba się dużo napracować. Nie każdy program partnerski się nada, jednak jeśli nie spróbujesz - nie dowiesz się, czy jest dochodowy.

[pw]: http://polishwords.com.pl/blog/2009/zarabianie-na-blogu-liczby/
[linklift]: http://www.linklift.com.pl/
[linkbazar]: http://www.linkbazar.pl/
[news1]: http://www.linkbazar.pl/aktualnosci/przyspieszamy-tempa.html
[news2]: http://www.linkbazar.pl/aktualnosci/utrzymujemy-tendencje-wzrostowa.html
[geozone]: http://www.geozone.pl/