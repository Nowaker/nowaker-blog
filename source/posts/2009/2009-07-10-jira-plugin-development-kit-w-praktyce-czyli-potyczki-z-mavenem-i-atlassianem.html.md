---
title: JIRA Plugin Development w praktyce – potyczki z Mavenem i Atlassianem
---

Ostatnio [w pracy][spartez] miałem się zająć rozwinięciem pluginu do [Atlassian JIRA][jira], który ma wspomagać pracę zespołu deweloperów w metodologii Scrum (agile). Zadanie - od strony technicznej - było bardzo ciekawe, ponieważ dowiedziałem się, w jaki sposób robi się pluginy do JIRY oraz przy okazji mogłem poznać wycinek architektury dużego systemu, jakim jest JIRA. Miałem również możliwość poznania Mockito, ponieważ nie dało się napisać testów jednostkowych JUnit operujących bezpośrednio na całej Jirze. Nie miało to nawet sensu.

[spartez]: http://www.spartez.com/
[jira]: http://www.spartez.com/pl/atlassian.html
<!--more-->

Niestety, najwięcej czasu wcale nie zabrała kreatywna praca, lecz zmagania z Mavenem i Atlassianem. Problem polegał na tym, że instrukcje na stronie Atlassianu dotyczące przygotowania pustego projektu pluginu w Jirze są nieaktualne. Problemem na samym początku było pobranie wszystkich zależności.

Najpierw wystąpił problem z samym projektem - artefakt jira-plugin-base w wersji 13 nie występował w repozytoriach Atlassianu. Pomimo tego, że plik ~/.m2/settings.xml utworzyłem według wytycznych Atlassianu, proces pobierania zależności wysypywał się. Przy zatrzymaniu się procesu pobierania zależności każdorazowo musiałem szukać nieznaleziony plik z pomocą Gogla i dopisywać nowe repozytorium w settings.xml. Czynność wbrew pozorom frustująca, ponieważ po drodze wynikały inne problemy - dla pewnych plików jedno repozytorium wysyłało permanentne przekierowanie pod inny adres, który okazywał się być 404 Not found. Maven z kolei już tego nie rozumiał i stronę 404 uznawał jako plik XML z danymi nt. paczki. Powstawały dziwne błędy trudne do zlokalizowania, na które tylko pomagał rm -rf na katalogu z błędnymi danymi.

Kolejnym bublem było serwowanie przez jeden z serwerów Atlassianu uszkodzonych paczek z danymi. Takie popsute dane również musiałem ręcznie usuwać i rozpoczynać proces pobierania zależności od nowa - w nadziei, że tym razem wszystko pójdzie gładko. Rozwiązaniem okazało się usunięcie z listy repozytoriów tego feralnego repozytorium Atlassianu, które serwowało uszkodzone paczki. Oczywiście na samym początku procesu pobieranie ten serwer musiał widnieć na liście repozytoriów, bo inaczej nie pobralibyśmy pewnych danych, które tylko na tym serwerze można znaleźć.

W „zaledwie” kilka godzin poradziłem sobie z tymi wszystkimi zależnościami i ściąganiem połowy Internetu na dysk. Odpaliła się wersja demonstracyjna JIRY, na której można było rozwijać plugin. Byłem wniebowzięty. Moje szczęście nie trwało jednak długo - przyszedł czas na zbudowanie paczki JAR z pluginem. Atlassian na swojej stronie mówi „*wpisz mvn jar, aby zbudować plik JAR*”. Maven na to mówi, że zadanie o nazwie jar nie istnieje. Sławek, kolega z pracy, proponuje spróbować *mvn package*. Zadziałało...

Rady dla przyszłych plugin developerów dla Atlassian JIRA:

- skorzystaj z mojego pliku **[settings.xml](/wp-content/uploads/2009/07/settings.xml)** i przeczytaj komentarze wewnątrz niego
- nie bój użyć się Gogla w celu znalezienia innych repozytoriów
- nie bój się usuwać katalogów z uszkodzonymi plikami w lokalnym repo Mavena
- nie bój się zadać pytania w komentarzach, może uda mi się odpowiedzieć ;)
