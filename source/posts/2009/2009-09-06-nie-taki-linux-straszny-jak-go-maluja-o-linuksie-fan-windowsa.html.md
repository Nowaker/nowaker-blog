---
title: Nie taki Linux straszny, jak go malują
---

Moi znajomi znali mnie dotychczas z tego, że lubiłem system Windows do tego stopnia, że nigdy nie przesiadłbym się dobrowolnie na Linuksa do codziennej pracy. Cóż mogę mieć na Linuksie, jeśli tylko kłopoty z niedziałającymi urządzeniami oraz konfigurowaniem wszystkiego w plikach konfiguracyjnych o zakręconej składni?

<!--more-->

Pierwszy kontakt z Linuksem na desktopie miałem około 7 lat temu. Był to Aurox Linux, dystrybucja ponoć dostosowana do polskich realiów. Tę przygodę zapamiętałem za sprawą problemów ze sprzętem. Myszka i inne urządzenia USB przestawały działać po wejściu w program wyświetlający graficznie podział dysku na partycje. Po roku miałem też kontakt z Red Hatem, który również mnie nie poruszył. Wtedy ostatecznie pożegnałem się z Linuksem na moim domowym komputerze, uznając go za system dla ludzi, którzy lubią tracić czas na robienie czegoś, co w Windowsie działa od początku albo można bardzo prosto osiągnąć.

Nadmienię, że uznając Linuksa jako system nienadający się na komputer domowy, nie odrzuciłem go jako systemu na serwery. Przeciwnie, na kupionym przeze mnie serwerze rackowym (połowa roku 2007) zainstalowałem Slackware Linux. Poruszałem się w mrocznych klimatach basha (później zsh) dość dobrze i ceniłem sobie stabilność i przewidywalność takiego „serwerowego” Linuksa. Windows byłby ostatnim systemem, jaki postawiłbym na serwerze. Niemniej jednak, na komputer domowy Linuksa nie uznawałem.

## Niespodziewany zwrot akcji

Historia zaczęła pisać się na nowo wraz z rozpoczęciem pracy w firmie [Spartez](http://www.spartez.com/). Dostałem jakiegoś blaszaka oraz polecenie przygotowania sobie środowiska pracy. Początkowo chciałem sobie zainstalować FreeBSD, które to kolega [kwiat](http://kwiat.jogger.pl) wręcz miłuje, lecz poszedłem łatwiejszą drogą. Jeden z kolegów w pracy miał przy sobie płytę instalacyjną Ubuntu 9.04, więc nie kombinowałem i zainstalowałem, co było.

Pierwsze wrażenie było pozytywne, ale ono jest tak naprawdę najmniej znaczące - liczy się to, co będzie później. Nie miałem czasu na zbytnie dostosowywanie Gnoma do swoich potrzeb i przyzwyczajeń. Przeleciałem szybko po wszystkich apletach w preferencjach i ustawiłem najważniejsze rzeczy. Potem zainstalowałem dodatkowe oprogramowanie, głównie z repozytorium, ale też trochę zewnętrznego. I zacząłem pracę.

## Miesiąc później...

Kolejny dzień w pracy, kolejne rzeczy do zrobienia. Linux sobie nadal stoi i w pełni zastępuje mi Windowsa. Nie powoduje mi przy tym żadnych problemów. W domu zaś troszkę brakuje mi fajnych apletów z Gnoma, apt-geta jako banalnego sposobu na szukanie i instalowanie oprogramowania oraz konsoli, w której można wykonać rzeczy, które graficznie robiłoby się kilka razy dłużej. Coraz bardziej myślałem o migracji...

## Linux w domu

Po 1,5 miesiąca praktycznie codziennej pracy z Ubuntu 9 byłem już na tyle przekonany do tego systemu, że zdecydowałem się na migrację na Linuksa w domu. W domu mam dość nowoczesny sprzęt i obawiałem się, że może nie być tak kolorowo, jak było w pracy. Uruchomiłem więc najpierw system z LiveCD, jednak wszystko wydawało się poprawnie działać. Natrafiłem tylko na jeden problem - przy pracy z dwoma monitorami okienka działały dość wolno. Porównywalnie do Windowsa bez sterowników karty graficznej. Nie przejąłem się tym jednak - myślałem, że może to być wina odpalenia systemu z płyty CD. Po instalacji problem nadal występował, jednak dzięki pomocy kolegi [whr-a](http://whr.ukraina.edu.pl/), zapalczywego maniaka Emacsa, zainstalowałem najnowszy sterownik Intela, dostowałem xorg.conf i na tym skończyły się problemy.

W domu Linux również się sprawdza. Oprogramowania zastępczego również jest dużo, znakomitą większość udało mi się zastąpić równoważnym pod względem funkcjonalności oprogramowaniem. Tylko dla jednego programu nie znalazłem substytutu - programu pocztowego The Bat!, który towarzyszy mi już co najmniej 10 lat, i oferuje funkcjonalność, o jakiej KMail, Evolution i Thunderbird nawet nie śniły. Program pocztowy to w zasadzie wrota do mojego miasta, krytyczny element. Z tym fantem poradziłem sobie za sprawą VirtualBoksa i na trzecim obszarze stoi sobie uruchomiony Windows z moim The Batem.

Dodatkowo, dzięki repozytorium i menedżerowi pakietów Synaptic znalazłem kilogramy dodatkowego oprogramowania, o którym nawet nie myślałem wcześniej, a wpadłem na nie przez przypadek. Przykłady: klient Twittera Gwibber, ekranowa linijka, czy program do zaparzania herbaty ;-)

## Podsumowanie

Osobista lekcja: *Jeśli coś banujesz, banuj na określony czas - nigdy permanentnie - możesz dużo stracić*. Przez 7 lat różnica między Linuksami okazała się być ogromna. To, co kiedyś słabo działało, teraz działa dobrze, a przy okazji jest bardziej funkcjonalne i elastyczne. Jeśli więc Linux na biurku nie spodobał Ci się kilka lat temu, wypróbuj go teraz. Jeśli teraz Ci się nie spodoba, wypróbuj za kilka lat.
