---
title: Bitwa programistów, czyli TDD Randori Session
---

Czy widzieliście walczących programistów? Jeśli nie, to wybierzcie się kiedyś na sesję randori. Na czym to polega? Otóż, dwóch programistów siedzi przed jednym komputerem. Jeden z nich pisze kod, drugi zaś go kontroluje i ewentualnie podpowiada. Zasada jest taka, że jeśli jeden pisze test jednostkowy, to drugi pisze implementację. W myśl TDD (test driven development) test jednostkowy pisany jest jako pierwszy. Potem drugi programista pisze tylko tyle implementacji, by zapalił się zielony kolor - czyli przejść nowy test jednostkowy oraz wszystkie poprzednie. Gdy test zakończy się sukcesem, pierwszy programista refaktoryzuje kod kolegi - obserwator widzi zawsze trochę więcej, niż robotnik. ;-)

<!--more-->

W pokoju znajduje się również widownia, która patrzy, czym zajmują się w danym momencie. Nie stoją oczywiście wszyscy za plecami dwóch programistów, ale śledzą wszystko na rzutniku. Dopóki nie pali się zielone światło, widownia nie ma prawa komentować ich działań - może tylko odpowiadać na pytania zadane przez nich. Dopiero, gdy testy jednostkowe zakończą się powodzeniem, widownia może zadawać swoje pytania oraz wyrażać opinie na temat jakości kodu.

Każda para testu i implementacji zostaje zaakceptowana, gdy wszyscy uczestnicy nie będą mieli zastrzeżeń. Wtedy można przejść do pisania kolejnego testu jednostkowego i implementacji. Dodatkowo, co 5-10 minut następuje zmiana par - obecna para udaje się na widownię, a do komputera zasiada inna para.

Po co takie podejście? Po pierwsze, można nauczyć skupiać się na chwilowym celu. Jeśli muszę napisać implementację, która ma przejść test - robię to i tylko to. Nie myślę, czy za chwilę będę musiał rozszerzyć jakąś metodę o nową funkcjonalność. Tym zajmę się, gdy przyjdzie na to czas. Kolejnym plusem jest jakość kodu - zawsze pokryty testami oraz wstępnie refaktoryzowany. Kolega zawsze poprawi jakiś bakcyl albo złą praktykę, bo siedząc obok widzi po prostu więcej.

Poza tym, dobrzy i doświadczeni programiści potwierdzają skuteczność tego podejścia - informacja wyniesiona prosto z COOLuarów. ;-) Ale ostrzegają, że programowanie w parach bardzo szybko obnaża braki w umiejętnościach któregoś z programistów.

P.S. Podziękowania dla [Łukasza Lenarta][lenart] za poprowadzenie sesji TDD randori oraz poprawki merytoryczne.

[lenart]: http://www.lenart.org.pl/