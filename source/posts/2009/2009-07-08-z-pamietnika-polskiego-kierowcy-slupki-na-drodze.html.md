---
title: Z pamiętnika polskiego kierowcy – słupki na drodze
---

Prawie dwa tygodnie temu postanowiłem odwiedzić [mojego kolegę i wspólnika][kolo], który mieszka w Warszawie.
Jako, że miałem do załatwienia coś po drodze w Tczewie, postanowiłem pojechać nową autostradą do końca,
dalej drogą krajową numer 1 do Łysomic przed Toruniem, by tam odbić w stronę Warszawy.

<!--more-->

Jazda autostradą z tempomatem na pokładzie to sama przyjemność. Do tego Korpiklaani i można jechać. :-)
Oczywiście wszystko, co dobre kończy się bardzo szybko i dalsza droga nie była już tak przyjemna.
W Pomorskiej Specjalnej Strefie Ekonomicznej przyszło mi stać w półgodzinnym korku.
Przyczyna korka nie była zdarzeniem losowym - to tylko światła w Łysomicach.
Po co ulepszać skrzyżowanie? Przecież lepiej zostawić tak, jak jest. 

Gdy już odbiłem w kierunku Warszawy, niewiele kilometrów dalej spotkała mnie kolejna niespodzianka.
Roboty na drodze to nic złego, pod warunkiem, że rzeczywiście się coś dzieje.
Ruch na kilkudziesięciokilometrowym odcinku był wahadłowy - jedna strona jezdni była zdarta z asfaltu.
Na jeden kilometr drogi przypadło nie więcej niż dwóch robotników. Polska organizacja pracy - bezcenna. ;-) 

Po ok. 50 km wahadłowego ruchu w końcu przyszedł czas na normalniejszą jazdę (wszak nadal nienormalną, bo to nie
autostrada). Zdziwiły mnie jednak wysepki na drodze, a raczej słupki, które znajdowały się przed nimi.
Zastanawiałem się, do czego miałyby służyć te słupki - na tamten moment odpowiedzi nie znalazłem. 

<a href="http://motoryzacja.interia.pl/wiadomosci/bezpieczenstwo/news/przejaw-urzedniczej-glupoty,1247943"><img alt="Słupki na drodze" src="http://img.interia.pl/motoryzacja/nimg/c/6/Przejaw_urzedniczej_foto0_3103133.JPG" title="Słupki na drodze" width="440" height="294" /></a>

Jazda w samej Warszawie była przyjemna. Szerokie, trójpasmowe drogi przelotowe i estakady bardzo ją umilały.
To śmieszne, że na cały Gdańsk mamy łącznie 10 estakad. Na Ursynowie drogi międzyosiedlowe są dwupasmowe.
Fajnie, bo Gdańsk to nie ma nawet dwupasmowej drogi dojazdowej. Słowackiego kończy się na Niedźwieniku,
zaś Armii Krajowej "uchodzi" w Kartuskiej, zamiast połączyć się bezpośrednio z obwodnicą.

Wracając do tematu słupków - zrozumiałem jednak, do czego one służą podczas powrotu do Gdańska.
Wyprzedzałem ciężarówkę, droga wolna, więc ciach na drugi pas. Będąc na wysokości kabiny kierowcy oczom mym ukazała się
wysepka w oddali i słupki blisko mnie. Wysepka była tak daleko, że zdołałbym wyprzedzić jeszcze jeden samochód osobowy i
wrócić na swój pas przed wysepką - gdyby nie głupki oczywiście. Teraz mogę powiedzieć, że słupki mają na celu
uniemożliwienie kierowcy powrotu na swój pas przed wysepką. Wróciłem na swój pas za wysepką.
*Miałem serce w przełyku*, jak to śpiewał Kazik, *lecz nic mi się nie stało*.

Gdy w ten weekend również jechałem do Warszawy, przyglądnąłem się bardziej tym słupkom.
Słupki zaczynały się od razu na linii ciągłej. Jeśli mylę się, to mnie poprawcie - ale zdaje się, że kierowca, który
rozpoczął manewr wyprzedzania na linii przerywanej ma prawo dokończyć manewr i wrócić na swój pas mimo linii ciągłej.
Te słupki pozbawiają kierowcy tego prawa. Co więcej, w jednym miejscu słupek stał pół metra **przed** linią ciągłą.
Ręce opadają - powiesiłbym tego, kto to wymyślił. Nieważne za co.

(zdjęcie pochodzi ze strony motoryzacja.interia.pl - kliknięcie zdjęcia powoduje przejście na stronę, na której się oryginalnie znajdowało)

[kolo]: http://www.koloroweru.geozone.pl/
