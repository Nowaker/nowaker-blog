---
title: Eclipse i Java Micro Edition (JME), czyli zabawy z komórkami – krok po proku
---

Napisanie pierwszego programu na komórkę w JME (kiedyś J2ME) może być proste, pod warunkiem, że mamy dobre środowisko. Nie da się bowiem pisać programu, nie mając chociażby emulatora komórki pod ręką - najlepiej zintegrowanego z środowiskiem programistycznym (IDE). Wpis ten stanowi krótki poradnik dla osób, które chciałyby napisać swój pierwszy program w Javie ME na komórkę przy użyciu Eclipse.

<!--more-->

Najpierw należy zainstalować pakiet WTK - Sun Wireless Toolkit. Zawiera on między innymi emulatory. [Można go pobrać tutaj](http://java.sun.com/products/sjwtoolkit/download.html), klikając przycisk, który znajduje się pod koniec strony.

Teraz uruchom swojego Eclipse'a (jeśli go nie masz - pobierz [Eclipse dla JSE](http://www.eclipse.org/downloads/) i [JDK](http://java.sun.com/javase/downloads/index.jsp)). Przejdź do Help -> Software updates -> Available software -> Add site i podaj adres: http://download.eclipse.org/dsdp/mtj/updates/0.9/stable. Na liście pojawi się Mobile Tools for Java (MTJ) - zainstaluj wszystkie pakiety.

Musimy teraz wskazać ścieżkę do emulatorów oraz innych narzędzi. W tym celu, po restarcie Eclipse'a przełącz się na perspektywę Java ME (View -> Open perspective -> Other -> Java ME) oraz przejdź do ustawień Windows -> Preferences -> Java ME -> Device Management -> Import -> Browse i wskaż ścieżkę, gdzie zainstałowałeś WTK. Po przeszukaniu katalogu, na liście powinny się pojawić cztery emulatory - zaznaczamy wszystkie i klikamy Finish -> OK.

Teraz możesz już stworzyć swój pierwszy projekt w Java Micro Edition - File -> New -> MIDlet project. Gdy utworzysz nową klasę midletu - pamiętaj, ażeby w Application Descriptor -> MIDlets dodać ją na listę. W innym przypadku program nie uruchomi się na komórce. Ja tego nie wiedziałem i spędziłem dwie godziny na szukaniu przyczyny. :-)