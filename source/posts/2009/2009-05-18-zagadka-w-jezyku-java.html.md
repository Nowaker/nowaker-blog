---
title: Zagadka w języku Java
---

Dzisiaj na mojej uczelni odbywały się coroczne Trójmiejskie Targi Pracy. Jedna z firm przedstawiających swoje oferty, [Sygnity](http://www.sygnity.pl/), zorganizowała mały konkurs z nagrodami. Konkurs miał 10 pytań o tematyce programistycznej (Java, JEE, SQL, build-scripts). Z testu uzyskałem wynik 9/10. Potknąłem się na rzeczy, której byłem po prostu pewien - na analizie krótkiego kodu w języku Java.

<!--more-->

Należy dodać, że nie był to typowy konkurs, w którym pytają o imię głównego bohatera serialu Świat wg Kiepskich, dając do wyboru Zbigniewa, Ferdynanda i Jerzego. Test zgrabnie stwierdzał, czy jesteś oczytany w tematach związanych z programowaniem. Jeden z wykładowców, pan Kaczmarek, mawiał na wykładach o Linuksach, że wiedza jest banalna, jeśli się pozna polecenie i niemożliwa do wymyślenia, jeśli tego polecenia na oczy nie widzieliśmy.

Poniżej umieszczam zadanie testowe, które mnie wkopało. Kurczę, podium było tak blisko :>

~~~
class A {
	public String a = "3";

	public String getA() {
		return this.a;
	}
}

class B extends A {
	public String a = "5";

	public String getA() {
		return this.a;
	}
}

public class C {
	public static void main(String[] args) {
		A a = new B();
		System.out.println(a.a + ", " + a.getA());
	}
}

~~~

Jaki będzie wynik działania tego programu?

- 3, 3
- 3, 5
- 5, 3
- 5, 5

Proponuję najpierw udzielić odpowiedź, a dopiero potem sprawdzić sobie rezultat na żywo. Ja po prostu nie wiedziałem, że tak to działa. Może dlatego, że zgodnie z zasadą enkapsulacji nie pozostawiałem nigdy żadnych publicznych pól? Ale teraz już wiem :-)

P.S. W losowaniu jedną z nagród zgarnął oczywiście [Zal](http://blog.4zal.net/), masowy wygrywacz wszystkich konkursów :D Ale dzięki temu miałem przyjemność poznać go osobiście.