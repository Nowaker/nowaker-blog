---
title: Prawa właściciela domeny – transfer i wydanie kodu authinfo
---

Niewiele osób wie, jakie są ich prawa jako właścicieli domeny. Znajomość tych praw przydaje się w krytycznym momencie – gdy zbliża się koniec ważności domeny, a koszt przedłużenia u innego rejestratora jest znacznie niższy.

<!--more-->

Nabywca po kupnie domeny staje się jej właścicielem. Oznacza to, że ma *wyłączne* prawo dysponowania nią. Rejestrator jest tylko odpowiedzialny ze techniczną obsługę jego domeny i właściciel domeny w każdej chwili może go zmienić. Najczęściej do transferu domeny do innego rejestratora potrzebny jest tzw. kod authinfo. Wydaje go rejestrator na wniosek właściciela. I tutaj zaczynają się schody – niektórzy rejestratorzy próbują różnych zagrywek, aby nie wydać Ci tego authinfo. Liczą na to, że właściciele nie znają przysługujących sobie praw lub grają na zwłokę, by w końcu właściciel był zmuszony przedłużyć domenę u niego.

## Polskie domeny z NASK

Polskimi domenami zarządza NASK. Każdy rejestrator domen jest de facto pośrednikiem między właścicielem, a NASK. Rejestrator w umowie z NASK zobowiązuje się przestrzegać pewnego regulaminu, który określa m.in. prawa i obowiązki zarówno rejestratora, jak i właściciela domeny. Można tam znaleźć m.in. warunki wydawania authinfo. W skrócie:

> 1) *Partner nie może uzależniać wydania kodu od dodatkowych warunków*.

> 2) *Dla swojego bezpieczeństwa sprawdź, czy w danych, służących do kontaktu pomiędzy NASK a Tobą jest wpisany poprawny adres e-mail. W przypadku istotnych zmian dotyczących nazwy domeny, wysyłane jest potwierdzenie ich dokonania na Twój adres poczty elektronicznej (e-mail) np. zmiana danych abonenta nazwy domeny.*

Tutaj za typowy przykład może posłużyć CAL.PL. Aby przetransferować domenę każą sobie wysłać faks. Jaki faks? Dyspozycja wydana z adresu e-mail, którym zawsze kontaktuję się z rejestratorem oraz na który zarejestrowana została domena jest wiążąca i wystarczająca.

## Domeny ICANN (domeny globalne .COM, .NET, .ORG i inne)

W przypadku domen globalnych, rejestratora obowiązuje [Uniform Domain Name Dispute Resolution Policy][udrp]. Czytamy:

> 3) Cancellations, Transfers, and Changes. We will cancel, transfer or otherwise make changes to domain name registrations under the following circumstances:  

> a) subject to the provisions of Paragraph 8, our receipt of written or appropriate electronic instructions from you or your authorized agent to take such action; 

Reasumując, wystarczy pisemna lub, co nas bardziej interesuje, elektroniczna dyspozycja.

## Domeny IANA (.US, .BIZ i inne)

Domeny amerykańskie są zarządzane przez firmę NeuStar na zlecenie [IANA][iana]. Zasady transferu tych domen można przeczytać [w tym pliku PDF][ustransfer], zaś zasady autoryzacji [tutaj][ustransferauth]. Swoją uwagę należy zwrócić na drugi dokument, który opisuje, w jaki sposób można stwierdzić, że dyspozycja wydania kodu authinfo pochodzi od właściciela domeny. Interesuje nas ten fragment:

> In the event that the Gaining Registrar relies on an electronic process to obtain this authorization the acceptable forms of identity would include:

> - [...]

> - Consent from an individual or entity that has an email address matching the  Registered Name Holder’s email address.

*Registered Name Holder* to, po prostu, dane właściciela domeny - w tym jego adres e-mail. Dyspozycje wydawane z tego adresu e-mail uważa się więc za dyspozycje właściciela domeny.

## Kilka porad

Sprawdź, czy firma, która obsługuje Twoje domeny rzeczywiście jest rejestratorem (poleceniem *whois*). Często zdarza się, że firmy hostingowe nie mają na początek tyle pieniędzy, aby stać się autoryzowanym partnerem NASK (czy też innego zarządcy domen). Jeśli nie jest, w sprawie transferu nawet nie kontaktuj się z tą firmą. Formalnie - obsługuje Cię tamten rejestrator.

Taki przypadek spotkał mnie z CAL.PL i transferem domeny US. Firma INFO-CAL, standardowo, wymaga wysłania faksu w celu transferu domeny. Ja zaś faksu nie posiadam, a biegać na pocztę albo do znajomych zamiaru nie miałem. Mimo moich dosadnych próśb o wydanie kodu authinfo i popartych zasadami rejestratora, kodu authinfo wydać nie chcieli.

Narzędzie *whois* wskazało, że rejestratorem jest ResellOne.net. W takim wypadku rzeczywiście nie mieli nic do stracenia - wszak nie są rejestratorem, tylko pośrednikiem. Skontaktowałem się więc bezpośrednio z ResellOne.net z prośbą o wydanie kodu authinfo - oczywiście pisałem z adresu, na który zarejestrowana była domena. Kod authinfo otrzymałem tego samego dnia, wysłany był o godzinie 9:30 czasu amerykańskiego. Nikt nawet nie pytał o zdanie CAL.PL - to ja jestem właścicielem domeny.


[udrp]:  http://www.icann.org/en/udrp/udrp-policy-24oct99.htm
[iana]: http://pl.wikipedia.org/wiki/Internet_Assigned_Numbers_Authority
[ustransfer]: http://www.nic.us/policies/docs/USTransferPolicy.pdf
[ustransferauth]: http://www.nic.us/policies/docs/USRegistrarTransfer-FOA.pdf
