---
title: Wydajność MySQL a PostgreSQL
---

Temat wydajności tych czy innych silników baz danych był już wielokrotnie poruszany na stronach i blogach. Testy, na które się dotychczas natknąłem nie były zbyt wartościowe. Raz MySQL wygrywał, raz PostgreSQL. W dodatku każda dyskusja na temat MySQL konta PostgreSQL wywoływała wiele niepotrzebnych emocji.

Natknąłem się jednak na ciekawy wpis na blogu działu IT serwisu grono.net. Cytat:

<code>
"W chwili obecnej całość działa już na PostgreSQLu, działa ładnie i szybko. Load maszyny z PostgreSQLem jest około <strong>3-krotnie</strong> niższy niż load maszyny z MySQL’ em (przy tym samym obciążeniu)."
</code>

Podano mało informacji na temat zawartości, jednak z profilu serwisu można się domyślać, że najczęstsze operacje to SELECT, a potem INSERT. Ilość danych to 60GB w 150 mln rekordach.

Całość na <a href="http://itblog.grono.net/articles/2007/12/10/baza1" title="IT blog grono.net">IT blogu grono.net</a>.