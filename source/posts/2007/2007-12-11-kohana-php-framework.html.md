---
title: Kohana PHP framework
---

[Kohana](http://www.kohanaphp.com/) to mały framework [MVC](http://pl.wikipedia.org/wiki/MVC),
wspomagający tworzenie stron internetowych.
Wersja 1.0 była forkiem słynącego z prostoty i szybkości (ale i rzadkimi aktualizacjami)
[CodeIgnitera](http://www.codeigniter.com).
Wersja 2.0, wydana około  miesiąca temu stanowi już całkowicie osobny projekt.
Na aktualizacje nie ma co narzekać - społeczność Kohany na bieżąco zajmuje się poprawianiem błędów i
implementacją nowych funkcji.

<!--more-->

[![Kohana PHP framework](/img/logos/kohana.png)](http://www.kohanaphp.com/)

Kohana PHP posiada praktycznie wszystkie funkcje z poprzednika oraz jest poszerzona o dużo własnych.
Kohana jest napisana całkowicie w PHP 5 i z nastawieniem na kodowanie UTF-8.

Nie ma róży bez kolców, na chwilę obecną Kohana jest praktycznie nieudokumentowana.
Może to stanowić nie lada problem dla osób, które jeszcze nigdy nie korzystały z żadnego frameworka.
Przy tworzeniu aplikacji można jednak wspomagać się [dokumentacją CodeIgnitera](http://codeigniter.com/user_guide/ "CodeIgniter") -
filozofia tworzenia aplikacji bowiem jest dokładnie taka sama.
Można również zadać pytanie na [oficjalnym forum](http://forum.kohanaphp.com/) Kohany.

W chwili obecnej piszę całkowicie od nowa mój [portal edukacyjny o geografii](http://www.geozone.pl) w oparciu o Kohanę.
Zmusiła mnie do tego ogromna trudność ingerencji w serwis - wszystko porozrzucane w wielu plikach,
a logika pomieszana z wyglądem.
Portal ma już 3 lata i dorzucanie kolejnych kilobajtów kodu ma ostatecznie takie skutki ;)

*   [Kohana PHP](http://www.kohanaphp.com/)
*   [CodeIgniter](http://www.codeigniter.com/)
*   [Polska dokumentacja CodeIgnitera](http://www.php.rk.edu.pl/w/p/kurs-ci/)
