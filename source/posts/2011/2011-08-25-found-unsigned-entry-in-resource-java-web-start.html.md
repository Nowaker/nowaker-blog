---
title: Java Web Start complains about unsigned JARs
keywords: java, applet, jnlp, java web start, unsigned jar, jarsigner
atlashost: true
---

You are developing a secure Java Web Start (JNLP) application, you sign JARs with `jarsigner` tool,
you have a valid certificate but some people still get `Found unsigned entry in resource` error message
although **your JARs are signed** and `jarsigner -verify` accepts them. Did you encounter this problem too?

<!--more-->

Exact error message in applet console:

        #### Java Web Start Error:
        #### Found unsigned entry in resource: http://my.spartez.com/download/screensnipe/jdom-1.0.jar

        com.sun.deploy.net.JARSigningException: Found unsigned entry in resource: http://my.spartez.com/download/screensnipe/jdom-1.0.jar
                at com.sun.javaws.security.SigningInfo.getCommonCodeSignersForJar(SigningInfo.java:411)
                at com.sun.javaws.security.SigningInfo.check(SigningInfo.java:292)
                at com.sun.javaws.LaunchDownload.checkSignedResourcesHelper(LaunchDownload.java:1741)
                at com.sun.javaws.LaunchDownload.checkSignedResources(LaunchDownload.java:1508)
                at com.sun.javaws.Launcher.prepareResources(Launcher.java:1232)
                at com.sun.javaws.Launcher.prepareAllResources(Launcher.java:621)
                at com.sun.javaws.Launcher.prepareToLaunch(Launcher.java:327)
                at com.sun.javaws.Launcher.prepareToLaunch(Launcher.java:199)
                at com.sun.javaws.Launcher.launch(Launcher.java:116)
                at com.sun.javaws.Main.launchApp(Main.java:416)
                at com.sun.javaws.Main.continueInSecureThread(Main.java:248)
                at com.sun.javaws.Main$1.run(Main.java:110)
                at java.lang.Thread.run(Thread.java:662)

## Fixing it

I googled for it and found [this topic](http://www.coderanch.com/t/200684/JNLP-Web-Start/java/jdk-found-unsigned-entry-resource).
It turns out that *disabling* Java plugin caching is sometimes a cause of this problem. Just enable Java plugin
caching and the problem is gone.

However, because you encountered this problem, you probably want the caching disabled.
Applet and Web Start developers tend to disable cache so that they always get a current version of application.
How to make the Java Web Start application working with disabled cache?

In my case, application consists of five signed JARs: screensnipe-1.5-rc3.jar, forms-1.2.1.jar, jdom-1.0.jar,
jide-oss-2.5.8.jar and jhlabs-filters.jar. Java claims that JDOM JAR is unsigned, others are fine. I took a look
at the JAR for something distinctive that can cause this particular JAR to fail verification.
Filename length was considered, JARs order in JNLP file, existence of classes in root package, META-INF/ directory
content and so on. It was `META-INF/MANIFEST.MF` file that contained something that Java Web Start verifier failed on.

        Name: org/jdom/output/
        Implementation-Vendor: jdom.org
        Implementation-Title: org.jdom.output
        Implementation-Version: 1.0
        Specification-Vendor: jdom.org
        Specification-Title: JDOM Output Classes
        Specification-Version: 1.0

        Name: org/jdom/transform/
        (...)

Removing directory entries from Manifest file made it working. Woah.
Note: Manifest file is valid - Java Web Start verifier just fails on signed directories when caching is disabled.

## Advertisement

I encountered this problem while developing a [ScreenSnipe](http://www.screensnipe.com/), an awesome screenshot
tool that integrates seamlessly with Atlassian JIRA and Confluence. All AtlasHost customers get it preinstalled
in their instance. Others can try out ScreenSnipe by grabbing it from
[Atlassian Plugin Exchange](https://plugins.atlassian.com/search/with?q=screensnipe&product=all) or installing
via Atlassian Plugin Manager in your JIRA/Confluence.

![ScreenSnipe for JIRA install via UPM](/img/posts/screensnipe-jira.png)
