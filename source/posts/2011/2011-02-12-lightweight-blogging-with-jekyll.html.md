---
title: Lightweight blogging with Jekyll
keywords: jekyll, blogging, blog, scm blog, ruby
atlashost: true
---

Privately, we are passionate about software development and open source.
Apart from providing a reasonable hosting of Atlassian tools, we want to contribute to community a bit.
This is why we decided to run a blog after getting the most important things with AtlasHost done.

<!--more-->

These days, Wordpress is the most popular blogging engine. Can you name any other blog software you can deploy
on your server? Probably not. I decided to take a look at alternative blog engines. As I like Ruby and its
ecosystem, I chose this way. Unfortunately, I didn't find anything good enough for me and I'm not too demanding.

A few days later I came across a blog engine that works in quite unusual way. It polls a source code repository
and publishes a blog post after something is found. The post gets generated as a static HTML and put somewhere in
public_html. Homepage, Atom feed and categories get updated. Quite interesting, huh?

I caught the idea. I started looking for some SCM-based blog engines by [asking a question at StackOverflow][stack].
I tried out some of engines mentioned there and [Jekyll][jekyll] turned out to be the one I liked most. Why?

- publishing a new post by putting a file in `/_posts` directory
- support for Markdown, my favourite markup language
- simple templating engine lets me have full control over what I get on the blog
- no need to run any weird servers, everything is a plain old static HTML
- no security hole **ever** - no one is able to inject SQL into a static website :-)
- it's dead simple, take a look at the [screenshot][screenshot]

Of course, everything can be both good and bad. It just depends. Being static means lack of comments.
However, this particular problem with comments can resolved by using a 3rd party comments widget like Disqus.

This blog is run by [Jekyll][jekyll] and Subversion. Run yours too!



[stack]: http://stackoverflow.com/questions/4917394/what-scm-based-blog-engines-are-there
[jekyll]: https://github.com/mojombo/jekyll
[screenshot]: /images/posts/jekyll-screenshot.png

