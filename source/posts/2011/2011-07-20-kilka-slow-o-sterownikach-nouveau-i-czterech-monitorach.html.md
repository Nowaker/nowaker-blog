---
title: Kilka słów o sterownikach Nouveau i czterech monitorach
---

Czy można mieć obsługę czterech monitorów w jednym komputerze w taki sposób, aby dowolnie przeciągać okna pomiędzy poszczególnymi monitorami? Oczywiście bez dodatkowych kosztów np. w postaci specjalistycznego sprzętu czy płatnego systemu operacyjnego pewnej firmy. *Yes, we can!*

<!--more-->

[![Four monitors, two graphic cards and Nouveau](/img/posts/nouveau-tweet.png)](http://www.twitter.com/nwkr)

> &lt;erdek&gt; Nouveau? Are you sane? Proprietary4Life!<br/>
> &lt;Nowaker&gt; Ale blob posysa.<br/>
> &lt;erdek&gt; Może i blob, ale performance co najmniej x2, niż otwarte.

Zamknięte sterowniki Nvidii rzeczywiście są szybsze i lepsze w typowych zastosowaniach. Compiz działa, gry działają. Bez żadnych problemów. W typowych zastosowaniach ciężko jest konkurować otwartym sterownikom [Nouveau](http://nouveau.freedesktop.org/) z zamkniętymi sterownikami [Nvidii](http://www.nvidia.com/object/unix.html) (nazywanymi dalej jako *blob*), ponieważ kuleje wsparcie sprzętowe 3D. Czy są w ogóle jakieś powody, dla których warto używać Nouveau? Tak! Jeśli jednak nie potrzebujesz 3D, Nouveau ma kilka asów w rękawie.

## Blob nie taki szybki na trzech monitorach

Obsługa trzech albo czterech monitorów przez dwie karty graficzne oczywiście nie jest typowym zastosowaniem. I tutaj Nouveau wygrywa z zamkniętym sterownikiem Nvidii pod względem szybkości, gdy osobne monitory trzeba połączyć Xineramą. Zaskakujące jest to, że konfiguracja trzymonitorowa na blobie jest wolniejsza od czteromonitorowej na Nouveau.

## KMS - kernel mode setting

> Kernel mode-setting (KMS) shifts responsibility for selecting and setting up the graphics mode from X.org to the kernel. When X.org is started, it then detects and uses the mode without any further mode changes. This promises to make booting faster, more graphical, and less flickery.[^2]

[^2]: https://wiki.ubuntu.com/X/KernelModeSetting

Dzięki temu naciśnięcie CTRL+ALT+F1 przenosi natychmiastowo do konsoli tekstowej - bez niepotrzebnego przełączania rodzielczości i migotania monitora. Blob nie umie KMS, więc przełącza na terminal wielkości 80x25 znaków - jak za dawnych czasów.

## RandR

> The X Resize, Rotate and Reflect Extension (RandR) allows clients to dynamically change X screens, so as to resize, rotate and reflect the root window of a screen. RandR extension framework brought the ability to change display characteristics without restarting the X session.[^3]

[^3]: http://en.wikipedia.org/wiki/RandR

Blob nie pozwala zmieniać rozdzielczości i dołączać/odłączać monitorów w locie za pomocą polecenia *xrandr* lub narzędzi typowych dla środowiska (np. *gnome-display-properties*), które pod spodem z tego polecenia korzystają. Trywialne ustawienia można zmieniać w locie za pomocą narzędzia *nvidia-settings*, jednak nietrywialne wymagają już restartu.

Niestety rozszerzenie [RandR](http://www.x.org/wiki/Projects/XRandR) nigdy nie jest dostępne podczas używania Xineramy. Xinerama to hack, który w sposób software'owy łączy osobne sesje X, aby zapewnić możliwość przeciągania okien pomiędzy monitorami podłączonymi do innych kart graficznych.

## Maksymalizacja okien

Blob Nvidii przegrywa potyczkę z Nouveau przy... maksymalizacji okien. Dotyczy to środowisk 3-monitorowych i większych z Xineramą. Gdy Xinerama łączy TwinView'owy screen z jakimkolwiek innym, próba maksymalizacji okna kończy się oknem rozciągniętym pomiędzy wszystkie monitory. Można to obejść poprzez używanie dekoratora okien, który nie sugeruje się nieprawidłowymi metadanymi X, lecz używa wbitych na sztywno danych przez użytkownika (użytkownik sam definiuje jakie rozmiary mają poszczególne monitory). Metacity, domyślny dekorator okien w GNOME, tego nie ma. Compiz potrafi, ale nie działa na Xineramach. Jeśli ktoś jest hardkorem, może używać [xmonad](http://xmonad.org/) - ponoć całkiem fajne. Albo można zamienić bloba na Nouveau.

## Społeczność

Nie sposób nie wymienić przyjaznej społeczności Nouveau, która nie tylko jest w stanie zasugerować rozwiązanie, ale zajrzeć do kodu źródłowego i naprawić błąd od razu.

Najpierw nie mogłem skonfigurować obsługi trzech monitorów na dwóch kartach. Polecono mi skorzystać z opcji ZaphodHeads, jednakże ta opcja nie była rozpoznawana przez moją wersję sterownika. Podciągnąłem więc iksy do [bleeding edge'a](https://launchpad.net/~xorg-edgers/+archive/ppa) i jeszcze raz spróbowałem - zadziałało.

Skoro już byłem przy trzech monitorach, chciałem od razu sprawdzić działanie czterech. Niestety obraz na czwartym monitorze był być najwyżej kopią trzeciego. Nie mógł działać jako niezależny - kończyło się to błędem. [Zgłosiłem błąd](https://bugs.freedesktop.org/show_bug.cgi?id=39099), Emil podesłał dwa patche, drugi zadziałał. Stałem się testerem Nouveau. ;-)

Takie brzydkie błędy powstają, gdy pisze się skomplikowanego ifa, zamiast stworzyć dwie zmienne pomocnicze. Niejawna konwersja z *int(0)* na *bool(false)* to też zło, jednak języka C nie zmienimy. Widzisz błąd?

```cpp
if (!xf86IsEntityShared(pScrn->entityList[0] ||
    pScrn->confScreen->device->screen == i))
```

## Podsumowanie

Nouveau lepiej pasuje do całego ekosystemu linuksowego. Blob z kolei po prostu działa jeśli chodzi o 3D. Stabilność obu sterowników jest taka sama. W stosunku do Nouveau jest to wręcz zaskakujące, ponieważ używam bleeding edge iksów, a sterownik mam prosto z gita i lekko [patchowany](https://bugs.freedesktop.org/show_bug.cgi?id=39099).

![OpenTTD with four monitors](/img/posts/openttd-four-monitors.jpg)

To tylko atrapa - nie da się praktycznie grać ze względu na zbyt małą liczbę klatek na sekundę. ;-) Xinerama łączy obraz ze wszystkich monitorów w sposób software'owy i nie wyrabia z animacją na wszystkich monitorach.

P.S. Dla zainteresowanych [xorg.conf na 4 monitory](http://wklej.org/id/559775/txt/).
