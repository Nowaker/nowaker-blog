---
title: Modele licencjonowania oprogramowania
---

Celem tego artykułu jest porównanie modeli licencjonowania oprogramowania z punktu widzenia programisty, który chce skorzystać z kodu źródłowego napisanego przez innych. Aby bez obaw włączyć cudzy kod do własnego projektu należy w pełni rozumieć licencję, na jakiej ten kod jest udostępniany. Postaram się porównać najpopularniejsze modele licencjonowania oprogramowania, do którego udostępniany jest kod źródłowy. Celowo pomijam wszystkie licencje, które dotyczą oprogramowania zamkniętego, bądź nie dotyczą oprogramowania w ogóle. Artykuł powstał w grudniu 2009 r. jako praca zaliczeniowa przedmiotu Społeczne aspekty informatyki.<!--more-->

## Ważne pojęcia

Aby rozumieć przekaz tego artykułu, należy zapoznać się z podstawowymi pojęciami dziedzinowymi dotyczącymi licencjonowania oprogramowania oraz tematów z tym związanych.

### Prawo autorskie

Prawo autorskie to ogół praw przysługujących autorowi utworu. W Polsce prawa autorskie osobiste są niezbywalne i chronią „intelektualny” związek twórcy z jego dziełem, np. podpisanie utworu własnym nazwiskiem. Autorskie prawa majątkowe to prawa do rozporządzania utworem przez uprawnioną osobę, czerpania z tego zysków oraz licencjonowania utworu innym podmiotom.

### Licencja

Licencja to dokument prawny lub umowa, określająca warunki korzystania z utworu jako dobra, którego dana licencja dotyczy. Właściciel praw autorskich, znaku handlowego lub patentu może (i często to robi) wymagać od innych posiadania licencji jako warunku użytkowania lub reprodukowania licencjonowanego utworu.

### Copyleft
Słowo copyleft może zostać przetłumaczone jako „lewo autorskie”. Celem copyleft nie jest jednak negowanie idei praw autorskich, lecz odwrócenie jego znaczenia. Twórcy wykorzystują „copyright” do ograniczenia wolności odbiorcy oprogramowanie, zaś ideą „copyleft” jest oddanie wszystkich praw użytkownikowi.

> By poddać program działaniu copyleft, najpierw zastrzegamy copyright, prawa autorskie do niego. Następnie dokładamy warunki rozpowszechniania, będące prawnym środkiem, dzięki któremu dajemy każdemu prawo do używania, modyfikowania i rozpowszechniania kodu naszego programu lub dowolnego programu pochodnego, ale tylko wtedy gdy warunki rozpowszechniania pozostaną niezmienione. W ten sposób, kod i przekazane wolności stają się prawnie nierozdzielne.[^1]

Z ideą copyleft ściśle powiązane są licencje GNU GPL, GNU LGPL oraz GNU FDL. W pewnym ograniczonym stopniu korzystają z niej licencje z serii Creative Commons[^2]. Pomijam jednak opis licencji z tej serii, ponieważ nie nadają się one do licencjonowania oprogramowania.

### Free software i open source

Ruchy, które działają na rzecz wolnego dostępu do oprogramowania przez ogół użytkowników. Różnica pomiędzy nimi jest raczej niewielka i całkowicie nieważna na potrzeby tego artykułu. Kluczową specyfiką oprogramowania wolnego (free software) i otwartego (open source software) jest udostępnianie kodu źródłowego oprogramowania i zezwolenie na dowolne korzystanie z niego pod warunkiem udostępnienia oprogramowania pochodnego również jako free lub open source. Free software jest promowane przez fundację Free Software Foundation i jest ściśle związane z projektem GNU. Open source software jest promowane przez fundację Open Source Initiative.

## GNU GPL - General Public License

### Prawa i obowiązki

GNU GPL to licencja typu copyleft, która zabezpiecza wszystkie prawa odbiorcy oprogramowania. Twórca oprogramowania udziela odbiorcom takich samych praw, jakie sam uzyskał. Musi również zapewnić dostęp do kodu źródłowego oprogramowania. Licencja precyzuje również, co jest rozumiane poprzez pojęcie „kod źródłowy”.

> Przez „kod źródłowy” utworu rozumie się najdogodniejszą postać utworu dla wprowadzania w niej modyfikacji.

Taka definicja uniemożliwia potraktowanie kodu poddanego zaciemnianiu (obfuskacji) kodu źródłowego. Taki kod jest w ogóle nieprzydatny i praktycznie nie da się w nim wprowadzać modyfikacji. Z tego też powodu jest zakazany przez licencję GPL.

Jeśli do uruchomienia programu, jego kompilacji bądź modyfikacji potrzebne jest inne oprogramowanie - ono również musi zostać włączone do projektu. Taki zapis również chroni odbiorców, ponieważ nie uznaje za właściwy takiego kodu źródłowego, do którego nie byłoby np. żadnego kompilatora. Nie trzeba jednak dołączać narzędzi ogólnie dostępnych (np. standardowych kompilatorów) oraz bibliotek systemowych (cały system operacyjny).

Nie zapomniano również o wymogu dołączenia instrukcji dotyczących instalacji oprogramowania. Instalacja oprogramowania nie może wymagać żadnych kluczy, czy haseł znanych tylko twórcy programu.

Licencja nie zapomina również o przypadkach wykrycia, że jakieś oprogramowanie własnościowe narusza warunki licencji GPL, tj. zawiera w sobie część na licencji GPL, zaś całe oprogramowanie nie zostało wypuszczone na licencji GPL. Wtedy właściciel oprogramowania własnościowego musi w ciągu 30 dni od daty pierwszego zgłoszenia naruszenia licencji przywrócić stan sprzed naruszenia licencji, czyli udostępnić całe oprogramowanie na licencji GPL, bądź zaprzestać rozpowszechniania kodu zamkniętego z elementami objętymi licencją GPL. Jeśli nie wykona tego w ciągu tego czasu, trwale traci prawo nawet do korzystania z tego oprogramowania - zarówno teraz, jak i w innych projektach w przyszłości.

### Punkt widzenia programisty

Programista, który chce włączyć jakikolwiek kod na licencji GPL do swojego projektu i móc projekt rozpowszechniać musi wypuścić całe swoje oprogramowanie na licencji GPL. Nie musi tego robić, jeśli oprogramowanie nie będzie rozpowszechniane.

Sublicencjonowanie jest zabronione. Nie wolno nakładać żadnych dodatkowych ograniczeń na sposób korzystania z oprogramowania, jego modyfikacji, czy redystrybucji. 

Jeśli programista jest właścicielem patentu (dotyczy Stanów Zjednoczonych) i użyje jego implementacji w oprogramowaniu na licencji GPL, daje wszystkim dalszym odbiorcom programu zezwolenie na korzystanie z patentu w ramach licencji GPL. Jest tak od trzeciej wersji licencji.

Można powiedzieć, że programista jest „ograniczony wolnością”[^3]. Należy jednak pamiętać, że ideą licencji jest zapewnienie pełnej wolności użytkownikom oprogramowania, a nie twórcom. Licencja GPL nie jest więc dobra dla firm, które chciałyby maksymalizować zysk poprzez nakładanie na użytkownika końcowego ograniczeń, gdyż tego robić nie mogą. Praktyka jednak dowodzi, że istnieje dużo firm, które oferują pełne wsparcie dla oprogramowania na licencji GPL i czerpią z tego rozsądne zyski. Przykładów jest mnóstwo, m.in. czołowe firmy wspierające rozwój systemu Linux - Red Hat, SuSE i Novell.

### Niepewności

Trudno powiedzieć, czy źródła aplikacji internetowej muszą być udostępniane. Umieszczenie aplikacji na swoim serwerze i korzystanie z niej poprzez protokół HTTP nijak nie można nazwać rozpowszechnianiem. Kod HTML serwowany przez serwer aplikacji to dane wyjściowe programu, a wg licencji dane wyjściowe nie są automatycznie objęte licencją. Jest to całkowicie zrozumiałe. Gdyby tak nie było, wywołanie polecenia „cat”, wyświetlającego zawartość pliku niosłaby za sobą przymus wypuszczenia pliku na licencji GPL.

Z drugiej strony, na aplikację internetową składają się różne pliki, m.in. JavaScript. Pliki JavaScript są rozpowszechniane przez zwykłe umieszczenie ich na serwerze. Korzystając z technologii AJAX, skrypty JavaScript kierują żądanie do serwera aplikacji. Można więc się zastanawiać, czy nie mamy do czynienia z pewną odmianą linkowania dynamicznego. Jeśli założymy, że jest to linkowanie dynamiczne to kod źródłowy części „serwerowej” jesteśmy zobowiązani również udostępnić.

Udostępnianie kodu źródłowego aplikacji internetowych nie jest jednak powszechną praktyką, aczkolwiek powszechność jakiejś praktyki wcale nie dowodzi, że jest ona zgodna z prawem.

## GNU AGPL - Affero General Public License

Fundacja Free Software Foundation zauważyła[^4] niepewności dotyczące aplikacji działających na serwerach sieciowych i wydała specjalną odmianę licencji GNU GPL, która porusza temat aplikacji internetowych. Licencja ta ma dokładnie taki sam wydźwięk, jak GNU GPL, z tym wyjątkiem, że aplikacje internetowe działające po stronie serwera muszą również udostępniać swój kod źródłowy.

> Licencja GNU AGPL została stworzona by zapewnić w takich przypadkach [oprogramowania działającego na serwerach sieciowych - przyp. red.] publiczną dostępność kodu źródłowego. Operator serwera sieciowego, na którym działa zmodyfikowany program, ma obowiązek udostępnienia użytkownikom serwera kodu źródłowego tego programu.

Free Software Foundation rekomenduje używać tej licencji do każdego oprogramowania, które będzie uruchamiane z serwerów sieciowych, aby zagwarantować dostęp do kodu źródłowego aplikacji serwerowych.

Programista, który dołącza kod objęty licencją AGPL musi udostępnić źródła całego oprogramowania. Bez względu na to, czy aplikacja jest rozpowszechniana w klasyczny sposób, czy tylko umieszczana na serwerze sieciowym i dostępna poprzez serwer aplikacji.

## Licencja X11 (MIT)

Licencja X11 to jedna z najbardziej liberalnych licencji oprogramowania. Występuje bez klauzuli copyleft. Twórca daje użytkownikowi pełnię praw do dysponowania kodem źródłowym - może go włączyć do własnego projektu i modyfikować w dowolny sposób. Twórca zastrzega tylko, że nie udziela żadnych gwarancji w stopniu dozwolonym przez prawo.

Sublicencjonowanie nie jest zabronione. Można więc rozwinąć jakiekolwiek oprogramowanie na tej licencji, kodu źródłowego nie udostępniać i czerpać zyski z dalszego licencjonowania programu. Licencja ta jest bardzo przyjazna, programista może włączyć taki kod do swojego projektu bez jakichkolwiek wątpliwości. 

## Licencje BSD

2-klauzulowa wersja licencji BSD jest praktycznie tożsama z licencją X11. 3-klauzolowa licencja BSD (tzw. BSD-new) zawiera dodatkową informację, że nie wolno wykorzystywać nazwisk autorów do promowania oprogramowania.

4-klauzulowa licencja BSD (tzw. stara licencja BSD) zawiera tzw. klauzulę ogłoszeniową. Każda modyfikacja jakiegokolwiek oprogramowania na tej licencji nakazuje pozostawienie nazwisk wszystkich poprzednich autorów. Gdy oprogramowanie przejdzie przez ręce stu programistów, należy utrzymać listę tych stu osób. Ze względu na te nieudogodnienia radzi się całkowicie odejść od starej, 4-klauzulowej licencji BSD.

Programista może bez żadnego problemu włączyć do swojego projektu kod udostępniany na tej licencji. Jeśli będzie miał do czynienia ze starą wersją licencji będzie musiał dodać jedynie listę z poprzednimi autorami oprogramowania.

## WTFPL - Do What The Fuck You Want To Public License

Mimo zabawnej nazwy, licencja jest całkiem popularna. Jej atutem jest to, że jest najbardziej liberalna. W treści licencji istnieje tylko jeden punkt, określający prawa odbiorcy oprogramowania:

> 0. You just DO WHAT THE FUCK YOU WANT TO.

Ze względu na dobre obyczaje nie będę tego dosłownie tłumaczyć. Wystarczy powiedzieć, że licencja pozwala na wszystko. W zabawny sposób wyjaśnia to również FAQ[^5] dostępne na oficjalnej stronie licencji.

> „By the way, with the WTFPL, can I also…”, „Oh but yes, of course you can.”
> „But can I…”, „Yes you can.”
> „Can…”, „Yes!”

Dodatkowo, do treści licencji można dodać klauzulę „no warranty”, w której zastrzega się, że twórca nie udziela żadnych gwarancji w stopniu dozwolonym przez prawo. Opcjonalność tej klauzuli daje możliwość używania licencji WTFPL nie tylko do licencjonowania oprogramowania, ale również sztuki albo dokumentacji.

Wydaje się jednak, że ze względu na specyficzną nazwę i treść, licencja może nie być popularna wśród firm.

## Public domain

Choć public domain nie jest licencją, specjalnie ją przedstawiam. Domena publiczna oznacza, że prawa autorskie do utworu wygasły i przez to dostępny jest dla wszystkich i do dowolnych zastosowań. W Polsce utwory przechodzą do domeny publicznej po 70 latach.

Public domain w odniesieniu do oprogramowania zakłada, że nikt nie ma żadnych praw, które nakładają restrykcje na utwór. Problem jednak polega na tym, że w większości państw nie ma możliwości zrzeknięcia się prawa autorskiego. W dodatku, bez jawnego zezwolenia na modyfikację utworu, nie możemy być pewni, czy mogliśmy ostatecznie wprowadzić te modyfikacje. W każdej chwili twórca może zacząć rościć swoje prawa do utworu[^6]. Jednak praktyka pokazuje, że twórcy wypuszczają utwory na public domain z niewiedzy.

Programista, który chce włączyć kod public domain do swojego projektu ma raczej niewielki problem. Najlepiej skontaktować się z autorem kodu i poprosić o wypuszczenie kodu na licencji, która jasno definiuje prawa osoby, która kod źródłowy otrzymuje. Zazwyczaj autorzy nie robią z tym problemów.

## Łączenie różnych licencji

Niniejszy rozdział ma na celu wyjaśnienie sytuacji, która ma miejsce, gdy do kodu na pewnej licencji programista chce dodać kod na innej licencji. Aby móc to uczynić, należy upewnić się, że licencja dodawanego kodu jest kompatybilna z licencją projektu, do którego kod jest włączany. Prostymi słowy - czy licencja projektu nie zabrania czegoś, co licencja dodawanego kodu nakazuje[^7].

Rozpatrzmy przykład włączenia kodu na licencji GNU GPL do kodu projektu na licencji X11. GNU GPL nakazuje udostępnianie wszystkich utworów pochodnych również na GPL, zaś X11 nie nakazuje tego. Z tego powodu licencje nie są zgodne - kodu GPL nie można włączyć do kodu X11. Przypadek odwrotny, włączenie kodu X11 do projektu GPL, jest jak najbardziej możliwy. Licencja X11 nie zabrania sublicencjonowania, a wypuszczenie kodu na innej licencji, niż dotychczasowa (tutaj GPL) jest właśnie sublicencjonowaniem.

Jak dobrze widać na przykładzie, kompatybilność licencji nie działa w dwie strony. Przedstawiamy wykaz kompatybilności niektórych licencji[^8]. O ile nie zaznaczono inaczej, mowa wyłącznie o jednostronnej zgodności.

| Licencja włączanego kodu | Licencja projektu | Zgodność |
| - | - | - |
| GNU GPL | Licencja bez klauzuli copyleft | Nie |
| GNU GPL w wersji 1 i 2 | Apache 2.0 | Nie |
| GNU GPL w wersji 3 | Apache 2.0 | Tak |
| Apache 2.0 | GNU GPL | Tak |
| GNU GPL | GNU Affero GPL | Tak |
| GNU Affero GPL | GNU GPL | Nie |
| WTFPL | GNU GPL | Tak |
| BSD | GNU GPL | Tak |
| Licencja X11 | GNU GPL | Tak |
| Licencja X11 | BSD | Tak (w dwie strony) |

## Podsumowanie

Licencjonowanie nie jest sprawą prostą. Aby móc w pełni legalnie wykorzystać cudzy kod należy dokładnie zrozumieć licencję, na której kod zostaje udostępniony. Mam nadzieję, że niniejsze opracowanie przedstawiło dobre streszczenie oraz wytłumaczenie najpopularniejszych licencji oprogramowania w sposób prosty i zrozumiały.

## Bibliografia

- [Licencja GNU GPL w wersji 3](http://www.gnu.org/licenses/gpl-3.0.html)
- [Tłumaczenie licencji GNU GPL w wersji 3](http://blaszyk-jarosinski.pl/wp-content/uploads/2008/05/gnu-powszechna-licencja-publiczna-v3-po-polsku.pdf)
- [Licencja X11 (MIT)](http://www.opensource.org/licenses/mit-license.php)
- [Tłumaczenie licencji X11](http://blaszyk-jarosinski.pl/wp-content/uploads/2008/05/licencja-mit-tlumaczenie.pdf)
- [Definicja licencji](http://pl.wikipedia.org/wiki/Licencja_(prawo))
- [Definicja prawa autorskiego](http://pl.wikipedia.org/wiki/Prawo_autorskie)
- [Wytłumaczenie idei copyleft](http://www.gnu.org/copyleft/copyleft.pl.html)
- [Wytłumaczenie idei copyleft](http://pl.wikipedia.org/wiki/Copyleft#Idea)
- [Typy licencji BSD](http://en.wikipedia.org/wiki/BSD_licenses)
- [FAQ dotyczące licencji WTFPL](http://sam.zoy.org/wtfpl/)
- [Informacje nt. licencji GNU AGPL](http://pl.wikipedia.org/wiki/Affero_General_Public_License)
- [Tłumaczenie licencji GNU AGPL w wersji 3](http://blaszyk-jarosinski.pl/wp-content/uploads/2008/04/agpl-v-3.pdf)
- [Praca magisterska nt. modeli licencjonowania oprogramowania](http://reksio.ftj.agh.edu.pl/~azrael/mgr/praca_v2.60-cc.pdf)
- [Wytłumaczenie pojęcia „public domain”](http://pl.wikipedia.org/wiki/Public_domain)

[^1]: Źródło: http://www.gnu.org/copyleft/copyleft.pl.html 
[^2]: Więcej informacji na temat licencji Creative Commons można przeczytać na stronie fundacji (http://creativecommons.org/), bądź na Wikipedii (http://pl.wikipedia.org/wiki/Licencje_Creative_Commons)
[^3]: Więcej nt. restrykcyjności większości licencji można przeczytać pod adresem http://www.dwheeler.com/essays/floss-license-slide.html 
[^4]: Dla ścisłości, pierwszą wersję licencji GNU Affero General Public License wydała firma Affero Inc. Fundacja Free Software Foundation wydała zaś wersję trzecią tej licencji.
[^5]: Frequently Asked Questions, czyli najczęściej zadawane pytania.
[^6]: Przykład: http://forumprawne.org/prawo-autorskie/51124-zdjecie-na-wikipedii-public-domain-uzycie-bez-zgody-autora.html
[^7]: Dokładniej: „Licencje są kompatybilne ze sobą, jeżeli zezwalają na łączenie objętego nimi oprogramowania”, http://ksiewicz.net/?p=36#toc-kompatybilnosc-licencji-oraz-podwojne-licencjonowanie 
[^8]: Kompatybilność pomiędzy licencjami Creative Commons można w łatwy sposób zbadać, korzystając z kreatora dostępnego pod adresem http://creativecommons.org.tw/licwiz/english.html 
