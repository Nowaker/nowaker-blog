---
title: Ruby on Rails okiem developera Javy
---

[atlashost]: http://www.atlashost.eu/jira-hosting.html

Jakiś czas temu w mojej głowie narodził się pomysł uruchomienia
nowego biznesu - hostingu narzędzi developerskich firmy Atlassian.
Ja znam się na administracji tymi narzędziami oraz ich programistycznym
rozszerzaniem, zaś kolega kwiat jest administratorem i sieciowcem.
W taki sposób powstał [AtlasHost][atlashost], gdzie oferujemy
na razie hosting issue trackera JIRA.

<!--more-->

Jak to do każdego biznesu, potrzebowaliśmy jakiejś stronki z ofertą.
Można było oczywiście postawić pierwszy lepszy PHP-owy CMS, ale my
jesteśmy ambitni. Zdecydowaliśmy się wykonać prostą stronę, spełniającą
kilka prostych założeń:

- ładne URL-e dla SEO
- internacjonalizacja (polski i angielski)
- cennik trzymany w bazie danych
- składanie zamówień, zapis w bazie
- powiadomienie mailowe o nowym zamówieniu

Jak widać, jest to niewiele ponad prostą wizytówkę. Nie ma nic ciekawego
w tworzeniu takiej stronki... chyba że napisze się ją w czymś nowym
i nieznanym. Tylko w czym? Na kanale #graveyard na ircnecie mamy bota,
który zawsze pomaga w podejmowaniu trudnych, życiowych decyzji.

    <@Nowaker> ,(random-choose '(play lift grails web2py rails catalyst))
    <@straganiarz> rails  ..(symbol)

Wypadło - stronka będzie napisana w Ruby on Rails. :-)

## Ruby on Rails to kompletny framework

Nie można tego powiedzieć o frameworkach PHP. Nawet do najprostszych rzeczy
trzeba było coś sobie dopisywać. Nigdy nie dostałem OOTB dokładnie wszystkiego,
co było potrzebne - a były to dość typowe wymagania. W przypadku Ruby on Rails
nie napisałem żadnego dodatkowego kodu. Zmieściłem się dokładnie w tym, co
framework udostępnia.

Ekosystem Javy to całkowicie inna historia. Tutaj typowym podejściem jest
budowanie całej aplikacji z klocków. Tak więc za warstwę webową odpowiada np.
Apache Wicket, za bazę danych kontener EJB, a za cokolwiek innego - biblioteki
zdefiniowane jako zależności. Jedyne znane mi kompletne rozwiązanie to
Play Framework, które upodabnia webdevelopment w Javie do znanego z języków
dynamicznych.

## Ruby on Rails jest prosty

Ruby on Rails jest bardzo intuicyjny. Wszystko wykonuje się w taki sposób,
w jaki sobie pomyślałem. Do dokumentacji oczywiście musiałem trochę zaglądać,
ale w kilka chwil dostawałem odpowiedź na moje pytanie. Bez porównania z
dokumentacją KohanaPHP - zawsze należało zaglądać do kodu źródłowego, aby dostać
odpowiedź. Nie byłbym szczęśliwy, gdybym musiał zaglądać do kodu źródłowego Rails.

Rails jest prosty. Stworzenie jednej prostej strony wystarczyło mi, aby
odpowiedzieć na kilka pytań na Stack Overflow. To mówi samo za siebie.

## Oszczędzajmy kod

Wykonanie strony zgodnej z podanymi przeze mnie założeniami wymagało
napisania ok. 250 linii kodu w języku Ruby. Gdyby odliczyć puste linie,
wyszłoby pewnie z 200. Naprawdę mało, jak na podane przeze mnie wymagania.
Cała reszta to widoki, pliki i18n, itp. 

## Podsumowanie

Rails jest idealny do robienia prostych stronek z niewielką logiką.
Można powiedzieć, że nie trzeba pisać praktycznie żadnego kodu, by
osiągnąć potrzebne rzeczy. Polecam zwłaszcza developerom Javy, ponieważ
nasze ciężkie, korporacyjne technologie to najczęściej za dużo do
prostych stronek. :-)

Każdy developer PHP powinien zaznajomić się z Ruby on Rails, a najlepiej
od razu porzucić ten zły świat. Sam koszt wejścia w Rails jest minimalny.
Zarówno język Ruby, jak framework Rails są proste. NetBeans zaś pomoże
w poznaniu API. Ale wystarczyło mi napisanie jednego projektu, by
kolejne pisać już w zwykłym edytorze tekstu.

Jeśli zaś chodzi o aplikacje internetowe (czyli nie "stronki"), to pozostaję 
oczywiście w Javie. Zbyt bardzo cenię sobie wykrywanie błędów jeszcze w czasie
kompilacji i wielkie możliwości refaktoryzacji.
