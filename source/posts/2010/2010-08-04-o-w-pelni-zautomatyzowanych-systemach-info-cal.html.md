---
title: O w pełni automatycznych systemach INFO-CAL (cal.pl)
---

[Bardzo wielu][cal1] [miało][cal6] [różnorakie][cal2] [przeboje][cal3], [hulanki][cal4] i [swawole][cal5] z firmą INFO-CAL znaną jako **cal.pl**. Zdążyłem się już przyzwyczaić do ich sposobu traktowania klienta, ale dzisiaj mnie zaskoczyli. :-) Oczywiście negatywnie, ale – będąc już przyzwyczajony, że *to tak musi po prostu być* – dzisiaj miała sytuacja jak z kabaretu. Ubaw po pachy.

[cal1]: http://www.pajmon.com/2009/06/02/domeny-w-calpl-raz-jeszcze/
[cal2]: http://www.webhostingtalk.pl/topic/21062-uwaga-calpl-nie-wydaje-kodow-wystawia-nienalezne-faktury-i-odmawia-wyjasnien/
[cal3]: http://www.webhostingtalk.pl/topic/5229-opinia-calpl/page__p__88983#entry88983
[cal4]: http://www.webhostingtalk.pl/topic/5229-opinia-calpl/page__st__160__p__204126#entry204126
[cal5]: http://www.hosthelp.pl/opinie/cal-pl/
[cal6]: http://blog.heintze.pl/index.php/2010/08/10/jak-wydobyc-kod-authinfo-domeny-od-rejestratora/

<!--more-->

Dnia 23.07 próbowałem wysłać faksem wniosek o wydanie authinfo kilku moich domen. Ich faks niestety nie działał – nie wiem, czy mają VoIP-a, czy coś, no ale nie działało. Niepewnie wysłałem więc maila do ich legendarnego biura obsługi klienta...

> - Date: Fri, 23 Jul 2010 20:22:16 +0200
> - To: pomoc@cal.pl
> - Subject: **Wydanie kodów authinfo**
> 
> Próbuję wysłać wniosek o wydanie kodów authinfo na Wasz fax -
> 616690456, jednak faksu nie idzie wysłać. Czasami jest "nie ma takiego
> numeru", a innym razem jest sygnał faksu, ale nie odbiera. Ja mam
> normalną linię TPSA i normalny fax, więc wina nie leży po mojej
> stronie. W związku z powyższym w załączeniu przesyłam zeskanowany
> wniosek wraz z kompletem wymaganych dokumentów.</pre>

Odpowiedź:

> - Date: Sat, 24 Jul 2010 09:46:44 +0200
> - From: "Biuro Cal.pl - INFO-CAL" &lt;pomoc@cal.pl&gt;
> - Subject: **[#807047] Wydanie kodÃ³w authinfo**
> 
> Prosze wyslac wniosek na nr +48 61 6690456, w przeciwnym przypadku pozostaje jedynie opcja wyslania poczta. Jedynie te dwie formy sa akceptowane.

No no, "nie idzie wysłać do was faksu" – "proszę wysłać wniosek faksem". Pogratulować. :-) Wysłałem więc wniosek pocztą dla świętego spokoju – koniecznie [zobacz jak wyglądał][wniosek] przed wydrukowaniem i podpisaniem. Dzisiaj dostałem odpowiedź. Uwaga...

> - Date: Wed, 04 Aug 2010 15:29:39 +0200
> - From: Domeny Cal.pl &lt;domeny@cal.pl&gt;
> - Subject: **Odrzucenie wniosku [rowerowa-warszawa.pl]**
> 
> Witam serdecznie,
> pragne poinformowac, ze Panstwa wniosek numer  zostal odrzucony.
> 
> Powodem odrzucenia wniosku jest:
> 
> wniosek jest nieczytelny
> 
> Informacje o wniosku:
> 
> Numer wniosku:
> 
> Domena: rowerowa-warszawa.pl
> 
> Rodzaj wniosku: authinfo
> 
> W przypadku checi dalszego wykonania wniosku prosimy ponownie dodac wniosek w panelu Centrum Klienta w poprawionej wersji i wyslac go do nas ponownie.

E-mail 27 sekund później:

> - Date: Wed, 04 Aug 2010 15:30:06 +0200
> - From: Biuro Cal.pl &lt;kontakt@cal.pl&gt;
> - Subject: **Wnioskek**
> 
> Witam serdecznie,
> 
> wniosek nie zawiera kodu kreskowego, nie mozemy w zwiakzu z tym rozpoznac go wsystemie. Mamy w pelni automatyczny system.

Tragic Fail.

P.S. Przeczytaj również komentarze. Robi się jeszcze śmiesznej - cal.pl FTW! :-)

[wniosek]: http://www.nowaker.net/wp-content/uploads/2010/08/authinfo.pdf
