---
title: Wybory sterowane sondażami
---

Ostatnio gadaliśmy z kolegą przy piwie o nadchodzących wyborach. Zapytany na kogo zagłosuję bez wahania odparłem, że na Komorowskiego. Wdaliśmy się w dyskusję, czy aby to na pewno jest to dobry kandydat i ostatecznie wyszło na to, że tak naprawdę głosuję przeciw Kaczyńskiemu.<!--more-->

A wszystko przez te sondaże. To one podsycają emocje i sprawiają, że przeciwnicy Kaczyńskiego i PiS-u decydują się ostatecznie nie głosować na swojego osobistego faworyta, lecz na Komorowskiego. I odwrotnie, przeciwnicy Komorowskiego i PO odejdą od swoich osobistych faworytów i zagłosują na Kaczyńskiego - byle tylko Komorowski nie został prezydentem.

Istotą demokracji jest głosowanie „za kimś”, a nie „przeciw komuś”. Pamiętaj o tym i w dniu wyborów zagłosuj na swojego osobistego kandydata. Ja już wybrałem go dawno temu - ale dopiero teraz zagłosuję na Korwina.
