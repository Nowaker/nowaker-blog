---
title: Apprenticeship – długa droga ku Software Craftsmanship
---

*Apprenticeship Patterns. Guidance for the Aspiring Software Craftsman* to kolejna przeczytana przeze mnie książka. I w sumie, to nie wiedziałem do końca, o czym będzie. Sprawdziłem tylko w słowniku, że *apprentice* to praktykant. Tak więc książka miała być o jakichś wzorcach dla praktykanta, który ma ambicję zostać mistrzem w popularnym teraz nurcie Software Craftsmanship.

<!--more-->

O samym Software Craftsmanship coś więcej usłyszałem pierwszy raz na konferencji 4developers. Sławek Sobótka prowadził wtedy wykład *Craftsmanship i wzorce projektowe*. [Manifesto for Software Craftsmanship](http://manifesto.softwarecraftsmanship.org/) miał stanowić niejako uzupełnienie Agile Manifesto o brakujące części dotyczące kunsztu w wytwarzaniu dzieła, jakim jest software. Ja osobiście nie widziałem, aby manifestowi agile czegokolwiek brakowało. Postrzegałem manifest rzemieślników software'u jako doprecyzowanie pewnych myśli, które w Agile Manifesto nie zostały jawnie spisane. Dopiero przeczytanie tej książki dało mi całkowicie nowy pogląd na Software Craftsmanship jako całość. Autorzy poświęcają aż na to dziesięć stron. Jeśli jesteś ciekawy, o co w tym chodzi, to przeczytaj tą książkę. Możesz ją kupić na brytyjskim Amazonie za kilkanaście funtów lub [przeczytać wersję on-line](http://apprenticeship-patterns.labs.oreilly.com/index.html) wydaną na licencji Creative Commons.

Ciężko byłoby mi jednoznacznie zakategoryzować *Apprenticeship Patterns*. Niewątpliwie jest to książka o programowaniu, jednak nie ma w niej żadnej implementacji. Wprawne oko znajdzie tutaj dużo zagadnień socjologicznych, ale od strony „praktycznej” (czytaj: nieakademickiej). Ostatecznie, zauważalne są też elementy poradnika kariery w zawodzie developera.

Nam, programistom, słowo *wzorzec* bardzo spowszedniało. Mamy od razu przed oczyma adaptery, kompozyty i inne wizytatory. *Apprenticeship Patterns* to zbiór wzorców wg definicji Christophera Alexandra - sprawdzonych koncepcji rozwiązania powtarzających się problemów w sposób abstrakcyjny. W tym przypadku - różnorodnych problemów w pracy od stanowiska junior developera do seniora.

Muszę przyznać, że lubię pisać streszczenia książek. Dzięki temu mogę później przypomnieć sobie, co było w książce bez czytania jej od nowa. Co prawda część informacji ginie, ale zysk czasowy jest w takim przypadku znaczący. ;-)

## Chapter 1: Emptying the Cup

### Your First Language

Problem: *Przed pójściem do pierwszej pracy, musisz znać w pewnym stopniu jakiś język programowania.*

Na pewno znasz powierzchownie kilka różnych języków programowania, jednak na sam początek musisz wybrać *ten jeden*. Będzie to język, w którym będziesz rozwiązywał wszystkie problemy przez najbliższych kilka lat, tak więc Twój wybór musi być rozsądny. Z drugiej strony, nie jesteś doświadczony, więc wybór w pewnym sensie będzie na pewno przypadkowy. Możesz popytać bardziej doświadczone osoby, dlaczego wybrały taki, a nie inny język. Ostatecznie jednak decyzję będziesz musiał podjąć sam na podstawie znanych Ci zalet i wad różnych języków oraz osobistych preferencji.

Nauka poprzez analizę kodów źródłowych z książek i blogów przynosi słabe efekty. Znajdź sobie jakieś zadanie do wykonania - może to być napisanie prostego systemu internetowego do blogowania lub aplikacja okienkowa do katalogowania zdjęć. Wszystko jedno - byle tylko cel był konkretny.

Dołącz do społeczności Twojego języka programowania. Tej wirtualnej - czytaj fora, blogi, pytaj, komentuj. I tej prawdziwej - uczęszczaj na spotkania grup użytkowników Twojego języka. Czytaj fachową literaturę - książki z serii *Effective* (np. *Effective Java*, *Effective C++*) przedstawiają bardziej zaawansowane techniki pisania kodu w danym języku.

Nie zakładaj jednak klapek na oczy. Mimo że ten język będzie Twoim głównym językiem, warto później poznać inne. Chcesz być profesjonalistą, a nie adwokatem jednej technologii.

### The White Belt

Problem: *Zacząłeś się już czuć swobodnie w Twoim języku programowania. Nie poznajesz już nowych rzeczy - wszystko jest Ci już najczęściej znane.*

Odstaw na bok dotychczasowe doświadczenia i rozpocznij od zera w języku o całkowicie innym paradygmacie. Rozwiązuj w nim te same problemy, co poprzednio. Nie pisz jednak w poprzednim stylu - użyj właściwego dla nowego języka. Na pewno nie będzie łatwo - znajdź osobę, która wprowadzi Cię w temat.

### Unleash Your Enthusiasm

Problem: *Powstrzymujesz się przed okazywaniem entuzjazmu, ponieważ cały Twój team dziwnie by się na Ciebie patrzył. ;-)*

Nie daj się zgasić - musisz podchodzić do swojej pracy z entuzjazmem. Nie powstrzymuj się przed jego okazywaniem - on działa dobrze na Twój zespół. To nie tylko Ty uczysz się od nich - oni uczą się również od Ciebie. Przynosisz nowe, nieznane im pomysły z całkowicie innego świata i dajesz się im w ten sposób wykazać. Jeśli masz jakiś pomysł, nie bój się go wypowiedzieć. Jeśli zostaną wytknięte wady, poproś o rady, jak je naprawić.

Ponad rok temu przyszedłem na praktykę do bardzo silnego zespołu doświadczonych developerów. Mimo że moja wiedza była niewielka, byłem w stanie przekazać im kilka nowych, świeżych pomysłów.

### Concrete Skills

Problem: *Nie masz odpowiednich umiejętności, aby być przyjętym jako uczeń do dobrego zespołu. Zespół ma obawy, że nie będziesz w stanie zajmować się nawet najprostszymi rzeczami.*

Zdobądź te umiejętności. Dobra znajomość Twojego pierwszego języka na pewno będzie dużym atutem. Musisz jednak mieć znajomość innych dziedzin, np. podstaw dobrego designu, rozumienia architektury klient-serwer, potrafić napisać jakieś trywialne rzeczy w JavaScript itp. Znajomość popularnych bibliotek też będzie doceniona.

Zdobądź CV osób, których umiejętności podziwiasz. Wypisz sobie te, które byłyby potrzebne firmie, w której chciałbyś pracować. Wymyśl sobie projekt do realizacji, który pokaże, że posiadasz te umiejętności i go zrealizuj. Dzięki temu będziesz mógł go umieścić w swoim portfolio, a zespół będzie wiedział, że wniesiesz jakąś wartość.

### Expose Your Ignorance

Problem: *Twój zespół lub menadżerowie potrzebują pewności, że będziesz mógł dostarczać software, ale nie znasz części technologii.* 

Pokaż osobom, które będą polegać na Tobie, że proces uczenia się jest częścią dostarczania software'u. Niech widzą Twoje postępy. Nie udawaj, że coś wiesz, tylko pytaj.

Spisz pięć rzeczy z pracy, których całkowicie nie rozumiesz. Umieść listę w takim miejscu, aby inni to widzieli.

### Confront Your Ignorance

Problem: *Nie masz pewnej wiedzy lub umiejętności, które są niezbędne do pracy. Wszyscy naokoło zdają się to potrafić i zakładają, że Ty też to potrafisz.*

Zajmij się jedną z luk w Twojej wiedzy. Poczytaj literaturę dotyczącą tego tematu. Wymyśl sobie też projekt, którym załatasz tą lukę. Jeśli jakiś znajomy również tego nie potrafi, spróbujcie razem okiełznać ten temat.

Trzeba jednak podchodzić ostrożnie do tego wzorca. Nie warto w pracy budować atmosfery, w której uczenie się i popełnianie błędów jest czymś nieakceptowalnym, w efekcie czego każdy uczy się w sekrecie przed innymi.

## Chapter 2: The Long Road

### The Deep End

Problem: *Czujesz, że być może potrzebowałbyś większych wyzwań - np. większego projektu lub zespołu, bardziej złożoną aplikację lub nową dziedzinę biznesową.*

Nie czekaj aż będziesz dostatecznie gotowy, bo może to nigdy nie nastąpić. Jeśli zastanie Ci zaoferowane jakieś wielkie wyzwanie - podejmij się go. Nie oznacza to, że masz się zgadzać na coś, czego nie jesteś w stanie wykonać. Jeśli jednak jest szansa zarówno na powodzenie, jak i porażkę, to staw czoła wyzwaniu. Samemu jest jednak ciężko walczyć - proś o pomoc swoich mentorów i kolegów.

Zacznij oceniać złożoność każdego projektu, w którym byłeś lub będziesz zaangażowany. Uwzględniaj takie dane, jak liczbę osób w zespole, liczba linii kodu, itp. Wraz z każdym kolejnym projektem jesteś w stanie ocenić, w jaką stronę podąża Twoja kariera lub podjąć decyzje, które ją zmienią.

### Retreat Into Competence

Problem: *Podjąłeś się wyzwania poza Twoimi możliwościami.*

Czasami trzeba wykonać krok do tyłu, aby móc wykonać dwa do przodu. Trzeba to jednak zrobić jak najszybciej. „Krok do przodu” oznacza stan, w którym *jutro* wiesz więcej, niż *dzisiaj*. Ustanów sobie limit czasowy na wykonywanie kroków wstecz. Przykłady z książki:

> I will spend the next 10 minutes refactoring JavaScript validation for this page before I optimize SQL queries that provide the data.

> I will spend the next four hours implementing the command-line interface for this tool before I learn how to call this third-party SOAP API

> I will spend the rest of today improving our test coverage before taking on the job of optimizing our code that is affected by Python's Global Interpreter Lock.

### The Long Road

Problem: *Chciałbyś zostać mistrzem Software Craftsmanship. W obecnych czasach jak najszybsze dojście do jak najlepszej płacy wydaje się jednak najważniejsze. Mówią Ci, że powinieneś przestać programować i przejść na szczebel menadżerski, gdy tylko będzie to możliwe.*

Pogódź się z tym, że możesz wydać się innym odrobinę dziwny. Jeśli chcesz zostać mistrzem, musisz planować długoterminowo. Mistrzostwo osiągniesz pokonując długą i ciężką drogę. Na początku nie będziesz zarabiał dużo, ponieważ będziesz się uczyć. Jednak później ta inwestycja się opłaci - będziesz dobrze zarabiał, robiąc w dodatku to, co najbardziej lubisz. Spróbuj spisać swój 40-letni plan kariery - pomyśl sobie, jakie doświadczenia chciałbyś mieć i co chciał robić. Używaj tego planu do podejmowania decyzji odnośnie pracy.

### Craft Over Art

Problem: *Masz projekt do wykonania dla swojego klienta. Możesz użyć sprawdzonego rozwiązania lub napisać coś nowego i fantastycznego.*

Projekty wykonywane są w celu zaspokojenia potrzeby biznesowej klienta, a nie do wyrażania się artystycznie. Software, który budujesz może być piękny, ale *musi* być użyteczny. Jeśli nie będzie, to nikt go nie kupi. W dodatku, Twój software musi mieć pewną jakość, nawet jeśli klient wcale o nią nie prosi.

Spróbuj przypomnieć sobie, kiedy zamiast czegoś użytecznego wykonałeś dzieło artystyczne. Czy wiązały się z tym później negatywne konsekwencje dla klienta lub Ciebie? Co by się stało, gdybyś użył sprawdzonego rozwiązania?

### Sustainable Motivations

Problem: *Pracujesz w środowisku, w którym wymagania są słabo sprecyzowane, praca wygląda chaotycznie, atmosfera jest ciężka i wywierana jest na Tobie presja.*

Spróbuj przetrzymać ten ciężki okres tak, aby nie stracić motywacji do nauki. Są lepsze i gorsze dni - te lepsze z pewnością przyjdą. Nie możesz jednak czekać na nie w nieskończoność - być może będziesz musiał zmienić pracę. Jeśli dobrze zarabiasz, może być Ci trudno to zrobić. Jednak jest to niezbędne, ponieważ w tych ciężkich warunkach nie osiągniesz mistrzostwa.

Wypisz piętnaście rzeczy, które motywują Cię w pracy (nie w tej konkretnej, w której pracujesz, ale ogólnie). Poczekaj kilka minut i dopisz kolejnych pięć. Wybierz pięć najbardziej motywujących Cię czynników. Gdy przyjdą ciężkie czasy, użyj tej listy do oceny sytuacji.

### Nurture Your Passion

Problem: *Pracujesz w środowisku, w którym Twoja pasja się dusi. Chciałbyś wykonywać coś naprawdę ekstra, ale nie możesz. Na przeszkodzie stoją źli menadżerowie, głupie formalizmy, zły zespół, hierarchia korporacyjna lub projekt, który ewidentnie dąży do upadku.*

Jeśli w pracy nie możesz pogłębiać swojej pasji, musisz to robić po godzinach. Pracuj w domu nad rzeczami, które najbardziej lubisz. Własny projekt to dobry wybór. Uczęszczaj na spotkania grup osób o podobnych zainteresowaniach. Znajdź konkretne osoby, z którymi będziesz mógł zgłębiać pewne tematy. Czytaj klasykę.

Wyznacz jasno, czego nie akceptujesz w pracy. Jeśli reszta zespołu spóźnia się z dostarczaniem zadań, nie bierz kolejnego zadania. Opuść spotkanie, jeśli zamiast konstruktywnej krytyki latają personalne ataki. Nie akceptuj rozwiązań o słabej jakości. Być może narazisz się wielu osobom lub ominie Cię podwyżka, jednak najważniejsze jest to, że nie udusisz swojej pasji. A przy okazji może uda Ci się przeprowadzić małą rewolucję z dobrymi efektami.

### Draw Your Own Map

Problem: *Nie odpowiada Ci żadna ze ścieżek kariery, które daje Ci Twój pracodawca.*

Musisz dobrze wybierać miejsce swojej pracy. Oprócz zdefiniowanych celów na daleką przyszłość, powinieneś mieć również małe, osiągalne cele na dzisiaj i jutro. Codzienne osiąganie tych małych celów będzie gwarancją, że idziesz w dobrym kierunku. Stale aktualizuj swoją mapę i sprawdzaj, gdzie jesteś. Pamiętaj - to jest Twoja mapa, to Ty wybierasz swoją drogę, nie Twój pracodawca.

### Use Your Title

Problem: *Nazwa Twojego stanowiska w pracy nie odzwierciedla tego, co robisz. Gdy przedstawiasz się w profesjonalnym towarzystwie, miałbyś ochotę tłumaczyć różnicę między Twoim stanowiskiem i tym, co tak naprawdę robisz.*

Używaj swojego stanowiska wyłącznie do określenia swojego miejsca w hierarchii Twojej firmy. Poza tym nie powinieneś zaprzątać sobie głowy. Nikt nie powiedział, że Senior Application Developer na pewno jest lepszy od Junior Developera. Sam autor tej książki będąc na stanowisku seniora pisał swoje pierwsze, proste skrypty w Perlu.

Wymyśl i spisz długą, opisową nazwę Twojego tytułu. Ma ona dokładnie odzwierciedlać to, czym się zajmujesz w pracy oraz jaki jest poziom Twoich umiejętności. Aktualizuj swój tytuł.

### Stay in the Trenches

Problem: *Doceniono Cię w pracy i zaproponowano Ci awans na stanowisko z dala od programowania.*

Nie da się osiągnąć mistrzostwa w Software Craftsmanship, będąc z dala od programowania. Każdy dzień bez niego to krok do tyłu. Aby nie cofać się, Ty i Twój pracodawca musicie znaleźć inny sposób nagrodzenia Twojej pracy. Nawet jeśli jeszcze nie zaproponowano Ci awansu, przygotuj się na ewentualną rozmowę o tym. Spisz sobie, co pracodawca mógłby dla Ciebie alternatywnie zrobić, gdy odmówisz awansu. Może to być wyższa pensja albo poluźnienie formalnych warunków pracy.

### A Different Road

Problem: *Podążasz wg swojej mapy i oddalasz się od drogi ku Software Craftsmanship.*

Podążaj swoją mapą, pamiętając czego nauczyłeś się dotychczas. Idąc tą drogą na pewno nie osiągniesz mistrzostwa w Software Craftsmanship. Jesteś jednak bogaty w pewne doświadczenia i tego nikt Ci nie odbierze. I w sumie jest Ci dobrze, bo znalazłeś lepsze zajęcie dla siebie, niż bycie software developerem lub wolisz spędzać więcej czasu z rodziną. Nie bój się zmienić branży - twoje doświadczenia mogą Ci pomóc w innych zajęciach. Ostatecznie, zawsze możesz wrócić.

Załóżmy, że nie możesz być dalej software developerem. Kim byłbyś? Spisz kilka innych zawodów. Znajdź osoby w tych zawodach i zapytaj, co w nich lubią. Porównaj je z rzeczami, które Ty lubisz w tworzeniu software'u.

## Chapter 4: Accurate Self-Assessment

### Be the Worst

Problem: *Coraz mniej się uczysz w pracy.*

Zawsze otaczaj się lepszymi - tylko wtedy będziesz się uczył. Nie zdajesz sobie sprawy, jak bardzo bycie w silnym zespole zwiększa Twoją produktywność. Dzięki radom kolegów unikasz upadku albo powstajesz z niego bardzo szybko. Jedno pytanie może oszczędzić Ci wielu godzin karkołomnych prób. Nie oznacza to jednak, że masz być cały czas najgorszy. Pracuj najciężej, aby stać się najlepszym i móc iść do jeszcze silniejszego zespołu.

Wypisz sobie wszystkie zespoły jakie znasz, również te open source'owe i w innych firmach. Posortuj te zespoły wg rosnących umiejętności. Ustal, jakie zespoły są otwarte na nowych członków. Może to wymagać trochę pracy, np. dołączenia do list mailowych i zadawanie pytań. Wykonując to ćwiczenie nauczysz się porównywać skill zespołów i przy okazji znajdziesz zespół dla siebie.

### Find Mentors

Problem: *Błądzisz po drodze ku Software Craftsmanship.*

Na pewno nie jesteś pierwszą osobą, która idzie długą drogą ku Software Craftsmanship. Znajdź sobie mentora lub mentorów, którzy pomogą Ci utrzymać dobry kurs. Idealnie byłoby, gdybyś znalazł mistrza, który prowadziłby Cię od zera, jak w dawnych czasach rzemieślników. Jednak ciężko teraz o takich mentorów. Łatwo jest co prawda zlokalizować prelegentów z konferencji lub grup użytkowników i zabrać im kwadrans na krótką rozmowę, trudniej jest jednak znaleźć mentora. Jednak musisz go poszukać - nie bój się odrzucenia. Oni też kiedyś się uczyli i na pewno pamiętają swoich przewodników. Mentora możesz spróbować poszukać na liście dyskusyjnej. Po dołączeniu nie udzielaj się, tylko spróbuj zrozumieć wartości tej społeczności. Obserwuj, kto jest dobrym i cierpliwym nauczycielem. W przyszłości możesz zwrócić się do tych osób podczas konferencji z prośbą o pozostanie mentorem.

### Kindred Spirits

Problem: *Nie możesz znaleźć mentora.*

Jeśli nie możesz znaleźć mentorów, musisz poradzić sobie jakoś inaczej. Poszukaj bratnich dusz, z którymi będziesz mógł razem podążać długą drogą ku Software Crasftsmanship i wymieniać się doświadczeniami. Możecie wspólnie poznawać jakąś technologię lub też poznawać inne i zdawać sobie relacje. Najłatwiej jest zacząć chodzić na spotkania grupy użytkowników danej technologii lub języka w Twoim mieście. Jeśli nie istnieje, to masz niepowtarzalną szansę założenia takowej.

### Rubbing Elbows

Problem: *Masz mentorów i bratnie dusze, z którymi cyklicznie się spotykasz, jednak software piszesz sam.*

Znajdź osoby, z którymi będziesz mógł popracować wspólnie przed jednym komputerem nad jakimś projektem open source. Programowanie w parach to najbardziej praktyczna forma nauki. Napotykane problemy i ich rozwiązania są prawdziwe - nie to, co liczne przykłady z książek. No i co dwie głowy, to nie jedna. Gdy jedna osoba skupia się na szczególe implementacyjnym, druga czuwa nad *big picture*.

### Sweep the Floor

Problem: *Jesteś w początkowym etapie nauki i trafiłeś do zespołu. Nie jesteś pewny siebie, zespół też nie wie, czy podołasz. Chciałbyś się w jakiś sposób wnosić wkład i zyskać zaufanie.*

Podejmij się małych, prostych, ale ważnych zadań w projekcie. Takich, których członkowie zespołu nie lubią wykonywać, bo są żmudne i mało wymagające. Mogą to być prace administracyjne na serwerze lub pisanie skryptów automatyzujących pracę. Uważaj tylko, żebyś nie został na zawsze z tymi zadaniami.

## Chapter 5: Perpetual Learning

### Expand Your Bandwidth

Problem: *Masz podstawowy, ale wąski zasób wiedzy. Znasz niskopoziomowe detale tylko tych technologii, z którymi masz styczność w pracy.*

Ucz się nie tylko w pracy. Jest mnóstwo wiedzy dostępnej w sieci. Zacznij czytać blogi i grupy dyskusyjne. Śledź, czym zajmują się znane osoby ze świata developerów. Uczęszczaj na lokalne spotkania grup użytkowników oraz konferencje. Po przeczytaniu książki, skontaktuj się z autorem, zadając mu dodatkowe pytania.

### Practise, Practise, Practise

Problem: *W Twojej pracy nie ma miejsca na naukę poprzez popełnianie błędów - nie możesz się czymś zwyczajnie pobawić.*

Czytanie co prawda jest niezbędne, bo otwiera oczy, jednak to praktyką się najszybciej uczysz. Jeśli w Twojej okolicy organizowane są jakieś warsztaty dla programistów, to staraj się na nie uczęszczać. Ja tydzień temu na warsztatach *Code Retreat* ćwiczyłem test-driven development na przykładzie gry w życie. Pokazano nam przy okazji tzw. *code kata* - trywialne ćwiczenia do wykonania typu kalkulator, których celem jest wyłącznie praktyka. Jedno ćwiczenie wykonuj kilkakrotnie. Sprawdzaj, czy każde kolejne rozwiązanie jest coraz lepsze - jeśli tak, to znaczy, że wynosisz z ćwiczeń nową wiedzę.

### Breakable Toys

Problem: *Doświadczenie jest budowane na sukcesach i porażkach. W Twojej pracy nie ma jednak miejsca na porażki.*

Linus Torvalds zaczynał od napisania małego klonu systemu Minix. Wymyśl sobie realny, praktyczny projekt do wykonania. Wykonuj go z najwyższą starannością. Gdybyś na przykład wybrał system wiki, miałbyś możliwość dotknięcia architektury trójwarstwowej w aplikacjach internetowych, protokołu HTTP, sposobów trzymania sesji, autoryzacji, REST-a, itp. Ja do pierwszego projektu aplikacji internetowej w frameworku Apache Wicket wybrałem aplikację do zbierania śmiesznych cytatów z irca (tak zwane bashe). Gdy już zaznajomiłem się z podstawami tego frameworka, razem z kolegą zacząłem tworzyć aplikację internetową dla osób zajmujących się pozycjonowaniem stron internetowych.

### Use the Source

Problem: *Nie wiesz, czy Twój kod źródłowy jest dobry, ponieważ nikt poza Tobą na niego nie patrzy.*

Spróbuj poszukać osób, które mogłyby przeglądnąć Twój kod, żeby wytknąć Ci błędy, złe praktyki i wskazać dobre rozwiązania. Czytaj też kod innych - weź kod źródłowy kolegów lub jakiegoś projektu open source. Gdybyś przyjrzał się kodowi źródłowemu systemu kontroli wersji *git*, znalazłbyś w nim struktury danych, jakie Ci się nawet nie śniły. Spróbuj je wyciągnąć i zrozumieć. Przeanalizuj architekturę całego systemu i opisz ją na swoim blogu.

### Reflect As You Work

Problem: *Masz już wiele lat praktyki i czekasz na moment, kiedy w końcu będziesz mógł być uważany za „doświadczonego” developera.*

Wykonuj retrospektywy tego, co robisz. Zastanawiaj się, czy metody, jakimi pracujesz są dobre i w jaki sposób można je ulepszyć. Poszukaj w Internecie informacji o *Personal Practises Maps* Joe Walnes'a. Spróbuj narysować własną i przemyśleć swoje praktyki.

### Record What You Learn

Problem: *Wielokrotnie rozwiązujesz ten sam problem na nowo.*

Dokumentuj rozwiązanie problemu - potem będziesz mógł łatwo do tego wrócić. Umieść wpis na swoim blogu lub wiki. Być może dzięki temu pomożesz innym. Staraj się nie zapominać o swoich tekstach - czytaj je co jakiś czas i aktualizuj. Krótkie myśli i cytaty też warto przechowywać - Twitter lub prywatna część wiki nadają się do tego idealnie.

Ja staram się pisać streszczenie każdej dobrej książki, którą przeczytałem. Dzięki temu mogę je sobie przypomnieć małym kosztem czasowym i z dowolnego miejsca. Zacząłem też sobie zapisywać ciekawe polecenia Linuksa, których nie jestem w stanie zapamiętać.

### Share What You Learn

Problem: *Dotychczas byłeś skupiony na uczeniu się. Jednak osiągnięcie mistrzostwa oznacza uczenie innych.*

Wykształć sobie nawyk dzielenia się lekcjami, jakie wynosisz. Może to być w postaci bloga albo spotkań z bratnimi duszami (*Kindred Spirits*). Mimo, że wiedza którą posiadasz może być mała, dla innych nie w temacie to i tak dużo. Ucząc innych, Ty również się uczysz. Wyobraź sobie, że organizujesz warsztat na ten temat - mogą Ci wtedy przyjść do głowy rozszerzenia i ulepszenia. Jeśli nie ma jeszcze tutoriala na ten temat, napisz go.

### Create Feedback Loops

Problem: *Nie wiesz, czy jesteś kompetentny, czy nie.*

Stwórz pętle, którymi będziesz otrzymywać dane zwrotne. W programowaniu będą to takie techniki, jak test-driven development albo programowanie w parach. Przeglądy kodu również będą bardzo pomocne. Zapytaj kolegów z pracy, jak oceniają Twoje umiejętności - tym prostym sposobem możesz naprawdę dużo się dowiedzieć.

Jeśli odbyłeś rozmowę kwalifikacyjną, zdobądź kontakt do osoby, z którą rozmawiałeś - nawet jeśli nie zostałeś przyjęty, możesz zapytać o Twoje dobre i słabe strony w rozmowie. Ja półtora roku temu takim sposobem dowiedziałem się, że wpisałem do CV lekko zawyżoną znajomość języka oraz że mój list motywacyjny był totalnie do bani. Miałem za to dobre portfolio i było widać, że potrafię dać wartość klientowi.

### Learn How You Fail

Problem: *Twoja zdolność do nauki zwiększyła liczbę Twoich sukcesów, jednak Twoje błędy i słabości pozostają.*

Spróbuj zidentyfikować, w jakich warunkach masz tendencję do popełniania błędu. Spróbuj wymyślić rozwiązanie. Nie będziesz jednak w stanie wymyślić rozwiązania na wszystko i trzeba to zaakceptować. Ja jestem strasznie cienki w matematycznych algorytmach i nic już tego nie zmieni - ważne jest być świadomym swoich słabości.

## Chapter 6: Construct Your Curriculum

### Reading List

Problem: *Masz do przeczytania więcej książek, niż fizycznie jesteś w stanie.*

Utrzymuj listę książek, które przeczytałeś i które zamierzasz przeczytać. Jeszcze przed skończeniem aktualnej zaplanuj, która będzie następna. Miej ją gotową, że jak skończysz jedną, to od razu przejdziesz do drugiej.

Jeśli nie wiesz, jaką książkę przeczytać jako następną, to zwracaj uwagę na bibliografię w książkach, które czytasz. Dużo cytatów  i odniesień do innej książki oznacza, że tamta też może być dobra. Sklep Amazon również pomaga w szukaniu nowych, ciekawych książek dzięki sekcji „Frequently Bought Together”. Możesz podpatrzeć tytuły na [mojej liście książek](http://www.nowaker.net/devblog/ksiazki).

### Read Constantly

Problem: *Jest bardzo dużo zagadnień, które Cię intrygują, mimo że masz już bardzo dobrą znajomość Twojego pierwszego języka.*

Noś zawsze z sobą jakąś książkę. Dzięki temu będziesz mógł produktywnie wykorzystać każdy mały czas za dnia, jak na przykład oczekiwanie na autobus lub przyjęcie do lekarza. Ja stosuję ten wzorzec od roku - mój czas jest zbyt cenny, by go marnować go na bezmyślne patrzenie się w sufit.

### Study the Classics

Problem: *Twoja wiedza bazuje na samodzielnej praktyce. Osoby z Twojego otoczenia posługują się jednak fachowymi terminami z literatury, których nie do końca rozumiesz.*

Nie udawaj, że znasz te pojęcia. Zapytaj, z jakiej książki pochodzą i dodaj ją do Twojej listy. Książki typu *The Pragmatic Programmer*, *Refactoring*, czy *Design Patterns* (GoF) są ponadczasowe i każdy powinien je przeczytać.

### Dig Deeper

Problem: *Nie masz dogłębnej, niskopoziomowej wiedzy nt. technologii, w których pracujesz.*

Rozumienie istoty sprawy jest potrzebne. Tutoriale, z których korzystałeś pomijały rzadkie przypadki oraz szczegóły realizacji zadań. Spróbuj zajrzeć pod maskę technologii, których używasz. Zrozum, jak działają transakcje w EJB albo co gwarantuje Java Memory Model. Używaj Wiresharka, żeby zobaczyć, jakie dane lecą naprawdę po sieci. Przeczytaj RFC protokołów HTTP 1.0 i 1.1.

### Familiar Tools

Problem: *Ciężko Ci estymować czas wykonania zadania, ponieważ zbiór Twoich narzędzi i bibliotek szybko się zmienia.*

Skup się na wytworzeniu swojego zestawu narzędzi. Chodzi o takie technologie, w których nie będziesz musiał zerkać do dokumentacji i szukać rozwiązań w Internecie - w których będziesz biegły. Nie oznacza to jednak, że masz ograniczać się wyłącznie do tych narzędzi i rekomendować ich użycie w każdym kontekście. Ważne jest aktualizowanie swojego zestawu i zamienianie popsutego lub słabego młotka lepszym.

Wypisz sobie listę takich dobrze znanych Ci narzędzi. Jeśli jest ich mniej niż pięć, spróbuj poznać jeszcze jakieś. Jeśli jest ich wystarczająco, zastanów się, czy nie warto któregoś zamienić czymś lepszym.
