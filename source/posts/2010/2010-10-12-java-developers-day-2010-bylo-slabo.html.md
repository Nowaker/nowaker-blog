---
title: Java Developer’s Day 2010 – było słabo
---

Kilka dni temu wróciłem z konferencji Java Developer's Day 2010 z Krakowa. Moje ogólne wrażenie z konferencji jest *słabe*. Co z tego, że konferencja trwała dwa dni, jeśli tym razem dostępna była wyłącznie jedna ścieżka? Jeszcze jeden z wykładów był po prostu reklamowy. Przyszła firma i mówiła o sobie – czy za to zapłaciliśmy? Chcemy porozmawiać z firmami podczas przerw – tutaj z kolei panowała cisza jak po śmierci organisty. Aktywność sponsorów podczas przerw była praktycznie zerowa, jeden nawet nie przybył. Dobę przed konferencją zmieniły się godziny prelekcji "z przyczyn niezależnych". A w losowaniu nagród kolega wygrał książkę o Javie 1.2. To krótkie streszczenie tegorocznego JDD. Zapraszam do przeczytania szczegółowej relacji, jeśli jeszcze nie wierzysz, że było raczej słabo.

<!--more-->

## Dzień pierwszy

### RESTFul Java, Bill Burke 

Postać Bille Burke'a jest wszystkim znana. Napisał kilka dobrych książek, brał udział w formowaniu standardu Java EE 5, współtworzył JBossa. Jego prezentacja od strony merytorycznej była jednak słaba – stanowiła streszczenie pierwszej strony jakiegokolwiek tutoriala do JAX-RS. Oto całość wiedzy wyniesiona z prezentacji: @Produces, @Consumes, @Path, @PathParam, @POST, @GET, @DELETE, @PUT.

Wydaje mi się, że prezentacje na konferencjach tego typu nie powinny być zwykłymi tutorialami. Oczekiwałbym przekazania pomysłów, czy całej ideologii. Rok wcześniej o architekturze REST opowiadał Scott Davis – i to był majstersztyk!

### Programowanie w Javie w dobie procesorów wielordzeniowych, Angelika Langer

Myślałem, że może tutaj zostanę zasypany nową wiedzą i ideami. W trakcie prelekcji odniosłem jednak wrażenie, że ta prezentacja stanowi podsumowanie drugiego i trzeciego rozdziału *Java Concurrency in Practise*. Wiem, jak działa modyfikator *volatile*, sekcje *synchronized* i że kolekcje z pakietu *java.util.concurrent* są lepsze od zwykłych, synchronizowanych kolekcji w współbieżnych aplikacjach. Dowiedziałem się jedynie kilku szczegółów związanych z modelem pamięciowym Javy... albo szczegółami implementacyjnymi Sunowej JVM. Wojtek, [kolega z pracy](http://www.spartez.com), nie przypomina sobie, aby dostęp do pola *volatile* gwarantował *flushowanie* wszystkich pól obiektu, również tych *non-volatile*.

### Testowanie wydajnościowe aplikacji Java Enterprise, Jarosław Błąd

Chyba nie ma konferencji Javowej bez firmy e-point. :-) Tym razem Jarosław Błąd opowiedział o sposobach testowania wydajnościowego aplikacji Java EE. Dobra prezentacja, choć momentami wydawała mi się trochę zbyt teoretyczna.

### Przewodnik po programowaniu funkcyjnym w Javie zapracowanego developera, Ted Neward

Podczas prezentacji Ted przedstawił podstawowe założenia programowania funkcyjnego, pisząc na żywo krótkie przykłady w Javie. Nie chodziło oczywiście, by pisać taki kod, lecz żeby przekazać nam Javowcom idee programowania funkcyjnego.

Q: Should we then program functionally in Java, just like you have shown?
A: No, forget everything I was talking about! Use Scala.

Prezentacja od strony technicznej była żywa, z humorem i całkowicie przemyślana – najlepsza ze wszystkich. Tak powinny wyglądać wszystkie prezentacje. Można ją porównać do zeszłorocznej prezentacji Scotta Davisa.

### Odważne zmiany: jak realizować nowe pomysły, Linda Rising

Kolejna dobra prezentacja od strony technicznej. Linda opowiadała o tym, jak przekonać innych do swojego pomysłu. Wykład o tematyce typowo psychologicznej. :) Bardzo miło się słuchało, dopóki organizatorzy *brutalnie* nie przerwali prelekcji. Gdyby mówił jakiś nudziarz (np. Sundberg!), to zrozumiałbym – ale kto widział przerywać jeden z najlepszych wykładów? Zwłaszcza, że był to już ostatni wykład tego dnia. Organizatorom pewnie śpieszyło się do domu.

## Dzień drugi

W drugim dniu, oprócz ścieżki wykładowej, dostępne były trzygodzinne warsztaty. Należało się na nie zapisać kilka dni przed konferencją. Zapisy miały wystartować na stronie JDD 23 września równo o godzinie 17:00... ale wystartowały następnego dnia w nocy o nieznanej mi godzinie. Brawo. Mimo takich utrudnień udało mi się zapisać na warsztat prowadzony przez Teda Newarda o Scali.

### Warsztat: Przewodnik zapracowanego developera Java po języku Scala

Myślałem, że skoro miał to być *warsztat*, to będziemy sobie siedzieć przed komputerkami, a Ted  będzie dawał nam zadania i pomagał, jeśli nie będzie wychodziło. Był to jednak raczej wykład, niż warsztat. Z tego powodu nie rozumiem, dlaczego ustanowiono limit 25 osób na ten warsztat – z pewnością były osoby, które chciały tego posłuchać, a nie pozwolono im. Miejsc w sali było na drugie tyle.

Należy pochwalić Teda za sposób jego prowadzenia – tak samo dobry, jak wykładu o programowaniu funkcyjnym z wcześniejszego dnia. Gładko wprowadził nas w składnię Scali i jej możliwości, dając dobre przykłady. Mimo że nie napisałem dotychczas nic w Scali, mam wewnętrzne poczucie, że dałbym chyba radę. :-)

### Wipro Technologies w Europie i możliwości rozwoju na polskim rynku

Ekhm...

### Dług techniczny, Thomas Sundberg

Tematyka prezentacji jest naprawdę fajna. Jednak co z tego, jeśli prezentacja jest koszmarna? Thomas mówił tak monotonnie i cicho, że nie szło go po prostu słuchać. Gdy hostessy poprosiły go po kilku minutach o mówienie głośniej, to poprawił sobie mikrofon – nic to i tak nie dało. Prelegent byłby z pewnością dobrym lektorem w *Modzie na sukces*.

## Podsumowanie

Jedyne, co wyniosłem z tej konferencji to podstawowa znajomość Scali i niedosyt po przerwanym wykładzie Lindy Rising. No dobra, wyniosłem jeszcze pełny brzuch, bo jedzenie było naprawdę dobre. A wszystko inne słabe. Proponuję zmianę nazwy konferencji na Scala Developer's Day 2010, usunięcie reszty agendy i obniżenie ceny do 50 zł. Wtedy byłbym bardzo zadowolony z konferencji. Było po prostu **słabo**.