---
title: Nazywam się Gmail i RFC mam gdzieś
---

Początkowo, wpis ten miał charakter szyderczy i kpiący z Gmaila. Po dłuższym badaniu okazało się jednak, że to Gmail ma rację i to on najlepiej wykonuje to, co RFC przykazało. Część szyderczą pozostawiam, aby potem wykazać swoją niewiedzę.

<!--more-->

> From: Nowaker
>
> To: Gmail
>
> Nazywasz się Gmail, jesteś tworzony przez Google, to myślisz, że [RFC][rfcemail] dla ciebie nie istnieje? Że niby po co ci RFC? Cały Internet tylko dzięki mojej firmie się trzyma kupy, więc myślisz, że to ty ustalasz zasady? I ustaliłeś sobie, że dodanie spacji na końcu linii ma od teraz specjalne znaczenie. Wg ciebie taka spacja ma oznaczać, że znak końca linii ma zostać zignorowany. Nie podoba mi się to.

Wytłumaczenie: Jeśli linia tekstu posiada jakąkolwiek spację na swoim końcu, znak nowej linii zostanie zignorowany. W [RFC 2822][rfcemail] nie znajduje się jakakolwiek wzmianka, jakoby spacja przed znakiem końca linii miała mieć jakieś specjalne znaczenie.

> From: Gmail
>
> To: Nowaker
>
> Panie Nowaker, słabo pan korzystał z naszej wyszukiwarki. Gdybyś jej dobrze użył, na pewno znalazłbyś informację o [RFC 3676][rfcflowed]. Ba, znalazłbyś piękne polskie [opracowanie nt. format=flowed][opracowanie]. Gdybyś był świadomy tego, nie dodałbyś format=flowed do Content-Type twojej wiadomości. My zaleceń RFC przestrzegamy, nawet jako jedni z niewielu dobrze interpretujemy format=flowed! Nie dodaj bezmyślnie format=flowed, to nie będziesz miał takich problemów.

Wytłumaczenie: standard e-maili powstał w 1982 roku. Standard był odświeżany, jednak część niedoskonałości już pozostała. Jednym z nich jest ograniczenie do siedemdziesięciukilku znaków w jednej linii. O ile kiedyś, w dobie terminali tekstowych o stałej rozdzielczości, miało to bardzo ważne znaczenie, teraz staje się to problemem. Rozdzielczości monitorów się zwiększają, a linie zawijane są nadal przy tych 70 znakach. Z kolei w urządzeniach przenośnych e-maile czyta się jeszcze gorzej.

format=flowed i jego RFC stanowi uzupełnienie standardu wiadomości e-mail i dostosowanie go do realiów tych czasów. Nadal łamiemy linie w okolicach 70 znaków, ale jeśli znak nowej linii został wymuszony (łamanie jednego paragrafu na wiele linii), wtedy na końcu takiej złamanej linii dodajemy spację. Wtedy klient poczty rozumie, że nastąpiło w tym miejscu sztuczne łamanie. Może więc usunąć je, scalić paragraf w jedną linię i potem połamać paragraf wg preferencji użytkownika, np. dostosować do rozmiaru okna programu.

[Więcej o format=flowed przeczytasz tutaj][opracowanie]. Artykuł przedstawia praktyczne zastosowanie tego wynalazku.

> From: Nowaker
>
> To: Gmail
>
> Sorki, Gmail, że na ciebie tak najechałem. Okazało się, że to ty masz jednak rację. Wielki plus, że porządnie interpretujesz składnię wiadomości e-mail. Na swoją obronę chcę tylko powiedzieć, że nie zauważyłem, że moje narzędzie do generowania e-maili dodaje domyślnie format=flowed.

[rfcemail]: http://www.faqs.org/rfcs/rfc2822.html
[rfcflowed]: http://www.faqs.org/rfcs/rfc3676.html
[opracowanie]: http://konfiguracja.c0.pl/format_flowed.html
