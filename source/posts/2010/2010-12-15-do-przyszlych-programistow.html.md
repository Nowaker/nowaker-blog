---
title: Do przyszłych programistów
---

Uczelnia jest ważna. Student przychodzi z niewielką wiedzą i w trakcie studiów
informatycznych zaczyna powoli *wszystko rozumieć* - dorasta do bycia programistą.
Trzeba jednak uważać i nie jeść wszystkiego, czym karmią wykładowcy.
Jest to zadanie trudne - bo kogo słuchać, jeśli nie wykładowców?
Najpierw posłuchaj mnie, skoro już tutaj trafiłeś. ;-)
Na końcu wrócę do tego pytania i postaram się odpowiedzieć.

Ten wpis kieruję do osób, które jeszcze studiują i nigdy nie pracowali
w firmie wśród doświadczonych developerów. 

<!--more-->

## UML, ISO, TQM gwarancją jakości oprogramowania

UML to mainstream na uczelniach. Każdy student wie, że dobry projekt i
twardy proces to gwarancja wykonania najlepszego oprogramowania.
Zespoły należy dzielić na analityków, projektantów, architektów,
programistów i testerów. Użycie konkretnych wzorców projektowych
planuje się już na etapie modelowania.

Proces wytwarzania oprogramowania oraz jego utrzymywania trzeba normować,
aby mieć gwarancję jego najwyższej jakości. Dlatego powstały różne normy
ISO albo TQM. Zaś diagramy w języku UML to wspólny język programistów i klientów.

Nawet jeśli z ust wykładowców pada czasami słowo *agile*, nie ma ono
takiej siły przebicia, jak ciężkie metodyki. Student rzadko usłyszy, że
dzięki zwinnemu procesowi wychodzi lepszy software, a gwarancją jakości
są: ścisła współpraca z klientem, testy jednostkowe z dobrym pokryciem i
działający serwer ciągłej integracji.

## Singleton - świetny wzorzec projektowy

Singleton to świetny wzorzec projektowy, który gwarantuje istnienie tylko
jednej instancji obiektu. Gdy ten student pójdzie do pracy, zacznie pisać
własne singletony. Okazuje się potem, że taki kod trudno testować jednostkowo.
Nic dziwnego, skoro wewnątrz różnych klas umieszczone są na stałe wywołania
typu Database.getInstance().

Student nie zawsze usłyszy, żeby zamiast singletona wykorzystać kontener
wstrzykiwania zależności, a wszystkie zależności definiować jako parametr
konstruktora. 

## Bird extends Animal, to jasne

Ptak jest zwierzęciem, więc całkowicie jasne jest, że klasa Ptak *powinna*
rozszerzać klasę Zwierzę. To typowy, akademicki przykład.

Dziedziczenie wielobazowe powinno się znajdować w każdym języku programowania.
Ograniczenie wprowadzane przez dziedziczenie jednobazowe to tylko uproszczenie
twórców języków, aby łatwiej pisało się im kompilatory.

Nie spotkałem się, by radzono studentom programować interfejsami, a nie
implementacjami (*code for interfaces, not implementations*). Nikt na uczelni
mi nie powiedział, aby najpierw rozważyć kompozycję zamiast dziedziczenia.

## Komentarze w kodzie

Wśród wykładowców raczej panuje przekonanie, że wysoki stosunek liczby komentarzy
do ilości kodu świadczy o dobrym projekcie. Potem student idzie do pracy i myśli,
że komentowanie naprawdę jest ważne, w efekcie czego kod wygląda tak:

	class Site {
	
		/**
		 * Właściciel tej strony.
		 */
		private final Person owner;
	
		/**
		 * Ta metoda znajduje największy wspólny dzielnik dwóch liczb.
		 *
		 * @param a
		 * @param b
		 * @return
		 */
		public int fi_nw(int a, int b) {
		  // ...
		}
	}

Wykładowcy chyba nie czytali *Clean Code* Boba Martina lub *Test-Driven Development*
Kenta Becka. W bibliografiach slajdów najczęstszymi pozycjami są stare książki
*zasłużonych profesorów* wychwalające UML, twarde procesy wytwórcze i "dobrze"
udokumentowany kod.

## KISC - Keep It Stupid Complicated

Wykładowcy najczęściej lubią rozwiązania przekombinowane. Musisz się namęczyć,
aby zasłużyć na ocenę. Proste, sensowne rozwiązania nie wchodzą w grę.

Sieć komputerowa w trzyosobowej firmie z branży księgowości musi mieć cztery
różne routery, dwa poziomy firewalla, switch zarządzalny, serwer www w strefie
DMZ, dwa serwery kopii zapasowej i serwer bazy danych odcięty od Internetu.
Proste i dobre rozwiązanie z jednym switchem i jednym serwerem na firewall, 
a drugim na bazę i backup jest niedopuszczalne. Biuro księgowe potrzebuje
profesjonalnych rozwiązań. 

Program do tagowania plików na dysku powinien działać na bazie
Microsoft SQL Server. SQLite to rozwiązanie nieeleganckie i prostackie.

Aplikacja internetowa na zaliczenie projektu nie może zostać napisana przy
użyciu jakiegokolwiek frameworka PHP. Użycie jQuery również jest zabronione.
Po co pisać elegancki, wysokopoziomowy kod?

Potem najczęstszym grzechem studenta, który przychodzi po studiach do pracy
jest łatwość, z jaką przychodzi mu utrudnianie sobie życia. Wszystko chce sam
implementować oraz wprowadzać niepotrzebne rzeczy. Pamiętaj o KISS i YAGNI.

## Napisz i wyrzuć

Wydaje mi się, że problemem jest sposób prowadzenia projektów.
Student otrzymuje zadanie - dla zobrazowania sytuacji niech będzie to
*napisanie aplikacji, wykorzystującej Java Persistence API*.
Nadchodzi termin oddawania projektu - student przynosi program i go
prezentuje. Prowadzący sprawdza, czy wszystko działa, jak należy i
wystawia ocenę. Najczęściej działa, bo w 5 minut błędu się pewnie
nie znajdzie.

W biznesie samo wytworzenie oprogramowania to dopiero początek.
Schody rozpoczynają się dopiero w fazie utrzymania. Wykrywane są
błędy, potrzebne są nowe funkcjonalności, itd.
Jak nauczyć studenta, żeby dogłębnie testował swoje programy?
Nie znam niestety odpowiedzi na to pytanie.

## Podsumowanie

Wykładowcy nie są tacy źli, jak by się mogło wydawać po tym wpisie.
To mądrzy ludzie, którzy dzielą się z Tobą wiedzą. Należy jednak
wybaczyć im brak wiedzy i umiejętności typowych dla biznesu. Wykładowcy
rzadko mają możliwość pracowania w grupie nad oprogramowaniem.
Nie tworzą software'u dla klientów, którzy mają wygórowane wymagania.

*Kogo słuchać jeśli nie wykładowców?* Słuchaj zarówno wykładowców,
jak i profesjonalistów z branży. Konferencje informatyczne, płatne czy darmowe,
to sposób na poznanie takich ludzi. W uproszczeniu możesz przyjąć, że
prelegenci dużych konferencji to osoby, których warto posłuchać.

Trzecim źródłem wiedzy są książki pisane przez doświadczonych developerów.
Warto zainteresować się pozycjami podpisanymi takimi nazwiskami, jak
jak Robert C. Martin, Kent Beck, Dave Thomas, Andy Hunt, Martin Fowler.
