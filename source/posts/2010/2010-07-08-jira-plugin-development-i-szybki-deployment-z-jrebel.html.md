---
title: JIRA plugin development i szybki deployment z JRebel
---

Nie od dziś wiadomo, że redeploy aplikacji Javowych to jedna z najbardziej wkurzających rzeczy.  Potrafi trwać od kilku sekund do długich minut – wszystko w zależności od wielkości aplikacji oraz użytego serwera aplikacji. Do tego dochodzi jeszcze kilka sekund budowania Mavenem.

<!--more-->

Z tego powodu pisanie pluginów do Atlassian JIRA nie należy do najprzyjemniejszych rzeczy, ponieważ jej przeładowanie (*mvn -P plugin-debug*) może trwać jedną lub dwie minuty.[^1] Zmiany w kodzie można co prawda przeładowywać podczas debugowania, ale w bardzo ograniczonym stopniu – zmiana sygnatury metody już wymaga przeładowania całej JIRY. Podobnie też zobaczenie zmian w szablonach Velocity wymaga przeładowania.

Jest jednak sposób, ażeby wszystkie zmiany były widoczne w mniej niż sekundę po zapisaniu pliku (klasy lub resource'a). Kupić licencję na JRebel za $59 i skonfigurować, co trzeba.

## Co jest potrzebne?

- JRebel z jakąś licencją – może być nawet evaluation
- IDE, które kompiluje klasy zaraz po naciśnięciu CTRL+S
- jakiś plugin ;-)

## Plik konfiguracyjny JRebel w projekcie

Należy stworzyć plik rebel.xml i umieścić go w *src/main/resources*. Próbowałem go wygenerować automatycznie przez *mvn javarebel:generate*, jednak nie chciało u mnie działać. Plik wygląda mniej więcej tak:

~~~
<?xml version="1.0" encoding="UTF-8" standalone="no" ?>
<application xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns="http://www.zeroturnaround.com" xsi:schemaLocation="http://www.zeroturnaround.com/alderaan/rebel-2_0.xsd">
	<classpath>
		<dir name="/home/nowaker/projekty/JiraScrumPrint/src/main/resources" />
		<dir name="/home/nowaker/projekty/JiraScrumPrint/target/classes" />
	</classpath>
</application>
~~~

Teraz przychodzi zadanie dla Twojego IDE, aby klasy kompilowały się od razu po zapisaniu. W NetBeansie jest to „Compile on save” w opcjach projektu. Dla Eclipse'a i IntelliJ IDEA instrukcje podaje „Configuration wizard” w JRebel.

## Podłączenie JRebel do JIRY

Trzeba zdefiniować nadpisać property „jvmargs”. Najprawdopodobniej nie masz go w pom.xml – jego definicja znajduje się poziom wyżej w jira-plugin-base.

~~~
<properties>
	<jvmargs>-XX:MaxPermSize=256m -Xms128m -Xmx564m -Djira.home=${project.build.directory}/jira-home -noverify -javaagent:/opt/java/jrebel-3/jrebel.jar -Drebel.log=true -Drebel.log.file=debug/jrebel.log</jvmargs>
</properties>
~~~

To zapewni dołączenie agenta JRebel do maszyny wirtualnej. Aby sprawdzić, czy JRebel uruchamia się z JIRĄ, sprawdź czy pojawia się coś w jrebel.log (*tail -f debug/jrebel.log*). W logu atlassian-jira.log też powinna się znaleźć jakaś wzmianka o JRebel.

## Przeładowywanie szablonów Velocity

JIRA keszuje szablony Velocity, więc przeładowanie zasobu w archiwum JAR pluginu to za mało. Trzeba więc to keszowanie wyłączyć. Zmodyfikuj plik *debug/jira/jira-webapp/WEB-INF/classes/velocity.properties* i ustaw class.resource.loader.cache=false.

## Podsumowanie

Otrzymaliśmy instancję Atlassian JIRA, która „widzi” na bieżąco wszystkie zmiany w klasach i resource'ach.

[^1]: Istnieje też Atlassian SDK, który pozwala w locie przeładowywać pluginy. Minusem tego rozwiązania jest jednak to, że te pluginy mają inną architekturę i nie działają z JIRĄ 3.13, której udział w rynku jest znaczący.
