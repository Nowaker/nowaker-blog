---
title: 4developers – relacja z konferencji
---

Przed dwunastą przekroczyłem próg swojego mieszkania - wróciłem z konferencji [4developers][4d] w Poznaniu. Oto publikuję moje przemyślenia z prezentacji, na których uczestniczyłem.

[4d]: http://2010.4developers.org.pl/

<!--more-->
## Walka o życie - pochodne i następcy Javy opanowują świat, Michael Hunger

W założeniach prezentacji było pokazanie różnych języków programowania działających na JVM. Michael najpierw mówił 10-15 minut o ekosystemach leśnych oraz polach uprawnych, które moim zdaniem leżą bardzo daleko od głównego tematu prelekcji. Wstęp jakiś zawsze musi być, ale czy tracenie pierwszych najważniejszych piętnastu minut prezentacji na jakieś lasy naprawdę miało sens?

Gdy już Michael się rozkręcił i pokazał slajd z listą kilku języków programowania działających na JVM, przeszedł do przekazywania bardzo cennych informacji o tych językach. Scala - kierowana przez Oderskiego, obiektowo-funkcyjny; JRuby - napisane przez kogoś tam w którymś tam roku, a RoR to fajna sprawa. I tak dalej...
[4d]: http://2010.4developers.org.pl/
Potem prelegent przeszedł do mówienia o grze w życie (Game of Life). Na wstępie wyśmiał implementację Allena Holuba. Całkowicie zapomniał, że jego implementacja miała nauczyć czytelnika korzystania z wzorców projektowych. W żadnym wypadku nie miała to być krótka i zwięzła implementacja gry w życie. Potem pokazał implementację w Groovym, JRubym oraz jakimś dziwnym języku do operacji na macierzach. Po drodze puścił dwa filmiki z YouTube'a - o ile pierwszy był jeszcze znośny, to drugi był wręcz męczący. 5 minut patrzenia, jak jakiś koleś wpisuje dziwne znaczki z klawiatury i otrzymuje macierze.

Ostatecznie przedłużył prelekcję o 10 minut. Ja też chcę wypić herbatę na przerwie i pogadać z innymi osobami na konferencji.

## Automatyczne generowanie kodu, Marek Berkan

Na wstępie prelegent powiedział, że celem jego wykładu jest przedstawienie technik i narzędzi, aby oprogramowanie było łatwiej "utrzymywalne". Jako pierwszy zły przykład pokazuje kod SQL bezpośrednio w kodzie Java. Widać, że jego celem jest stwierdzenie, że wypadałoby użyć mapowania obiektowo-relacyjnego... ale za nic w świecie nie chciał tego powiedzieć. Cały czas krążył naokoło tego, jak gdyby prowadził wykład dla uczniów gimnazjum. Minęło 10 minut, nim powiedział "tak, ORM będzie tutaj dobrym rozwiązaniem".

Jedyna rzecz, która mnie jakoś zainteresowała, to pomysł na obsługę plików lokalizacyjnych w aplikacjach internetowych. Marek sugeruje, aby odniesienia po kluczu opakowywać w jakieś klasy, aby wyłapywać klucze bez tłumaczeń na etapie kompilacji. OK, dobra informacja - tylko jedna dobra informacja na 45 minut prelekcji to trochę za mało. Przydatne informacje wprowadziła w sumie dyskusja po prezentacji.

## Toplink Grid - skalowanie aplikacji korzystających z Java Persistence API, Waldemar Kot

Konkretnie i na temat o uwydajnianiu aplikacji korzystających z JPA i TopLinka. Waldek przedstawił "Data grid" - product Oracle'a, który staje pomiędzy EntityManagerFactory, a bazą danych jako klaster cache. Szkoda, że produkt nie jest darmowy dla zastosowań komercyjnych. ;-)

## Ewolucja outsourcingowych projektów informatycznych w ostatniej dekadzie, Daniel Jabłoński

Daniel opowiedział o zmianach, jakie w ciągu dwudziestu lat nastąpiły w dziedzinie outsourcingowanych projektów:

- kiedyś planowano na 10 lat do przodu, teraz tak dalekie planowanie upadło
- kiedyś piłeczka była odbijana od klienta do wykonawcy (i odwrotnie), teraz występuje współpraca
- kiedyś wymagano ścisłych procesów, teraz klient wymaga tylko tyle, by było dobrze

Nie kojarzy się to wam z agile? ;-) Daniel jednak nie wydawał się być przekonany, aby te zmiany miały bezpośredni z wiązek z niewydolnością standardowego zarządzania projektami.

## Najważniejsze pytanie w projekcie, Jakub Dąbkowski

Jakub na wstępie kilkakrotnie mówił, że stresuje się jako prelegent i powinniśmy mu wybaczyć niedociągnięcia. W gruncie rzeczy poprowadził jedną z najbardziej żywiołowych i ciekawych prezentacji. Widać, że lubi rozmawiać - po tym można rozpoznać dobrego menadżera.

Każdego dnia podczas tzw. stand upu każdy z członków zespołu agile'owego odpowiada na trzy pytania:

1. Co zrobiłem wczoraj?
2. Co zrobię dzisiaj?
3. Co mi przeszkadzało?

I najważniejsze jest to trzecie. Najbardziej zabawnym przykładem była odpowiedź "nie mam śmietnika". :-) Developerowi to przeszkadzało, ponieważ każdego dnia wykonywał bardzo dużo rysunków/notatek na kartkach, a do kosza na śmieci miał daleko. Odchodzenie od miejsca pracy, aby wyrzucić niepotrzebne kartki wybijało go z rytmu. Dostał więc śmietnik.

Przykładów było więcej. Przesłanie jednak jest jedno - jeśli każdy członek zespołu będzie mówił, co mu przeszkadza, możliwa będzie zmiana procesu/organizacji zespołu, czy też miejsca ich pracy. Przełoży się to na większą efektywność zespołu i lepszy kod. 

Poruszane były również tematy o byciu dobrym menadżerem, jednak jako typowy koder pomijam te aspekty. ;-)

## Craftsmanship i wzorce projektowe, Sławomir Sobótka

Faktem jest, że raczej niewiele można powiedzieć o wzorcach projektowych podczas 45-minutowej prezentacji. Mimo tego prezentacja Sławka mi się podobała. Mówił o tym, kiedy warto stosować wzorce projektowe, a kiedy nie - również na prostych przykładach.

Wg Sławka, [Agile Manifesto][ag] to trochę za mało. Jako ideał przytacza [Manifesto for Software Craftsmanship][cr]:

- Not only working software, but also well-crafted software.
- Not only responding to change, but also steadily adding value.
- Not only individuals and interactions, but also a community of professionals.
- Not only customer collaboration, but also productive partnerships.

Ja z tym manifestem nie tyle nie zgadzam się, co uważam, że jest wyrażeniem Agile Manifesto w trochę inny sposób. Może bardziej pokrętny, czy też wyidealizowany.

- Ciągłe dodawanie wartości jest niczym innym, jak dawaniem klientowi działającego software'u co każdą iterację.
- Poprzez wzajemne interakcje w zespole, wytwarza się społeczność profesjonalistów - jedno wynika z drugiego. Dodać należy, że wg teorii [złożonych systemów adaptacyjnych][cas], siła zespołu rośnie razem z ilością interakcji w nim.
- Punkt czwarty niczego nie zmienia. [Collaboration][co], czyli współpraca z założenia musi być produktywna, ponieważ jest nakierowana na osiągnięcie pewnego celu, czyli produktu. Oznacza więc w praktyce tyle samo co "produktywne partnerstwo". 

Oczywiście moje zdanie na temat Craftsmanship nie ma żadnego wpływu na wrażenia z prelekcji. Była ciekawa oraz dobrze się jej słuchało.

## Modele komponentowe SCA, OSGi, Distributed OSGi i OSGi Enterprise a Java EE, Jacek Laskowski

Gdy Jacek rozpoczął prelekcję, okazało się, że popsuły mu się slajdy. Został tylko slajd tytułowy - reszty nie było. Ano, nie było, bo Jacek rozpoczął prezentację od ostatniego slajdu, który był taki sam, jak tytułowy. Lekcja wzięta z tego zdarzenia: nigdy nie kopiuj slajdu tytułowego na koniec prezentacji. ;-)

Wykład naprawdę fajnie poprowadzony, z odpowiednią ilością humoru oraz interakcją z publicznością. Po wykładzie poczułem się jak prawdziwy ekspert OSGi. Jacek nakłaniał bowiem, aby do swojego CV dopisać znajomość wszystkich technologii związanych z OSGi i poruszonych na tym wykładzie. ;-)

## Podsumowanie

Dwa wykłady były słabe, w tym jeden z nich był tragiczny. Pięć innych było w porządku. Szybkie obliczenia i 5/7 daje ocenę 70%. Na tyle podobała mi się konferencja 4developers. Szału raczej nie ma, bowiem darmowa Javarsovia 2009 uzyskała u mnie ocenę 100%, a Java Developer's Day 2009 - 85%. 

Oczywiście konferencja to nie tylko prelekcje - również rozmowy z innymi programistami w kuluarach. Czas na pewno dobrze spożytkowany, chociaż te dwa nieszczęsne wykłady mogły być lepsze.


[co]: http://www.ldoceonline.com/dictionary/collaboration
[cas]: http://en.wikipedia.org/wiki/Complex_adaptive_system
[cr]: http://manifesto.softwarecraftsmanship.org/
[ag]: http://agilemanifesto.org/
[4d]: http://2010.4developers.org.pl/

