---
title: Linux x64 i emulator Java Micro Edition
---

Niedawno postanowiłem zainstalować sobie Ubuntu na świeżo, przy okazji migrując na wersję 64-bitową. Jak postanowiłem, tak uczyniłem tydzień temu. Dzisiaj chciałem kontynuować pracę nad jednym projektem w Javie ME i...

<!--more-->

~~~
java.lang.UnsatisfiedLinkError: /opt/sun-java-me-wtk-2.5-1/bin/sublime.so: /opt/sun-java-me-wtk-2.5-1/bin/sublime.so: wrong ELF class: ELFCLASS32
at java.lang.ClassLoader$NativeLibrary.load(Native Method)
~~~

Próba odpalenia emulatora Java ME na 64-bitowym Linuksie kończy się błędem, ponieważ WTK 2.5 działa tylko w architekturze x86 (32-bit).

## Rozwiązanie:

1. Zainstalować 32-bitową wersję JDK. W Ubuntu jest pakiet *ia32-sun-java6-bin*
2. Zainstalować Sun Wireless Toolkit (WTK) np. w */opt/wtk-x64*
3. W pliku */opt/wtk-x64/bin/emulator* zamienić *javapathtowtk=* na *javapathtowtk=/usr/lib/jvm/ia32-java-6-sun/bin/*
4. Skonfigurować swoje IDE tak, aby korzystało z emulatora w */opt/wtk-x64*. W przypadku NetBeans będzie to Tools - Java Platform, a potem we właściwościach projektu wybrać nowo utworzone emulowane środowisko Java ME

Efekt - wszystko działa jak należy. Na blogu Chrisa Hattona przeczytałem, że używanie Javy z pakietu *ia32-sun-java6-bin* do emulatora nie daje wszystkich możliwości, np. nie można debugować. Ale u mnie działa. ;-)
