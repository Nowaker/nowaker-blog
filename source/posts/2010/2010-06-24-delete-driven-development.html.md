---
title: Delete Driven Development
---

1. Usuń losową linię kodu.
2. Odpal testy jednostkowe.
3. Jeśli nadal jest zielone światło, zachowaj zmiany na stałe.