//= require vendor/jquery-2.1.0.min.js
//= require bootstrap

$(function() {
  var viewPortHeight = $(window).height();
  var $footer = $('footer');
  var footerHeight = $footer.height();
  var footerBottom = $footer.position().top + footerHeight;

  if (footerBottom < viewPortHeight) {
    // padding-top + padding-bottom + border-top = 21
    $footer.css('margin-top', (viewPortHeight - footerBottom - 21) + 'px');
  }
});
